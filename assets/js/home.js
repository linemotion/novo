$(document).ready(function() {

    $(".home-product-items").owlCarousel({
       items : 4,
       navigation : false,
       slideSpeed : 500
    });

    $("#hpi-next").click(function(){ $(".home-product-items").trigger('owl.next'); return false; });
    $("#hpi-prev").click(function(){ $(".home-product-items").trigger('owl.prev'); return false; });


});