$(document).ready(function() {


    // $(".pg-items").owlCarousel({
    //    items : 5,
    //    navigation : false,
    //    slideSpeed : 500
    // });
      $("#submit-newsletter").click(function () {

        var value = $("input[name = 'mail']").val();
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        
        if (pattern.test(value) && value)
        {
            $.post("/newsletter/flash",{ "mail": value},

            function(data){
             window.location.replace('/' + location.pathname.split('/')[1] + data);
            }); 
        } 
        else
        {
          $("input[name = 'mail']").css({"border-color": "red"});
        } 
        
        return false;
      });

    $(".fancybox").fancybox({
        padding: 10
    });
    $(".open-lightbox").fancybox({
        padding: 10
    });

    $( ".site-article iframe" ).wrap( "<div class='video-container'></div>" );

    if ( $( ".video-container" ).length ) {
        $(".video-container").fitVids();
    }

    if ( $(".sidebar-filters").length && $(".sidebar-filters h4").length ) {
        $('<div class="filters-responsive"><a href="#" class="filters-open btn-default btn-no-icon">Filteri</a></div>').insertAfter('.sidebar-responsive');
        var sidebarFilters = $(".sidebar-filters").parent();
        enquire.register("screen and (max-width: 640px)", {
            match : function() {
                sidebarFilters.appendTo(".filters-responsive");
            },  
            unmatch : function() {
                sidebarFilters.appendTo(".subnav-content");
            }
        });
    }

    if ( $(".inside-sidebar-move").length) {
        var sidebarMove = $(".inside-sidebar-move");
        enquire.register("screen and (max-width: 650px)", {
            match : function() {
                $('.inside-main').after(sidebarMove);
            },  
            unmatch : function() {
                $('.inside-main').before(sidebarMove);
            }
        });
    }

    $(".filters-open").click(function () {
        $(this).fadeOut();
        $('.filters-responsive .sidebar-filters').slideDown();
        return false;
    });

    $('.select-url').change( function() {
        location.href = $(this).val();
    });

});