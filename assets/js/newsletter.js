$(document).ready(function(){

      $("#submit-newsletter").click(function () {

        var value = $("input[name = 'mail']").val();
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        
        if (pattern.test(value) && value)
        {
            $.post("/newsletter/flash",{ "mail": value},

            function(data){
             window.location.replace('/' + location.pathname.split('/')[1] + data);
            }); 
        } 
        else
        {
          $("input[name = 'mail']").css({"border-color": "red"});
        } 
        
        return false;
      });

  // function equalHeight(){
  //   var heightArray = $(".content-inside .col").map( function(){
  //        return  $(this).height();
  //        }).get();
  //   var maxHeight = Math.max.apply( Math, heightArray);
  //   $(".content-inside .col").height(maxHeight);
  //     }

  // equalHeight();

  // $("a.showLink").click(function(){
  //     $("#hide").show();
  //     $(".columnContainer>div").removeAttr("style");
  //     equalHeight();
  // });


});