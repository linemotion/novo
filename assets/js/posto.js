$(document).ready(function() {

    $("input[name='prijava-type']").change(function () {
	    var choice = $(this).val();
	    if (choice == "fizicka") {
	        $("#pravna-container").hide();
	        $("#fizicka-container").show();
	    }
	    else{
	        $("#fizicka-container").hide();
	        $("#pravna-container").show();
	    }
	});

    var counter = 1;

    $("#dodaj-lice").click(function () {
    	counter++;
    	
    	lice = $('.lice:last').clone();
    	lice.find("input").val("");
    	lice.find("span").html(counter);

    	$(".lice:last").after(lice).html();

    	return false;
	});


    // Fizicka lica validacija

	$("#fizicka-form").submit(function(e) {

		$("#fizicka-form input").each( function() {
		    if ( $(this).val().length < 1) {
	            $(this).next().removeClass("dn");
	            e.preventDefault();
	        }
		});
        
    });


    // Pravna lica validacija

	$("#pravna-form").submit(function(e) {

		$("#pravna-form input").each( function() {
		    if ( $(this).val().length < 1) {
	            $(this).next().removeClass("dn");
	            e.preventDefault();
	        }
		});
        
    });



});