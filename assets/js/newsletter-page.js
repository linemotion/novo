$(document).ready(function() {

    var checkboxNumber = 0;
    $('.cf-checkbox').change(function() {
        checkboxNumber = $(".cf-checkbox:checked").length;
        $('.checkbox-number').text(checkboxNumber);
    });

    chooseTrigger = 0;

    $('.cf-choose-trigger').click(function() {
        if (chooseTrigger == 0) {
            chooseTrigger = 1;
            $('.ch-choose-content').slideDown();
        }
        else {
            chooseTrigger = 0;
            $('.ch-choose-content').slideUp();   
        }
    });

    $('.ch-choose-close').click(function() {
        $('.ch-choose-content').slideUp();
        return false;
    });

    $(document).click(function(e){
        if( ($(e.target).closest('.cf-choose-trigger').length < 1) && ($(e.target).closest('.ch-choose-content').length < 1) && (chooseTrigger = 1) ) {
            chooseTrigger = 0;
            $('.ch-choose-content').slideUp();   
        }
    });

    $("#newsletter-form").submit(function(e) {

        var chooseError = $(this).find(".cf-choose-error");

        // Check if nothing is written in input
        if ( $('.field-email').val().length < 1) {
            $('.field-email-error').removeClass("dn");
            e.preventDefault();
        }
        if ( $('.field-name').val().length < 1) {
            $('.field-name-error').removeClass("dn");
            e.preventDefault();
        }
        if ( $('.field-city').val().length < 1) {
            $('.field-city-error').removeClass("dn");
            e.preventDefault();
        }

        // Check if casino is chosen
        if ($(".casino-dropdown").val() === "") {
            errorContainer.removeClass('is-hidden');
            $('.error-complaint-casino').removeClass('is-hidden');
            $('.casino-dropdown').addClass('form-dropdown--error');
            e.preventDefault();
        }

        if ( checkboxNumber < 1) {
            chooseError.removeClass('dn');
            e.preventDefault();
        }

    });



    $("#contact-form").submit(function(e) {

        $("#contact-form input").each( function() {
            if ( $(this).val().length < 1) {
                $(this).next().removeClass("dn");
                e.preventDefault();
            }
        });

        $("#contact-form textarea").each( function() {
            if ( $(this).val().length < 1) {
                $(this).next().removeClass("dn");
                e.preventDefault();
            }
        });
        
    });

});