var $pageflip = $('#pageflip');

/* Initialize & Start Pageflip 5 */
$pageflip.pageflipInit( {
    /* Configuration options */
    PageDataFile: "/akcija-content.html",
    PageWidth: 400,
    PageHeight: 551,
    FullScreenEnabled: false,
    Margin: 34,
    MarginBottom: 64,
    AutoScale: true,
    AutoStageHeight: true,
    StartPage: 1,
    AutoFlipLoop: -1,
    ControlbarFile: "/common/controlbar_svg.html",
    ControlbarToFront: true,
    HashControl: true,
    ShareLink: "http://www.okov.me",
    ShareText: "OKOV",
    ShareVia: "@okovme",
    ShareImageURL: "http://pageflip-books.com/images/shareimage2.jpg",
    Copyright: Key.Copyright,
    Key: Key.Key,
    PagerText: "Strana #~Strane #–#",
    ZoomEnabled: false
    /* book ID - used as CSS class name */
}, "book" );