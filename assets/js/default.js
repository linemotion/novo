$(function(){

    // responsive navigation stuff:
    var pathArray = window.location.pathname.split( '/' );
    var pathLanguage = pathArray[1];
    if (pathLanguage == "mn") {
       $('.site-logo').after('<div class="show-menu-cont"><a href="#" class="show-menu group"><i class="icon icon-menu"></i> Navigacija</a></div>');
    }
    else {
       $('.site-logo').after('<div class="show-menu-cont"><a href="#" class="show-menu group"><i class="icon icon-menu"></i> Navigation</a></div>');
    }
    $('<div class="menu-overlay"><a href="#" class="hide-menu"><i class="icon icon-close"></i></a></div>').appendTo('body');
    $(".site-nav ul").clone().appendTo(".menu-overlay");

     $(".show-menu-cont a").click(function() {
        $(".menu-overlay").fadeIn();
        return false;
    });
    $(".hide-menu").click(function() {
        $(".menu-overlay").fadeOut();
        return false;
    });

    // search stuff:

    searchTrigger = 0;

    var openSearch = function() {
        searchTrigger = 1;
        $('.search-input').animate({width: 'toggle'});
        $('.search-btn-submit').show();

    };

    var closeSearch = function() {
        searchTrigger = 0;
        $('.search-input').fadeOut();
        $('.search-btn-submit').hide();

    }

     var switchSearch = function() {
        if(searchTrigger==0){
             openSearch();
        }else{
             closeSearch();
        };
    };

    $(".search-btn").click(function(){
        switchSearch();
        return false;
    });

    // $(document).mouseup(function (e)
    // {
    //     var container = $(".search-container");

    //     if (!container.is(e.target)
    //         && container.has(e.target).length === 0)
    //     {
    //         closeSearch();
    //     }
    // });

    $("#choose-kind").change(function ()
    {
        var pathArray = window.location.pathname.split( '/' );
        var sort = $(this).val();

        var lang = pathArray[1];
        var category = pathArray[3];
        var subcategory = pathArray[4];

        if (sort == "all-kinds") {window.location.replace("/" + lang + "/proizvodi/" + category + "/" + subcategory);}
        else  {window.location.replace("/" + lang + "/proizvodi/" + category + "/" + subcategory + "/" + sort + "/p/0" );}  
    });

    $("#choose-kind-2").change(function ()
    {
        var pathArray = window.location.pathname.split( '/' );
        var sort = $(this).val();

        var lang = pathArray[1];
        var category = pathArray[3];
        var subcategory = pathArray[4];

        if (sort == "all-kinds") {window.location.replace("/" + lang + "/proizvodi/" + category + "/" + subcategory);}
        else  {
            window.location.replace("/" + lang + "/proizvodi/" + category + "/" + subcategory + "/" + sort + "/p/0" );
        }  
    });

    $("#choose-filter-group").change(function ()
    {
        var pathArray = window.location.pathname.split( '/' );
        var group = $(this).val();

        var lang = pathArray[1];

        if (group == "all-groups") {window.location.replace("/" + lang + "/proizvodi/rasprodaja/");}
        else  {window.location.replace("/" + lang + "/proizvodi-rasprodaja/" + group);}  
    });
    $("#choose-filter-group2").change(function ()
    {
        var pathArray = window.location.pathname.split( '/' );
        var group = $(this).val();

        var lang = pathArray[1];

        if (group == "all-groups") {window.location.replace("/" + lang + "/proizvodi");}
        else  {window.location.replace("/" + lang + "/proizvodi-akcija/" + group);}  
    });



});