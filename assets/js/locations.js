$(function() {

      var pg = new google.maps.LatLng(41.924832, -87.697456),
          pointToMoveTo,
          first = true,
          curMarker = new google.maps.Marker({}),
          $el;

      var myOptions = {
          zoom: 15,
          center: pg,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

      var map = new google.maps.Map($("#map_canvas")[0], myOptions);

      $("#locations li").click(function() {

        $el = $(this);

        if (!$el.hasClass("snm-selected")) {

          $("#locations li").removeClass("snm-selected");
          $el.addClass("snm-selected");

          if (!first) {

            // Clear current marker
            curMarker.setMap();

            // Set zoom back to pg level
            // map.setZoom(10);
          }

          // Move (pan) map to new location
          pointToMoveTo = new google.maps.LatLng($el.attr("data-geo-lat"), $el.attr("data-geo-long"));
          map.panTo(pointToMoveTo);

          // Add new marker
          curMarker = new google.maps.Marker({
              position: pointToMoveTo,
              map: map,
              icon: "/assets/img/pin.png"
          });

          // On click, zoom map
          google.maps.event.addListener(curMarker, 'click', function() {
             map.setZoom(14);
          });

          // Fill more info area
          $(".map-container")
            // .find("h2")
            //   .html($el.find("h3").html())
            //   .end()
            .find(".map-info")
              .html($el.find(".longdesc").html());

          // No longer the first time through (re: marker clearing)
          first = false;
        }

        return false;

      });

      $("#locations li:first").trigger("click");

    });