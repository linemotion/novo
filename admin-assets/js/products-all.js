var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

$(".sortable").sortable({
    helper: fixHelper,
    handle: '.move-btn'
}).disableSelection();


$(".sortable-small").sortable({
    helper: fixHelper,
        stop: function( ) {
 			var order = $(".sortable-small").sortable("toArray");

        $.post( "/filter_values/sort", { values: order })
              .done(function( data ) {
          });
        }
}).disableSelection();

$(".filter-value-edit").click(function(e) {
       name = $(this).parent().text();
       $('#click').replaceWith('<input id="click" type="text" name="name" class="txtinput" value="' + name + '" data-required="true">');
    });