$(document).ready(function() {

    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $(".sortable").sortable({
        helper: fixHelper,
        handle: '.move-btn'
    }).disableSelection();

    CKEDITOR.replace( 'editor', {
        customConfig: '/ckeditor/config-okov.js'
    });

    CKEDITOR.replace( 'editor2', {
        customConfig: '/ckeditor/config-okov.js'
    });

});
