$(document).ready(function() {

    // discount JS:

    var inputDisc = $('input.popust-control');
    function checkDisc() {
        if(inputDisc.is(":checked")) {
            $(".popust-window").slideDown();
        } else {
            $(".popust-window").slideUp();
        }
    }
    inputDisc.change(function() {
        checkDisc();
        $('.action-control').prop('checked', false);
        $('.sales-control').prop('checked', false);
        checkAction();
        checkSales();
    });
    checkDisc();
    $('.discount').change(function () {
        $('.new-price').val( $('.db-price').val() - $('.db-price').val() * $(this).val()/100 );
    });
    $('.new-price').change(function () {
        $('.discount').val( 100 - $(this).val()/$('.db-price').val() * 100 );
    });

    // action JS:

    var inputAction = $('input.action-control');
    function checkAction() {
        if(inputAction.is(":checked")) {
            $(".action-window").slideDown();
        } else {
            $(".action-window").slideUp();
        }
    }
    inputAction.change(function() {
        checkAction();
        $('.popust-control').prop('checked', false);
        $('.sales-control').prop('checked', false);
        $('.posto-control').prop('checked', false);
        checkDisc();
        checkSales();
        checkPosto();
    });
    checkAction();
    $('.discount-action').change(function () {
        $('.new-price-action').val( $('.db-price').val() - $('.db-price').val() * $(this).val()/100 );
    });
    $('.new-price-action').change(function () {
        $('.discount-action').val( 100 - $(this).val()/$('.db-price').val() * 100 );
    });


    // sales JS:

    var inputSales = $('input.sales-control');
    function checkSales() {
        if(inputSales.is(":checked")) {
            $(".sales-window").slideDown();
        } else {
            $(".sales-window").slideUp();
        }
    }
    inputSales.change(function() {
        checkSales();
        $('.popust-control').prop('checked', false);
        $('.action-control').prop('checked', false);
        $('.posto-control').prop('checked', false);
        checkDisc();
        checkAction();
        checkPosto();
    });
    checkSales();
    $('.discount-sales').change(function () {
        $('.new-price-sales').val( $('.db-price').val() - $('.db-price').val() * $(this).val()/100 );
    });
    $('.new-price-sales').change(function () {
        $('.discount-sales').val( 100 - $(this).val()/$('.db-price').val() * 100 );
    });


    // posto JS:

    var inputPosto = $('input.posto-control');
    function checkPosto() {
        if(inputPosto.is(":checked")) {
            $(".posto-window").slideDown();
        } else {
            $(".posto-window").slideUp();
        }
    }
    inputPosto.change(function() {
        checkPosto();
        $('.popust-control').prop('checked', false);
        $('.action-control').prop('checked', false);
        $('.sales-control').prop('checked', false);
        checkDisc();
        checkAction();
        checkSales();
    });
    checkPosto();
    $('.discount-posto').change(function () {
        $('.new-price-posto').val( $('.db-price').val() - $('.db-price').val() * $(this).val()/100 );
    });
    $('.new-price-posto').change(function () {
        $('.discount-posto').val( 100 - $(this).val()/$('.db-price').val() * 100 );
    });




    $('#sort').change(function () {

        var id = $(this).val();

        $.post( "/sorts/ajaxSpecs", { id: id })
              .done(function( data ) {

            $("#specs").html(data);
          });

        $.post( "/sorts/ajaxFilters", { id: id })
              .done(function( data ) {

            $("#filters").html(data);
            $(".chosen-select").chosen({ width: '100%', disable_search_threshold: 10 });
          });
        //$('.discount').val( $(this).val()/$('.db-price').val() * 100 );
    });
    $(".chosen-select").chosen({ width: '100%', disable_search_threshold: 10 });

    $(".chosen-img").fancybox();

    $( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy." });

    CKEDITOR.replace( 'editor', {
        customConfig: '/ckeditor/config-okov.js'
    });

    CKEDITOR.replace( 'editor2', {
        customConfig: '/ckeditor/config-okov.js'
    });

});
