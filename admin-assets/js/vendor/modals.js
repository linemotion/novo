/**
 * modalEffects.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var ModalEffects = (function() {

	function init() {

		var overlay = document.querySelector( '.md-overlay' );

		[].slice.call( document.querySelectorAll( '.md-trigger' ) ).forEach( function( el, i ) {

			var modal = document.querySelector( '#' + el.getAttribute( 'data-modal' ) ),
				close = modal.querySelector( '.md-close' );

			function removeModal( hasPerspective ) {
				classie.remove( modal, 'md-show' );

				if( hasPerspective ) {
					classie.remove( document.documentElement, 'md-perspective' );
				}
			}

			function removeModalHandler() {
				removeModal( classie.has( el, 'md-setperspective' ) ); 
			}

			el.addEventListener( 'click', function( ev ) {



//-----------------------------------------
				if($(this).attr("data-controller") != undefined)
				{
					var id = $(this).attr("data-id");
					var controller = $(this).attr("data-controller");

					$("#delete_cat_form").attr("action","/" + controller + "/delete/" + id);
				}

				if($(this).attr("data-spec") != undefined)
				{
					var id = $(this).attr("data-id");

					$("#edit_form").attr("action","/specifications/edit_proccess/" + id);
				}


				if($(this).attr("data-filter") != undefined)
				{
					var id = $(this).attr("data-id");

					$("#edit_form").attr("action","/filters/edit_proccess/" + id);
				}

				if($(this).attr("data-manuf") != undefined)
				{
					var id = $(this).attr("data-id");

					$("#edit_form").attr("action","/manufacturers/edit_proccess/" + id);
				}

				if($(this).attr("data-value") != undefined)
				{
					var id = $(this).attr("data-id");

					$("#value_form").attr("action","/filter_values/create_proccess/" + id);
				}
				if($(this).attr("data-value-edit") != undefined)
				{
					var id = $(this).attr("data-id");

					$("#edit_filter_value").attr("action","/filter_values/edit_proccess/" + id);
				}
//-----------------------------------------------




				classie.add( modal, 'md-show' );
				overlay.removeEventListener( 'click', removeModalHandler );
				overlay.addEventListener( 'click', removeModalHandler );

				if( classie.has( el, 'md-setperspective' ) ) {
					setTimeout( function() {
						classie.add( document.documentElement, 'md-perspective' );
					}, 25 );
				}
			});

			close.addEventListener( 'click', function( ev ) {
				ev.stopPropagation();
				removeModalHandler();
			});

		} );

	}

	init();

})();