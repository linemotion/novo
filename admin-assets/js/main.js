$(document).ready(function() {
    $(".md-trigger").click(function(e) {
       e.preventDefault();
    });
    $(".md-close").click(function(e) {
       e.preventDefault();
    });
    $('<a href="#" class="show-hide-menu"><i class="fa fa-lg fa-bars"></i></a>').appendTo('.sidebar-header');
    $(".show-hide-menu").click(function() {
        $(".sidebar-nav").slideToggle();
        return false;
    });
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};
 

$(".sortable").sortable({
    helper: fixHelper,
    handle: '.move-btn',
        stop: function( ) {
      var order = $(".sortable").sortable("toArray");
      var controller = $(".sortable").attr("data-controller");      

        $.post( "/" + controller + "/sort", { values: order })
              .done(function( data ) {
          });
        }
}).disableSelection();



});
