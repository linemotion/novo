$(document).ready(function() {


    $(".copy-text-link").click(function(e) {
       var imageUrl = $(this).parent().parent().children("img").prop("src");
       $("#img-address").val(imageUrl);
    });

    $(".fancybox").fancybox({
	    afterShow: function() {
	        $("#img-address").select();
	    }
	});

    $( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy." });

    CKEDITOR.replace( 'editor', {
        customConfig: '/ckeditor/config-okov.js'
    });

    CKEDITOR.replace( 'editor2', {
        customConfig: '/ckeditor/config-okov.js'
    });

});
