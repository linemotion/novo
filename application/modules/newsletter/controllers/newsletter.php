<?php

class Newsletter extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_newsletter');
	}

	function sidebar()
	{
		$this->load->view('sidebar');
	}
	function widget()
	{
		$this->load->view('newsletter');
	}

	function admin_old()
	{
		$data["mails"] = $this->get_old("id");
		echo modules::run('template/admin_render',"newsletter","admin",$data);
	}
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$data['aplication'] = array_shift($this->get_where_custom("name","aplication"));

		echo modules::run('template/admin_render',"newsletter","anewsletter");
	}
	function index()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("newsletter"),"link" => "/")
			); 
		$data["title"] = $this->lang->line("newsletter");
		
		//$data['locations'] = modules::run("Locations/get","id");

		echo modules::run('template/render',"newsletter","index",$data);
	}
	function create_proccess()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("newsletter"),"link" => "/")
			); 

		$data["title"] = $this->lang->line("newsletter");
		$news['name'] = $this->input->post('name');
		$news['city'] = $this->input->post('city');
		$news['phone'] = $this->input->post('phone');
		$news['mail'] = $this->input->post('mail');
		$news['jmbg'] = $this->input->post('jmbg');
		$news['occupation'] = $this->input->post('occupation');
		$news['timestamp'] = date('Y-m-d',time());

		$this->_insert($news);
		$ref["new_id"] = $this->get_max();

		foreach ($this->input->post('cats') as $id) 
		{
			$ref["cat_id"] = $id;
			$this->db->insert("new_cat",$ref);
		}



		echo modules::run('template/render',"newsletter","ty",$data);
		
	}
	function flash()
	{
		$this->load->library('session');
		$this->session->set_flashdata('mail', $this->input->post('mail'));		

		echo "/kontakt/newsletter";
		
	}

	function delete()
	{
		$this->_delete($this->uri->segment(3));
		redirect('/subcategories/edit/'.$this->uri->segment(4));
	}

	function get_old($order_by)
	{
		$tags = $this->mdl_newsletter->get_old($order_by);
		return $tags;
	}
	function get($order_by)
	{
		$tags = $this->mdl_newsletter->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_newsletter->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_newsletter->get_where($id);
		return $category;
	}

	function get_cats($id)
	{
		$this->db->where("new_id", $id);
		$query = $this->db->get("new_cat");
		return $query->result();
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_newsletter->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_newsletter->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_newsletter->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_newsletter->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_newsletter->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_newsletter->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_newsletter->_custom_tags($mysql_tags);
		return $tags;
	}

}