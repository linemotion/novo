        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/kontakt">Kontakt</a></li>
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/kontakt/newsletter">Newsletter</a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <p class="contact-intro">
                        Josipa Broza Tita 26<br>
                        81000 Podgorica, Crna Gora<br>
                        +382 20 658 501
                    </p>

                    <p class="contact-intro">
                        <a href="mailto:okov@okov.me"><i class="icon icon-envelope"></i> okov@okov.me</a>
                        <a href="http://www.facebook.com/okov"><i class="icon icon-facebook"></i> facebook.com/okovdoo</a>
                    </p>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <h1 class="section-subtitle">
                        Hvala!
                    </h1>

                    <p class="section-intro">
                        Vaša prijava je prihvaćena.<br>
                    </p>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
