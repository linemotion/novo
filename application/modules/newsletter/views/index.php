

        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/kontakt">Kontakt</a></li>
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/kontakt/newsletter">Newsletter</a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <p class="contact-intro">
                        Josipa Broza Tita 26<br>
                        81000 Podgorica, Crna Gora<br>
                        +382 20 658 501
                    </p>

                    <p class="contact-intro">
                        <a href="mailto:okov@okov.me"><i class="icon icon-envelope"></i> okov@okov.me</a>
                        <a href="http://www.facebook.com/okov"><i class="icon icon-facebook"></i> facebook.com/okovdoo</a>
                        <a href="http://www.twitter.com/okovdoo"><i class="icon icon-twitter"></i> twitter.com/okovdoo</a>
                    </p>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <h1 class="section-subtitle">
                        Prijava na Newsletter
                    </h1>

                    <p class="section-intro">
                        Ukoliko želite da dobijate redovne informacije o specijalnim ponudama, molimo vas da unesete podatke.<br>
                        <b>*</b> Polja obeležena zvezdicom su obavezna.
                    </p>

                        <form action="/<?php echo $this->uri->segment(1) ?>/newsletter/create_proccess/" data-parsley-validate class="contact-form" id="newsletter-form" method="POST">

                        <div class="cf-field">
                            <label class="cf-required"><?=$this->lang->line('E-Mail')?>:</label>
                            <input name="mail" type="text" value="<?php echo $this->session->flashdata('mail'); ?>" class="field-email">
                            <div class="cf-error field-email-error dn">
                                <?php if ($this->uri->segment(1) == "en"): ?>
                                    You have to enter an e-mail.
                                <?php else: ?>
                                    Morate uneti e-mail adresu.
                                <?php endif ?>
                            </div>
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label class="cf-required"><?=$this->lang->line('Full_Name')?>:</label>
                            <input name="name" type="text" class="field-name">
                            <div class="cf-error field-name-error dn">
                                <?php if ($this->uri->segment(1) == "en"): ?>
                                    You have to enter first and last name.
                                <?php else: ?>
                                    Morate uneti ime i prezime.
                                <?php endif ?>
                            </div>
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label class="cf-required"><?=$this->lang->line('city')?></label>
                            <input name="city" type="text" class="field-city">
                            <div class="cf-error field-city-error dn">
                                <?php if ($this->uri->segment(1) == "en"): ?>
                                    You have to enter your city.
                                <?php else: ?>
                                    Morate uneti grad.
                                <?php endif ?>
                            </div>
                        </div> <!-- .cf-field -->

                        <div class="cf-field cf-field-choose">
                            <label class="cf-required"><?=$this->lang->line('Products_Interested')?></label>
                            <div class="cf-choose">
                                <div class="cf-choose-trigger field-choose">
                                    <?php if ($this->uri->segment(1) == "en"): ?>
                                        You have chosen <b class="checkbox-number">0</b> product groups.
                                    <?php else: ?>
                                        Izabrano je <b class="checkbox-number">0</b> grupa proizvoda.
                                    <?php endif ?>
                                </div>
                                <div class="ch-choose-content dn">
                                    <a href="#" class="ch-choose-close">x</a>
                                    <ul>
                                    <?php foreach (modules::run("categories/get","position") as $cat): ?>
                                        <li><label><input type="checkbox" name="cats[]" class="cf-checkbox" value="<?php echo $cat->id ?>"> <?php echo $cat->name ?></label></li>                                   
                                    <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="cf-choose-error cf-error dn">
                            <?php if ($this->uri->segment(1) == "en"): ?>
                                You must choose at least one product gorup.
                            <?php else: ?>
                                Morate izabrati bar jednu grupu proizvoda.
                            <?php endif ?>
                        </div>

                        <div class="cf-field">
                            <label class="cf-optional"><?=$this->lang->line('Telephone_Number')?>:</label>
                            <input name="phone" type="text">
                        </div>

                        <div class="cf-field">
                            <label class="cf-optional"><?=$this->lang->line('Personal_Identity_Number')?>:</label>
                            <input name="jmbg" type="text">
                        </div>

                        <div class="cf-field">
                            <label class="cf-optional"><?=$this->lang->line('Occupation')?>:</label>
                            <input name="occupation" type="text">
                        </div>

                        <button type="submit" class="btn-default">
                            Pošalji <i class="icon icon-arrow-right"></i>
                        </button>

                    </form>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->


