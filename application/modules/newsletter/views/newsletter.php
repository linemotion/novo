                    <div class="col-1-2">
                        <div class="home-info-item">
                            <h4 class="hi-title">Newsletter</h4>
                            <?php if ($this->uri->segment(1) == "mn"): ?>
                                <p class="hi-text">Ukoliko želite da dobijate redovne informacije o popustima i akcijama, molimo vas da unesete vašu e-mail adresu.</p>
                            <?php else: ?>
                                <p class="hi-text">If you want to get information on discounts and sales, please type in your e-mail address.</p>
                            <?php endif ?>
                            <div class="hi-action newsletter">
                                <form>
                                    <input type="email" name="mail" placeholder="Vaš e-mail" required> <button id="submit-newsletter" class="btn-default" type="submit"><?=$this->lang->line('send')?> <i class="icon icon-arrow-right"></i></button>
                                </form>
                            </div> <!-- .hi-action -->
                            <div style="display:none" class="newsletter-result"><?=$this->lang->line('thank')?></div>
                            <img src="/assets/img/newsletter.svg" alt="" class="hi-img">
                        </div> <!-- .home-newsletter -->
                    </div>