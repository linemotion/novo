        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                <?php echo modules::run("newsletter/sidebar") ?>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle">Prijavite se!</h1>
                        

                            <div class="tab tab-1">

                                <form action="/<?php echo $this->uri->segment(1) ?>/newsletter/create_proccess/" data-parsley-validate class="contact-form" method="POST">

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('E-Mail')?>:</label>
                                        <input name="mail" type="text" required>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Full_Name')?>:</label>
                                        <input name="name" type="text" required>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('city')?></label>
                                        <input name="city" type="text" required>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Telephone_Number')?>:</label>
                                        <input name="phone" type="text" required>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Personal_Identity_Number')?>:</label>
                                        <input name="jmbg" type="text" required>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Occupation')?>:</label>
                                        <input name="occupation" type="text" required>
                                    </div> <!-- .cf-field -->
                                    
                                    <?php if ($this->uri->segment(1) == "mn"): ?>
                                        <p class="cf-required">Sva polja su obavezna.</p>
                                        <blockquote class="cf-disclaimer">Saglasan/na sam s tim da Okov d.o.o. može čuvati, obrađivati i koristiti moje lične podatke u svrhu izdavanja i korišćenja Posto kartice, kao i za slanje promotivnih obavještenja o pogodnostima.</blockquote>
                                    <?php else: ?>
                                        <p class="cf-required">All fields are required</p>
                                        <blockquote class="cf-disclaimer">I agree that Okov d.o.o. can store, process and use my personal data for the purpose of issuing and using Posto card, as well as for sending promotional materials.</blockquote>
                                    <?php endif ?>

                            
                                    

                                    <button type="submit" class="btn-default btn-big-form">
                                        <?=$this->lang->line('Sendit')?>
                                    </button>

                                </form>

                            </div>

                    </article>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->