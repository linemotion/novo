                    <div class="sidebar-window sidebar-window-newsletter group">
                        <h3 class="sw-title">Newsletter</h3>
                            <?php if ($this->uri->segment(1) == "mn"): ?>
                                <p class="sw-text">Ukoliko želite da dobijate redovne informacije o popustima i akcijama, molimo vas da unesete vašu e-mail adresu.</p>
                            <?php else: ?>
                                <p class="sw-text">If you want to get information on discounts and sales, please type in your e-mail address.</p>
                            <?php endif ?>
                        <div class="sidebar-newsletter group newsletter">
                            <div class="sn-input">
                                <input type="email" name="mail" placeholder="Vaš e-mail">
                            </div>
                            <button type="submit" id="submit-newsletter" class="btn-arrow"><i class="icon icon-arrow-right"></i></button>
                        </div>
                        <div style="display:none" class="newsletter-result"><?=$this->lang->line('thank')?></div>

                    </div> <!-- .sidebar-window -->