<?php /**
* 
*/

class Cron extends MX_Controller 
{
 
      function __construct()
      {
        parent::__construct();
      }
 
      function action()
      {
            $time = time();

            $actions = modules::run("products/get_where_custom","action",1);
            $sales = modules::run("products/get_where_custom","sales",1);
            $discount = modules::run("products/get_where_custom","discount",1);
            $percents = modules::run("products/get_where_custom","percent",1);

            if ($sales) 
                  foreach ($sales as $sale) 
                  {
                        $tmp = strtotime($sale->sales_end);

                        if ($tmp < $time && $tmp!="") 
                        {
                           $sale_data["sales_end"] = "";
                           $sale_data["sales"] = 0;

                           modules::run("products/_update",$sale->id,$sale_data);
                        }
                  }
            if($discount)
                  foreach ($discount as $dis) 
                  {
                        $tmp = strtotime($dis->discount_end);
                        if ($tmp < $time && $tmp!="") 
                        {
                           $dis_data["discount_end"] = "";
                           $dis_data["discount"] = 0;

                           modules::run("products/_update",$dis->id,$dis_data);
                        }
                  }

            if($actions)
                  foreach ($actions as $action) 
                  {
                        $tmp = strtotime($action->action_end);

                        if ($tmp < $time && $tmp!="") 
                        {
                           $action_data["action_end"] = "";
                           $action_data["action"] = 0;

                           modules::run("products/_update",$action->id,$action_data);   
                        }
                  }
            if($percents)
                  foreach ($percents as $percent) 
                  {
                        $tmp = strtotime($percent->percent_end);

                        if ($tmp < $time && $tmp!="") 
                        {
                           $percent_data["percent_end"] = "";
                           $percent_data["percent"] = 0;

                           modules::run("products/_update",$percent->id,$percent_data);   
                        }
                  }
      }
}