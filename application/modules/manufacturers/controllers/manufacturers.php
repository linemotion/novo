<?php

class Manufacturers extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_manufacturers');
	}

	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data["manufacturers"] = modules::run("manufacturers/get","id"); 
		echo modules::run('template/admin_render',"manufacturers","admin",$data);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$data['name'] = $this->input->post('name');

		$id = $this->_insert($data);

		redirect('/manufacturers/admin/'.$id);
	}
	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);

		$data['name'] = $this->input->post('name');
		
		$this->_update($id,$data);

		redirect('/manufacturers/admin/');
	}

	function change()
	{

		$id = $this->input->post('id');
		$output = '<select name="tags" class="uniformselect" id="tag"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('subcat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$this->_delete($this->uri->segment(3));
		redirect('/manufacturers/admin/');
	}

	function get($order_by)
	{
		$tags = $this->mdl_manufacturers->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_manufacturers->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_manufacturers->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_manufacturers->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_manufacturers->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_manufacturers->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_manufacturers->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_manufacturers->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_manufacturers->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_manufacturers->_custom_tags($mysql_tags);
		return $tags;
	}

}