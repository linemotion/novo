
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

                <nav class="sidebar-nav">
                    <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li  class="sidebar-nav-selected">
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                    </ul>
                </nav>


            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Proizvođači</h2>
                    <a href="#" class="btn-add md-trigger" data-modal="modal-add-group"><i class="fa fa-plus-circle"></i> Dodaj proizvođača</a>
                </div>

                <div class="c-block">

                    <table class="main-table">
<?php if (modules::run("manufacturers/get","id")): ?>
                        <caption class="tab-title">GRUPE PROIZVOĐAČA</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime proizvođača</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody>
<?php foreach (modules::run("manufacturers/get","id") as $manufacturer): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <?php echo $manufacturer->name; ?>
                                </td>
                                <td class="td-action">
                                    <a href="/manufacturers/delete/<?php echo $manufacturer->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-manuf" data-id="<?php echo $manufacturer->id; ?>" id="delete_cat_action" data-controller="manufacturers"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/manufacturers/edit/<?php echo $manufacturer->id; ?>" class="act-btn edit-btn md-trigger" data-modal="modal-edit-manuf" data-manuf="true" data-id="<?php echo $manufacturer->id; ?>"><i class="fa fa-pencil"></i>Izmeni</a>
                                </td>
                            </tr>
<?php endforeach ?>
<?php else: ?>
                    <table class="main-table">
                        <caption class="tab-title">Proizvođači</caption>
                    </table>

                    <div class="content-blank">

                        <p>Lista proizvođača je prazna. Molimo Vas da dodate proizvođača.</p>

                    </div> <!-- .content-blank -->
<?php endif; ?>
                   </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->


        <div class="md-modal md-effect-1" id="modal-add-group">
            <div class="md-content">
                <h3>Dodaj proizvođača <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime proizvođača</h4>

                        <form action="/manufacturers/create_proccess" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite proizvođača" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-manuf">
            <div class="md-content md-content-del">
                <h3>Obriši proizvođača <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovog proizvođača?</h4>

                        <form  method="post" id="delete_cat_form" action="">
                            <div class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-edit-manuf">
            <div class="md-content md-content-edit">
                <h3>Izmeni proizvođača <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime proizvođača</h4>

                        <form action="" method="post" id="edit_form">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
