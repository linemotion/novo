                    <div class="pagination">

                        <ul>
                            <?php if ($this->paginate): ?>
                                <?php echo $this->paginate->create_links();?>       
                            <?php endif; ?>  

                        </ul>
                        <ul>
                            <li class="page-selected"><a>1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#" class="next"><i class="icon icon-arrow-right"></i></a></li>
                        </ul>
                    </div> <!-- .pagination -->
