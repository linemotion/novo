<?php

class Pagination extends MX_Controller
{


    function __construct()
    {
		parent::__construct();
		$this->load->library('paginate');
    }

	public function paginateView($module="",$method="",$item="products")
	{
    	$offset = $this->uri->segment(5);
    	$id = $this->uri->segment(4);

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$module.'/'.$method.'/'.$id;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run($item."/get_where_custom",'subcat_id',$id));
		$config['uri_segment'] = 5;
		$config['num_links'] = 10;

		$config['display_pages'] = FALSE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$this->paginate->initialize($config);

		($this->uri->segment(5)) ? $data["offset_pg"] = $this->uri->segment(5) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];
		$data['counter_pg'] = $data["offset_pg"] + count(modules::run("products/get_where_paginateView",$id,1,$this->uri->segment(5)));

		$this->load->view('pagination',$data);
	}
	public function paginateAction()
	{
    	$offset = $this->uri->segment(4);

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/akcija';
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run("products/get_by_action"));
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);
	}	

	public function paginateFront()
	{
    	$subcat_url = $this->uri->segment(4);
		$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(3));

		//uzas jebeni
		$this->db->where("cat_id", $data['category']->id);
		$this->db->where('url',$subcat_url);
		$query = $this->db->get("subcategories");
		//kraj uzasa


		$subcategory = $query->row();

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/'.modules::run("categories/get_by_id",$subcategory->cat_id)->url.'/'.$subcategory->url;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run("products/get_where_custom",'subcat_id',$subcategory->id));
		$config['uri_segment'] = 5;
		$config['num_links'] = 5;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		// $config['prev_tag_open'] = '<li class="page-prev">';
		// $config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(5)) ? $data["offset_pg"] = $this->uri->segment(5) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginate_sales()
	{
		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/rasprodaja/';
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_num_sales");
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		// $config['prev_tag_open'] = '<li class="page-prev">';
		// $config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_products',$data);

	}

	public function paginateFrontSort()
	{
		$url = $this->uri->segment(5);
		
		$sort_id = array_pop(explode("-", $url));
		$data['sort'] = modules::run("/sorts/get_by_id",$sort_id);

		//$data['sort'] =modules::run("sorts/get_by_url",$this->uri->segment(4));

		//uzas jebeni
		$this->db->where('id',$data['sort']->subcat_id);
		$query = $this->db->get("subcategories");
		//kraj uzasa


		$subcategory = $query->row();

		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/'.modules::run("categories/get_by_id",$subcategory->cat_id)->url.'/'.$subcategory->url."/".$data['sort']->url."-".$data["sort"]->id."/p/";
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run("products/get_where_custom",'sort_id',$data['sort']->id));
		$config['uri_segment'] = 7;
		$config['num_links'] = 5;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		// $config['prev_tag_open'] = '<li class="page-prev">';
		// $config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(7)) ? $data["offset_pg"] = $this->uri->segment(7) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginateFrontFilter()
	{
    	$sort_url = $this->uri->segment(5);

		$sort_id = array_pop(explode("-", $sort_url));
		$data['sort'] = modules::run("sorts/get_by_id",$sort_id);

		//$data['sort'] =modules::run("sorts/get_by_url",$this->uri->segment(4));

		//uzas jebeni
		$this->db->where('id',$data['sort']->subcat_id);
		$query = $this->db->get("subcategories");
		//kraj uzasa


		$subcategory = $query->row();
		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/'.modules::run("categories/get_by_id",$subcategory->cat_id)->url.'/'.$subcategory->url."/".$data['sort']->url."-".$data['sort']->id."/f/";
    	$config['per_page'] = 12;

		//recipes
		//end recipes
		$config['total_rows'] = count(modules::run("products/get_where_filter"));
		$config['uri_segment'] = 7;
		$config['num_links'] = 5;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		// $config['prev_tag_open'] = '<li class="page-prev">';
		// $config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(7)) ? $data["offset_pg"] = $this->uri->segment(7) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}

	public function paginateFrontCategory()
	{
		$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(3));

		//pagination---------------------------------------
		//$this->load->library('paginate');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/'.$data['category']->url."/";
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = count(modules::run("products/get_where_custom",'cat_id',$data['category']->id));
		$config['uri_segment'] = 4;
		$config['num_links'] = 5;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginateFrontAll()
	{
		//$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(2));

		//pagination---------------------------------------
		//$this->load->library('paginate');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/';
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_where_paginateFrontAll_rows");
		$config['uri_segment'] = 3;
		$config['num_links'] = 5;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(3)) ? $data["offset_pg"] = $this->uri->segment(3) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginateFrontAllAction()
	{
		//$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(2));

		//pagination---------------------------------------
		//$this->load->library('paginate');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/akcija/';
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_where_paginateFrontAllAction_rows");
		$config['uri_segment'] = 3;
		$config['num_links'] = 5;

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		//config previous links
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(3)) ? $data["offset_pg"] = $this->uri->segment(3) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_action',$data);

	}
	public function paginate_search_products()
	{
    	$search = $this->uri->segment(4);


		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/'.$this->uri->segment(1).'/proizvodi/pretraga/'.$search;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_num_search",$search);
		$config['uri_segment'] = 5;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(5)) ? $data["offset_pg"] = $this->uri->segment(5) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_products',$data);

	}
	public function paginate_search_categories()
	{
    	$search = $this->uri->segment(3);


		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/grupe/pretraga/'.$search;
    	$config['per_page'] = 10;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("categories/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 2;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_categories',$data);

	}
	public function paginate_search_subcategories()
	{
    	$search = $this->uri->segment(3);


		//pagination---------------------------------------
		$this->load->library('pagination');
		

		$config['base_url'] = '/grupe/pretraga/'.$search;
    	$config['per_page'] = 2;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("categories/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];

		$this->load->view('paginate_search_categories',$data);

	}
	public function paginate_search_products_admin($search)
	{
		
		//pagination---------------------------------------
		$this->load->library('pagination');

		$config['base_url'] = '/products/asearch/'.$search;
    	$config['per_page'] = 12;

		//recipes

		//end recipes
		$config['total_rows'] = modules::run("products/get_num_search",$search);
		$config['uri_segment'] = 4;
		$config['num_links'] = 10;

		$config['next_tag_open'] = '<li class="page-next">';
		$config['next_tag_close'] = '</li>';
		//config previous links
		$config['prev_tag_open'] = '<li class="page-prev">';
		$config['prev_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="page-selected"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		// config last link
		$config['last_link'] = false;
		$config['first_link'] = FALSE;
		
		$this->paginate->initialize($config);

		($this->uri->segment(4)) ? $data["offset_pg"] = $this->uri->segment(4) : $data['offset_pg'] = 0;
		$data['total_rows_pg'] = $config['total_rows'];
		$data['counter_pg'] = $data["offset_pg"] + count(modules::run("products/get_where_paginateSearch",$search,12,$this->uri->segment(4,0)));

		$this->load->view('pagination',$data);

	}				
}
