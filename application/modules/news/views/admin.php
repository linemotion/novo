        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>
            
                </ul>
            </nav>


            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">
                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Novosti</h2>
                    <a href="/news/create" class="btn-add"><i class="fa fa-plus-circle"></i> Dodaj novost</a>
                </div>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">
                            Lista novosti
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime novosti</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">

<?php foreach ($news as $new): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/news/edit/<?php echo $new->id ?>"><?php echo $new->name ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="#" class="act-btn del-btn md-trigger" data-modal="modal-del-news" data-id="<?php echo $new->id ?>" data-controller="news"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/news/edit/<?php echo $new->id ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                </td>
                            </tr>    
<?php endforeach ?>

                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->
        <div class="md-modal md-effect-1" id="modal-del-news">
            <div class="md-content md-content-del">
                <h3>Obriši novost <button class="md-close"><i class="fa fa-times"></i></button></h3>
                <div class="c-block">

                    <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu novost?</h4>

                    <form id="delete_cat_form">
                        <div class="form-bottom">
                            <button type="submit" class="btn-del-large"><i class="fa fa-times"></i> Da, obriši</button>
                        </div>
                    </form>

                </div> <!-- .c-block -->
            </div>
        </div>
