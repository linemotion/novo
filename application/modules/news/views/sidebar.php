<aside class="sidebar col">


<?php if ($this->uri->segment(2) == "akcija"): ?>
        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/akcija" class="discounted disc-selected">Akcijski artikli</a>
<?php else: ?>
        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/akcija" class="discounted">Akcijski artikli</a> 
<?php endif ?>

    <nav class="aside-nav">

        <ul>
    <?php foreach (modules::run("categories/get","position") as $category): ?>
        <?php if ($category->url == $this->uri->segment(2)): ?>
            <li class="aside-selected"><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->name?></a>
                <ul>
                    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                        <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->name?></a></li>
                    <?php endforeach ?> 
                </ul>
            </li>
        <?php else: ?>
                <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->name?></a></li>
        <?php endif ?>
    <?php endforeach ?> 
        </ul>

    </nav>

    <h3 class="subtitle">
        Pogledajte i
    </h3>

    <div class="side-content">
        <a href="/black-red-white" class="see">
            <img src="/assets/img/brw-kol.jpg" alt="">
        </a>
    </div>

</aside>