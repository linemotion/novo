<?php

class News extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_news');

		$this->module = "news";
	}
	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		// $data['news'] = modules::run("news/get","id");

		echo modules::run('template/admin_render',$this->module,"create");
	}
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);
		$data['new'] = $this->get_by_id($id);


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['gallery'] = modules::run("gallery/get_where_custom","news_id",$this->uri->segment(3));

		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['news'] = modules::run("news/get","id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$categories = modules::run("categories/get","position");

		foreach ($categories as $category) 
		{
			$data['url'] = url_title(rs_char($category->name));
			$data['image'] = url_title(rs_char($category->name)).".jpg";

			$this->_update($category->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$image = modules::run('gallery/get_by_id', $this->uri->segment(3));

		$this->db->where("id" , $this->uri->segment(3));
		$this->db->delete("gallery");

		redirect('/news/gallery/'.$image->news_id);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			$data["image"] = modules::run('resize/upload',"news");

		$data['news_id'] = $this->uri->segment(3);

		$this->db->insert("gallery",$data);

		redirect('/news/gallery/'.$data['news_id']);
	}	
	function index()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["short"] = ($this->uri->segment(1) == "en" ? "short_en" : "short");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("news"),"link" => "/")
			); 	

		$data['news'] = modules::run("news/get","position");
		$data["title"] = $this->lang->line("news");


		echo modules::run('template/render',$this->module,"index",$data);
	}

	function review()
	{
		$news_id = $this->uri->segment(3);
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
	    $this->lang->load('index',$this->uri->segment(1));

		$data['new'] = modules::run("news/get_by_id",$news_id);
		$data["title"] = $data['new']->$data["name"];

		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("news"),"link" => "/".$this->uri->segment(1)."/novosti "),
			array("name" => $data['new']->date,"link" => "/")
			); 
				

		$data['gallery'] = modules::run("gallery/get_where_custom","news_id",$news_id);



		// $data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);
		// $data['products'] = modules::run("products/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['date'] = $this->input->post('date');
		$data['desc'] = $this->input->post('desc');
		$data['center'] = ($this->input->post('center') == "on") ? 1 : 0;
		$data['desc_en'] = $this->input->post('desc_en');
		$data['short'] = $this->input->post('short_desc');
		$data['short_en'] = $this->input->post('short_desc_en');

		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		if($_FILES['userfile2']['error'] == 0)
			$data['thumb'] = modules::run('resize/upload',$this->module,"userfile2");

		$this->_insert($data);

		redirect('/news/admin');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(3));

		redirect('/news/admin/');
	}
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$sorted = $this->input->post("data");

		foreach ($sorted as $key => $value)
		{
			$data['position'] = $value;

			$this->db->where("id",$key);
			$this->db->update("categories",$data);	
		}
		
		//$category = $this->get_by_id($id);

		// echo "success"; 
	}
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);


		$data['new'] = $this->get_by_id($id);

		// $data['breadcrumbs'] = array(
		// 			array("name" => "Proizvodi","link" => "/admin/products"),
		// 			array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id)
		// );
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['date'] = $this->input->post('date');
		$data['desc'] = $this->input->post('desc');
		$data['center'] = ($this->input->post('center') == "on") ? 1 : 0;
		$data['desc_en'] = $this->input->post('desc_en');
		$data['short'] = $this->input->post('short_desc');
		$data['short_en'] = $this->input->post('short_desc_en');

		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		if($_FILES['userfile2']['error'] == 0)
			$data['thumb'] = modules::run('resize/upload',$this->module,"userfile2");


		$this->_update($id,$data);

		redirect('/news/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_news->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_news->get_by_url($url);		
		return $categories;
	}
	function get_by_id($id) 
	{
		$categories = $this->mdl_news->get_by_id($id);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_news->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_news->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}


	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_news->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_news->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_news->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_news->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_news->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_news->get_max();
		return $max_id;
	}

	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_news->_custom_categories($mysql_categories);
		return $categories;
	}

}