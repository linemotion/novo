<?php
class Mdl_user extends CI_Model 
{

    function __construct()
    {
     parent::__construct();
    }

    function get_user()
    {
        $q = $this->db->get('users');
        return $q->row();
    }

    function validate()
    {
        $user = $this->get_user();

        if ($this->input->post('username') == $user->username && md5($this->input->post('password')) == $user->password)
            return TRUE;
        else
            return FALSE;
    }
}

