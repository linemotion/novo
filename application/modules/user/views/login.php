<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 login-page"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 login-page"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 login-page"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js login-page"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets/js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body class="login-page">


        <div class="login-window">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <form data-parsley-validate id="login" action="/user/do_login" method="post">

                <div class="c-block">

                    <h4 class="cb-title">Korisničko ime</h4>
                    <input type="text" name="username" class="txtinput" placeholder="Unesite ime" required>

                </div>

                <div class="c-block">

                    <h4 class="cb-title">Šifra</h4>
                    <input type="password"  name="password" class="txtinput" placeholder="Unesite šifru" required>

                </div>

                <div class="c-block">

                    <button type="submit" class="btn-add-large"><i class="fa fa-sign-in"></i>Prijavi se</button>

                </div>

            </form>


        </div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="/admin-assets/js/vendor/parsley.min.js"></script>
        <script src="/admin-assets/js/vendor/parsley-sr.js"></script>
        <script src="/admin-assets/js/plugins.js"></script>
        <script src="/admin-assets/js/main.js"></script>

    </body>
</html>