<?php

class Pro_gallery extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_pro_gallery');

		$this->module = "pro_gallery";
	}

	function delete()
	{
		$this->_delete($this->uri->segment(3));

		redirect("/pro_gallery/show/".$this->uri->segment(4));
	}

	function index()
	{

		$data['categories'] = modules::run("categories/get","position");
		$data['groups'] = modules::run("pro_gallery/get_groups","id");

		echo modules::run('template/admin_render',$this->module,"index",$data);
	}

	function show()
	{

		$group_id = $this->uri->segment(3);

		$data['images'] = modules::run("pro_gallery/get_where_custom","group_id",$group_id);

		// echo "<pre>";
		// print_r($data);
		// die();
		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}


	function review()
	{
		$id = $this->uri->segment(3);

		$data['category'] = $this->get_by_id($id);

		$data['breadcrumbs'] = array(
			array("name" => "Black Red White","link" => "/black-red-white"),
			array("name" => $data['category']->name,"link" => "/".$this->uri->segment(1)."/proizvodi/".$url)
			); 

		$data['subcategories'] = modules::run("subcategoriesBRW/get_where_custom","cat_id",$id);

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create()
	{
		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		$data['group_id'] = $this->uri->segment(3);

		$this->_insert($data);

		redirect('/gallery/show/'.$data['group_id']);	
	}

	function create_group()
	{
		$data['name'] = $this->input->post("name");

		$this->db->insert("gall_group", $data);

		redirect('/gallery/');	
	}

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);

		$data['category'] = $this->get_by_id($id);

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['url'] = url_title(rs_char($this->input->post('name')));
		
		if($_FILES['userfile']['error'] == 0)
		{
			modules::run('resize/delete',"categories",$category->image);
			$data['image'] = modules::run('resize/upload',"categories");
		}

		$this->_update($id,$data);

		redirect('/categoriesBRW/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_pro_gallery->get_max_pos();
		return $max_pos;
	}


	function get_groups($order_by)
	{
		$groups = $this->mdl_pro_gallery->get_groups($order_by);
		return $groups;
	}

	function get_by_id($id) 
	{
		$categories = $this->mdl_pro_gallery->get_by_id($id);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_pro_gallery->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_pro_gallery->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}


	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_pro_gallery->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_pro_gallery->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_pro_gallery->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_pro_gallery->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_pro_gallery->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_pro_gallery->get_max();
		return $max_id;
	}

	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_pro_gallery->_custom_categories($mysql_categories);
		return $categories;
	}
}