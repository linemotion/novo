<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV d.o.o. - okovi, alati, oprema za domaćinstvo i baštu - Podgorica, Crna Gora</title>
        <meta name="description" content="OKOV d.o.o. - okovi, alati, oprema za domaćinstvo i baštu. Josipa Broza Tita 26, Podgorica, Crna Gora.">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="assets/css/main.css?v=11">
        <link rel="stylesheet" href="assets/css/additional.css?v=2">
    </head>
    <body>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1009948045697339');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1009948045697339&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

        <header class="site-header">
            <div class="content group">
                <h1 class="site-logo">
                    <a href="/"><img src="assets/img/okov-logo-2.svg" alt="Okov"></a>
                    <img src="assets/img/slogan.svg" alt="Majstoru od majstora" class="site-slogan">
                </h1>
                <nav class="site-nav group">
                    <ul>
                        <li class="has-subnav">
                            <a href="<?php echo $this->uri->segment(1)?>/profil"><?=$this->lang->line('company')?> <i class="icon icon-arrow-down"></i></a>
                            <ul class="site-subnav">
                                <li><a href="<?php echo $this->uri->segment(1)?>/profil"><?=$this->lang->line('profile')?></a></li>
                                <li><a href="<?php echo $this->uri->segment(1)?>/menadzment"><?=$this->lang->line('management')?></a></li>
                                <li><a href="<?php echo $this->uri->segment(1)?>/karijera"><?=$this->lang->line('career')?></a></li>
                                <li><a href="<?php echo $this->uri->segment(1)?>/brendovi"><?=$this->lang->line('brands')?></a></li>
                                 <li><a href="/<?php echo $this->uri->segment(1); ?>/letsdoit"><?=$this->lang->line('letsdoit')?></a></li>
                             </ul>
                        </li>
                        <li class="has-subnav has-sub-subnav">
                            <a href="/<?php echo $this->uri->segment(1);?>/proizvodi "><?=$this->lang->line('products')?> <i class="icon icon-arrow-down"></i></a>
                            <ul class="site-subnav">
<?php foreach (modules::run("categories/get","position") as $category): ?>
                                <li>
                                    <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->$name?></a>
                                    <ul class="site-sub-subnav">
    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                                        <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->$name?></a>  </li>
    <?php endforeach; ?>
                                    </ul>
                                </li>
<?php endforeach; ?>              
                            <li class="promo-nav"><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja"><?=$this->lang->line('sales')?></a></li>                  
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo $this->uri->segment(1)?>/akcija"><?=$this->lang->line('action')?></a>
                        </li>
                        <li class="has-subnav">
                            <a href="/<?php echo $this->uri->segment(1)?>/posto"><?=$this->lang->line('percent')?> <i class="icon icon-arrow-down"></i></a>
                            <ul class="site-subnav">
                                <li><a href="/<?php echo $this->uri->segment(1)?>/posto"><?=$this->lang->line('Card')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1)?>/posto/prijava"><?=$this->lang->line('Application')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1)?>/posto/pravila"><?=$this->lang->line('rules')?></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo $this->uri->segment(1)?>/novosti"><?=$this->lang->line('news')?></a>
                        </li>
                         <li>
                            <a href="<?php echo $this->uri->segment(1)?>/uradisam"><?=$this->lang->line('yourself')?></a>
                        </li>
                        <li>
                            <a href="<?php echo $this->uri->segment(1)?>/lokacije"><?=$this->lang->line('locations')?></a>
                        </li>
                        <li class="has-subnav">
                            <a href="/<?php echo $this->uri->segment(1);?>/kontakt"><?=$this->lang->line('contact')?><i class="icon icon-arrow-down"></i></a>
                             <ul class="site-subnav">
                                <li><a href="/<?php echo $this->uri->segment(1)?>/kontakt/"><?=$this->lang->line('contact')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1)?>/kontakt/newsletter"><?=$this->lang->line('newsletter')?></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <div class="site-header-meta">
                    <a href="/<?php echo $this->uri->segment(1); ?>/letsdoit" class="ldi"><img src="/assets/img/ldi.png" alt="Let's do it!"></a>
<?php if ($this->uri->segment(1) == "en"): ?>
                    <a href="/mn" class="sh-lang">MN</a>    
<?php else: ?>
                    <a href="/en" class="sh-lang">EN</a>
<?php endif ?>                    
                    <a href="http://www.facebook.com/okovdoo"><i class="icon icon-facebook"></i></a>
                    <div class="search-container">
                        <a href="#" class="search-btn"><i class="icon icon-search"></i></a>
                        <form action="/<?php echo $this->uri->segment(1) ?>/products/search" method="post">
                            <input type="text" name="search" class="search-input" placeholder="Unesite riječ">
                            <button type="submit" class="search-btn-submit"><i class="icon icon-search"></i></button>
                        </form>
                    </div>
                </div> <!-- .site-header-meta -->
            </div> <!-- .content -->
        </header> <!-- .site-header -->

        <section class="home-subheader">
            <div class="content group">

<?php if (modules::run("actions/has_action")): ?>

                <div class="home-action">

                    <div class="home-action-window group">

                        <div class="col-1-2">
                            <a href="/<?php echo $this->uri->segment(1) ?>/akcija" class="home-action-thumb"><img src="/img/actions/thumb/<?php echo array_shift(modules::run('action_gallery/get','id'))->image ?>" alt=""></a>
                        </div> <!-- .col-1-2 -->

                        <div class="col-1-2">

                            <div class="ha-desc">
                                <h4 class="ha-title"><?php echo $action->$name ?></h4>
                                <p class="ha-text">Od <?php echo $action->start ?> do <?php echo $action->end ?></p>
                                <a href="/<?php echo $this->uri->segment(1) ?>/akcija" class="btn-default"><?=$this->lang->line('pamphlet')?> <i class="icon icon-arrow-right"></i></a>
                            </div> <!-- .ha-desc -->

                        </div> <!-- .col-1-2 -->

                    </div> <!-- .home-action-window -->

                </div> <!-- .home-action -->
<?php else: ?>

                <div class="home-action">

                    <div class="home-action-window group">
                        
                        <?php if ($this->uri->segment(1) == "mn"): ?>
                            <div class="soon-title">Uskoro stiže novi akcijski letak.</div>

                            <div class="soon-desc">Do tada, pogledajte ostale proizvode koji su na akcijama i popustima:</div>

                            <div class="soon-link">
                                <a href="/<?php echo $this->uri->segment(1) ?>/akcija" class="btn-default">Na akciji i popustu <i class="icon icon-arrow-right"></i></a>
                            </div> 
                        <?php else: ?>
                            <div class="soon-title">New catalogue coming soon.</div>

                            <div class="soon-desc">In the meantime, check out other discounted products.</div>

                            <div class="soon-link">
                                <a href="/<?php echo $this->uri->segment(1) ?>/akcija" class="btn-default">Discounted products <i class="icon icon-arrow-right"></i></a>
                            </div> 
                        <?php endif ?>

                    </div> <!-- .home-action-window -->

                </div> <!-- .home-action -->

<?php endif ?>

                <div class="home-groups home-groups-s">

                    <ul class="home-groups-pager"></ul>

                    <div class="home-groups-slider cycle-slideshow"
                        data-cycle-swipe=true
                        data-cycle-swipe-fx=fade
                        data-cycle-fx=fade
                        data-cycle-timeout=4500
                        data-cycle-slides="> div"
                        data-cycle-pager=".home-groups-pager"
                        data-cycle-pager-template="<li><b></b></li>"
                    >
                    <?php foreach ($campaigns as $camp): ?>                       
                    <div class="home-groups-slider-item">
                        
                            <img src="/img/people/<?php echo $camp->image ?>" alt="" class="hg-img">

                            <div class="hg-items group">
                                <?php foreach (modules::run("blocks/get_blocks",$camp->id) as $block): ?>
                                    <div class="col-1-2">
                                        <a href="<?php echo $block->link ?>">
                                            <img src="/img/blocks/<?php echo $block->image ?>">
                                            <span><?php echo $block->$name ?></span>
                                        </a>
                                    </div> 
                                <?php endforeach ?>
                            </div>                            
                    </div> <!-- .home-groups-slider -->
                    <?php endforeach ?>

                </div> <!-- .home-groups -->

            </div> <!-- .content -->
        </section> <!-- .home-subheader -->

        <section class="home-content">

            <div class="content">

                <h3 class="page-subtitle"><span><?=$this->lang->line('featured')?></span></h3>

                <div class="hpi-container">

                    <div class="home-product-items group">










<?php for($i=0;$i < count($products) ;$i++): ?>

<?php if (!$products[$i]->show_price): ?>
                        
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name pi-name-noprice">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                </a>
                            </div> <!-- .product item -->
                        
<?php elseif($products[$i]->discount): ?>
                        
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_discount')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->discount_value); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        

<?php elseif($products[$i]->action): ?>

                        
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_action')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->action_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        
<?php elseif($products[$i]->sales): ?>
                        
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_sale')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->sales_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        
<?php elseif($products[$i]->percent): ?>
                        
                            <div class="product-item">
                                <div class="special-tag-posto">
                                    <span>POSTO</span>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->percent_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->percent_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->

<?php else: ?>
                        
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $products[$i]->price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        

<?php endif ?>



<?php endfor ?>  

                    </div> <!-- .home-product-items -->

                    <a href="#" class="carousel-ctrl carousel-prev" id="hpi-prev"><i class="icon-arrow-left-big"></i></a>
                    <a href="#" class="carousel-ctrl carousel-next" id="hpi-next"><i class="icon-arrow-right-big"></i></a>

                </div> <!-- .hpi-container -->

                <h3 class="page-subtitle"><span><?=$this->lang->line('news')?></span></h3>

                <div class="home-news home-news-main group">
<?php for ($i = 0; $i < count($news) && $i < 3; $i++): ?>
                    <div class="col-1-3">

                        <div class="home-news-item">
                            <small class="hni-date"><?php echo $news[$i]->date ?></small>
                            <h3 class="hni-title"><a href="/<?php echo $this->uri->segment(1); ?>/novosti/<?php echo $news[$i]->id; ?>"><?php echo $news[$i]->$name ?></a></h3>
                            <div class="group">
                                <div class="hni-img-container">
                                    <img src="/img/news/<?php echo $news[$i]->thumb ?>" alt="" class="hni-img">
                                </div>
                                <p class="hni-text"><?php echo $news[$i]->$short; ?> </p>
                                <a href="/<?php echo $this->uri->segment(1); ?>/novosti/<?php echo $news[$i]->id; ?>" class="link-default"><?=$this->lang->line('more')?> <i class="icon icon-arrow-right"></i></a>
                            </div>
                        </div> <!-- .home-news-item -->

                    </div> <!-- .col-1-3 -->  
<?php endfor ?>

                </div> <!-- .home-news -->


                <p class="home-news-link">
                    <a href="/<?php echo $this->uri->segment(1);?>/novosti" class="btn-default"><?=$this->lang->line('all_news')?> <i class="icon icon-arrow-right"></i></a>
                </p>

                <h3 class="page-subtitle"><span><?=$this->lang->line('yourself')?></span></h3>

                <div class="home-news home-news-main group">
<?php for ($i = 0; $i < count($yourself) && $i < 3; $i++): ?>
                    <div class="col-1-3">

                        <div class="home-news-item">
                            <small class="hni-date"><?php echo $yourself[$i]->date ?></small>
                            <h3 class="hni-title"><a href="/<?php echo $this->uri->segment(1); ?>/uradisam/<?php echo $yourself[$i]->id; ?>"><?php echo $yourself[$i]->$name ?></a></h3>
                            <div class="group">
                                <div class="hni-img-container">
                                    <img src="/img/yourself/<?php echo $yourself[$i]->thumb ?>" alt="" class="hni-img">
                                </div>
                                <p class="hni-text"><?php echo $yourself[$i]->$short; ?> </p>
                                <a href="/<?php echo $this->uri->segment(1); ?>/uradisam/<?php echo $yourself[$i]->id; ?>" class="link-default"><?=$this->lang->line('more')?> <i class="icon icon-arrow-right"></i></a>
                            </div>
                        </div> <!-- .home-news-item -->

                    </div> <!-- .col-1-3 -->  
<?php endfor ?>

                </div> <!-- .home-news -->


                <p class="home-news-link">
                    <a href="/<?php echo $this->uri->segment(1);?>/uradisam" class="btn-default"><?=$this->lang->line('yourself')?> <i class="icon icon-arrow-right"></i></a>
                </p>

                <div class="home-info group">

                <?php echo modules::run("newsletter/widget") ?>

                    <div class="col-1-2">
                        <div class="home-info-item">
                            <h4 class="hi-title">Okov DOO</h4>
                            <?php if ($this->uri->segment(1) == "mn"): ?>
                                <p class="hi-text">Od svog osnivanja 1994. godine izrasli smo u vodeće preduzeće u prodaji svih vrsta proizvoda za rad i opremanje kuće i bašte.</p>
                            <?php else: ?>
                                <p class="hi-text">Since our foundation in 1994, we have become a leader in retail of all types of products for work and household and garden equipment.</p>
                            <?php endif ?>
                            <div class="hi-action">
                                <a href="/<?php echo $this->uri->segment(1) ?>/profil" class="btn-default"><?=$this->lang->line('about')?> <i class="icon icon-arrow-right"></i></a>
                            </div> <!-- .hi-action -->
                            <img src="/assets/img/about.svg" alt="" class="hi-img">
                        </div> <!-- .home-about -->
                    </div>

                </div> <!-- .home-info -->


                <h3 class="page-subtitle"><span><?=$this->lang->line('brands')?></span></h3>

                <div class="home-brands content">

                    <img src="/assets/img/brands.png" class="home-brands-img">

                    <img src="/assets/img/brands-mobile.png" class="home-brands-img-mobile">

                    <a href="/<?php echo $this->uri->segment(1) ?>/brendovi" class="btn-default"><?=$this->lang->line('all_brands')?> <i class="icon icon-arrow-right"></i></a>

                </div> <!-- .home-brands -->

            </div> <!-- .content -->

        </section> <!-- .home-content -->

        <footer class="site-footer">

            <section class="site-footer-top">

                <div class="content group">

                    <nav class="footer-cats group">
<?php $ft_categories = modules::run("categories/get","position"); ?>
<?php (count($ft_categories)%3 == 0) ? $moduo = count($ft_categories)/3 : $moduo = count($ft_categories)/3 + 1 ; ?>
<?php for ($i=0; $i < count($ft_categories); $i++): ?>
<?php if ($i%$moduo == 0 ): ?>
    <?php if ($i != 0): ?>
         </ul><ul>
     <?php else: ?>
        <ul>
    <?php endif ?>                
<?php endif ?>
                                <li>
                                    <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $ft_categories[$i]->url;?>"><?php echo $ft_categories[$i]->$name?></a>
                                </li>  
<?php endfor ?>
                    </nav> <!-- .footer-cats -->

                    <div class="footer-info">
                        <img src="assets/img/okov-logo.svg" alt="Okov" class="footer-info-img">
                        <p>
                            Josipa Broza Tita 26<br>
                            81000 Podgorica, Crna Gora<br>
                            +382 20 658 501<br>
                            <a href="http://www.facebook.com/okovdoo">facebook.com/okovdoo</a>
                        </p>
                    </div>

                </div> <!-- .content -->

            </section> <!-- .site-footer-top -->

            <section class="site-footer-meta">

                <div class="content group">

                    <nav class="footer-nav">
                        <ul>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/"><i class="icon icon-house"></i></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/profil"><?=$this->lang->line('company')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/proizvodi"><?=$this->lang->line('products')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/akcija"><?=$this->lang->line('action')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/novosti"><?=$this->lang->line('news')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/lokacije"><?=$this->lang->line('locations')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/kontakt"><?=$this->lang->line('contact')?></a></li>
                        </ul>
                    </nav>

                    <div class="footer-disclaimer">
                        Cijene na sajtu uključuju PDV i informativnog su karaktera; mogu se razlikovati od stvarnih cijena.<br>
                        Opisi i fotografije mogu odstupati od originala. Zadržavamo pravo greške.
                    </div> <!-- .footer-disclaimer -->

                </div> <!-- .content -->

            </section> <!-- .site-footer-meta -->

        </footer> <!-- .site-footer -->
        
        <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="/assets/js/vendor/owl.min.js"></script>
        <script src="/assets/js/vendor/jquery.cycle2.min.js"></script>
        <script src="/assets/js/vendor/jquery.cycle2.swipe.js"></script>
        <script src="/assets/js/default.js"></script>
        <script src="/assets/js/home.js?v=2"></script>
        <script src="/assets/js/newsletter.js?v=2"></script>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-29378350-1', 'auto');
          ga('send', 'pageview');

        </script>
        
<!-- Google Code for Remarketing Tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 953291135;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/953291135/?guid=ON&amp;script=0"/>
</div>
</noscript>

    </body>
</html>
