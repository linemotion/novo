<?php

class Home extends MX_Controller
{

	function __construct()
	{
		$this->output->nocache();
		parent::__construct();
	}

	function index()
	{

		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["short"] = ($this->uri->segment(1) == "en" ? "short_en" : "short");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");

		if(!$this->uri->segment(1))
			redirect("/mn");
		$data['breadcrumbs'] = array();

		// $test = array_shift(modules::run('action_gallery/get','id'))->image;
		// echo "<pre>";
		// print_r($test);
		// die();
	    $this->lang->load('index',$this->uri->segment(1));

	    $data['products'] = modules::run('products/get_home');

	    $data['campaigns'] = modules::run("campaigns/get","id");
	    $data['action'] = modules::run("actions/get","id");
	    $data['news'] = modules::run("news/get","id");
	    $data['yourself'] = modules::run("yourself/get","id");

	    // $data['blocks'] = modules::run("blocks/get_blocks",$data['campaign']->id);	    

// echo "<pre>";
// echo print_r($data["blocks"]);
// die();
	    $data['name'] = ($this->uri->segment(1) == "en") ? "name_en" : "name" ;
		
		$this->load->view('home',$data);
	}
	function products()
	{
		echo modules::run('template/admin_render',"admin","products");
	}
	function brw()
	{
		echo modules::run('template/admin_render',"admin","productsBRW");
	}
}