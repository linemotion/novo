        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                        <ul class="sidebar-nav-groups">
                            <li>
                                <a href="/company/admin">Profil</a>
                            </li>
                            <li>
                                <a href="/management/admin">Menadžment</a>
                            </li>
                            <li>
                                <a href="/career/admin">Karijera</a>
                            </li>
                            <li>
                                <a href="/brands/admin">Brendovi</a>
                            </li>
                            <li class="sidebar-nav-groups-selected">
                                <a href="/letsdoit/admin">Let's do it</a>
                            </li>
                        </ul>                    
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Let's do it</h2>
                </div>

                <header class="tab-header group">
                    <a href="/letsdoit/admin" class="tab-1-2">Sadržaj</a>
                    <a href="/letsdoit/gallery" class="tab-1-2 tab-active">Galerija</a>
                </header>

                <form data-parsley-validate method="post" action="/letsdoit/add_image" enctype="multipart/form-data">


                    <div class="f-block group">
                        <div class="fi-1-1">

                            <h4 class="cb-title">Slike</h4>

                            <ul class="admin-gal group">
<?php foreach ($gallery as $image): ?>
                                <li>
                                    <img src="/img/letsdoit/<?php echo $image->image; ?>">
                                    <span>
                                        <a href="/letsdoit/<?php echo $image->image; ?>"><i class="fa fa-search"></i></a>
                                        <a href="/letsdoit/del_image/<?php echo $image->id; ?>"><i class="fa fa-lg fa-times"></i></a>
                                    </span>
                                </li>
<?php endforeach ?>
                            </ul> <!-- .admin-gal -->

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Dodaj sliku</h4>

                            <input type="file" name="userfile" class="txtinput" required>

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Format: <b>JPG, PNG, GIF</b></p>

                        </div>

                    </div> <!-- .f-block -->

                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->
