<?php

class Letsdoit extends MX_Controller
{
	var $module;


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_letsdoit');

		$this->module = "letsdoit";
	}
	function index()
	{
	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");


		$data["title"] = $this->lang->line("letsdoit");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("company"),"link" => "/".$this->uri->segment(1)."/profil"),
			array("name" => $this->lang->line("letsdoit"),"link" => "/")
			); 

		$data['letsdoit'] = array_shift($this->get_where_custom("name","letsdoit"));
		$data['gallery'] = modules::run("letsdoit/get_gallery");

		echo modules::run('template/render',$this->module,"index",$data);
	}

	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => "Kampanje","link" => "")
		// 	); 

		$data['letsdoit'] = array_shift($this->get_where_custom("name","letsdoit"));



		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => "Kampanje","link" => "")
		// 	); 

		$data['gallery'] = $this->get_gallery();



		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}
	function profile()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data["title"] = $this->lang->line("profile");
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => "Kampanje","link" => "")
			); 

		$data['campaigns'] = $this->get("id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}


	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

	    $this->db->where('id', $this->uri->segment(3));
	    $this->db->delete("lets_gallery");

		redirect('/letsdoit/gallery');
	}


	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['subtitle'] = $this->input->post('subtitle');
		$data['subtitle_en'] = $this->input->post('subtitle_en');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');
		
		$letsdoit = array_shift($this->get_where_custom("name","letsdoit"));


		$this->_update($letsdoit->id,$data);

		redirect('/letsdoit/admin/');
	}

	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
				
		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',"letsdoit");

    	$this->db->insert("lets_gallery", $data);

		redirect('/letsdoit/gallery/');
	}

	function get($order_by)
	{
		$products = $this->mdl_letsdoit->get($order_by);
		return $products;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$products = $this->mdl_letsdoit->get_with_limit($limit, $offset, $order_by);
		return $products;
	}

	function get_by_id($id)
	{
		$products = $this->mdl_letsdoit->get_where($id);
		return $products;
	}
	function get_gallery()
	{
		$products = $this->mdl_letsdoit->get_gallery();
		return $products;
	}

	function get_where_custom($col, $value) 
	{
		$products = $this->mdl_letsdoit->get_where_custom($col, $value);
		return $products;
	}

	function _insert($data)
	{
		return $this->mdl_letsdoit->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_letsdoit->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_letsdoit->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_letsdoit->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_letsdoit->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query) 
	{
		$products = $this->mdl_letsdoit->_custom_query($mysql_query);
		return $products;
	}

}