<?php

class Yourself extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_yourself');

		$this->module = "yourself";
	}
	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		// $data['yourself'] = modules::run("yourself/get","id");

		echo modules::run('template/admin_render',$this->module,"create");
	}
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['gallery'] = modules::run("self_gallery/get_where_custom","self_id",$this->uri->segment(3));


		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}
	// function get_gallery()
	// {
	// 	$products = $this->mdl_yourself->get_gallery();
	// 	return $products;
	// }
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');


		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['yourself'] = modules::run("yourself/get","id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$categories = modules::run("categories/get","position");

		foreach ($categories as $category) 
		{
			$data['url'] = url_title(rs_char($category->name));
			$data['image'] = url_title(rs_char($category->name)).".jpg";

			$this->_update($category->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$image = modules::run('gallery/get_by_id', $this->uri->segment(3));

		$this->db->where("id" , $this->uri->segment(3));
		$this->db->delete("self_gallery");

		redirect('/yourself/gallery/'.$image->self_id);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			$data["image"] = modules::run('resize/upload',"yourself");

		$data['self_id'] = $this->uri->segment(3);

		$this->db->insert("self_gallery",$data);

		redirect('/yourself/gallery/'.$data['self_id']);
	}	
	function index()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["short"] = ($this->uri->segment(1) == "en" ? "short_en" : "short");

	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("yourself"),"link" => "/")
			); 	

		$data['yourself'] = modules::run("yourself/get","position");
		$data["title"] = $this->lang->line("yourself");


		echo modules::run('template/render',$this->module,"index",$data);
	}

	function review()
	{
		$self_id = $this->uri->segment(3);
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
	    $this->lang->load('index',$this->uri->segment(1));

		$data['your'] = modules::run("yourself/get_by_id",$self_id);
		$data["title"] = $data['your']->$data["name"];

		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("yourself"),"link" => "/".$this->uri->segment(1)."/novosti "),
			array("name" => $data['your']->date,"link" => "/")
			); 
				

		$data['gallery'] = modules::run("self_gallery/get_where_custom","self_id",$self_id);


		// $data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);
		// $data['products'] = modules::run("products/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['date'] = $this->input->post('date');
		$data['desc'] = $this->input->post('desc');
		$data['center'] = ($this->input->post('center') == "on") ? 1 : 0;
		$data['desc_en'] = $this->input->post('desc_en');
		$data['short'] = $this->input->post('short_desc');
		$data['short_en'] = $this->input->post('short_desc_en');

		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		if($_FILES['userfile2']['error'] == 0)
			$data['thumb'] = modules::run('resize/upload',$this->module,"userfile2");

		$this->_insert($data);

		redirect('/yourself/admin');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(3));

		redirect('/yourself/admin/');
	}
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$sorted = $this->input->post("data");

		foreach ($sorted as $key => $value)
		{
			$data['position'] = $value;

			$this->db->where("id",$key);
			$this->db->update("categories",$data);	
		}
		
		//$category = $this->get_by_id($id);

		// echo "success"; 
	}
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);


		$data['your'] = $this->get_by_id($id);

		// $data['breadcrumbs'] = array(
		// 			array("name" => "Proizvodi","link" => "/admin/products"),
		// 			array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id)
		// );
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['date'] = $this->input->post('date');
		$data['desc'] = $this->input->post('desc');
		$data['center'] = ($this->input->post('center') == "on") ? 1 : 0;
		$data['desc_en'] = $this->input->post('desc_en');
		$data['short'] = $this->input->post('short_desc');
		$data['short_en'] = $this->input->post('short_desc_en');

		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		if($_FILES['userfile2']['error'] == 0)
			$data['thumb'] = modules::run('resize/upload',$this->module,"userfile2");


		$this->_update($id,$data);

		redirect('/yourself/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_yourself->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_yourself->get_by_url($url);		
		return $categories;
	}
	function get_by_id($id) 
	{
		$categories = $this->mdl_yourself->get_by_id($id);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_yourself->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_yourself->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}


	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_yourself->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_yourself->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_yourself->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_yourself->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_yourself->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_yourself->get_max();
		return $max_id;
	}

	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_yourself->_custom_categories($mysql_categories);
		return $categories;
	}

}