        <section class="home-content home-content-sec">

            <div class="content group">

                <h3 class="page-subtitle"><span><?=$this->lang->line('yourself')?></span></h3>

                <div class="home-news home-news-inside inside-news group">
<?php foreach ($yourself as $self): ?>
                    <div class="col-1-3">

                        <div class="home-news-item">
                            <small class="hni-date"><?php echo $self->date ?></small>
                            <h3 class="hni-title"><a href="/<?php echo $this->uri->segment(1); ?>/uradisam/<?php echo $self->id; ?>"><?php echo $self->$name ?></a></h3>
                            <div class="group">
                                <div class="hni-img-container">
                                    <img src="/img/yourself/<?php echo $self->thumb ?>" alt="" class="hni-img">
                                </div>
                                <p class="hni-text"><?php echo $self->$short;?> </p>
                                <a href="/<?php echo $this->uri->segment(1); ?>/uradisam/<?php echo $self->id; ?>" class="link-default"><?=$this->lang->line('more')?> <i class="icon icon-arrow-right"></i></a>
                            </div>
                        </div> <!-- .home-news-item -->

                    </div> <!-- .col-1-3 -->    
<?php endforeach ?>
                </div> <!-- .home-news -->

<!--                 <div class="pagination">

                    <ul>
                        <li class="page-selected"><a>1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#" class="next"><i class="icon icon-arrow-right"></i></a></li>
                    </ul>

                </div> <!-- .pagination --> 

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
