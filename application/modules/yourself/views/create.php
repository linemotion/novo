        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>
        
                </ul>
            </nav>


            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Dodaj članak</h2>
                </div>

                <header class="tab-header group">
                    <a href="/yourself/create" class="tab-1-2 tab-active">Sadržaj članka</a>
                    <a href="/yourself/gallery" class="tab-1-2 tab">Galerija</a>
                </header>

                <form data-parsley-validate action="/yourself/create_proccess" method="post" enctype="multipart/form-data">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Naslov</h4>

                            <input type="text" name="name" class="txtinput" placeholder="Unesite naslov" required>

                        </div>
                        <div class="fi-1-2">

                            <h4 class="cb-title">Naslov <i>en</i> </h4>

                            <input type="text" name="name_en" class="txtinput" placeholder="Unesite naslov" required>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">
                        <div class="fi-1-2">

                            <h4 class="cb-title">Datum</h4>

                            <input type="text" name="date" class="txtinput datepicker" placeholder="Izaberite datum" required>

                        </div>
                    </div>
                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Glavna slika članka</h4>

                            <input type="file" name="userfile" class="txtinput">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Formati: JPEG, GIF, PNG. Kolorni mod: RGB. Poželjna uspravna slika.</p>

                            <h4 class="cb-title">Centrirana slika?</h4>

                            <div class="switch-cont">
                                <label class="switch switch-green">
                                    <input type="checkbox" name="center" class="switch-input">
                                    <span class="switch-label" data-on="Da" data-off="Ne"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div> <!-- .switch-cont -->

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Mala slika članka</h4>

                            <input type="file" name="userfile2" class="txtinput">

                            <p class="form-helper form-helper-important"><i class="fa fa-info-circle"></i> Formati: JPEG, GIF, PNG. DIMENZIJE: 130x140</p>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Sadržaj </h4>

                            <div class="ckeditor-outer">
                                <textarea value="" id="editor" name="desc" cols="80" rows="5"></textarea>
                            </div>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Sadržaj <i>en</i></h4>

                            <div class="ckeditor-outer">
                                <textarea value="" id="editor2" name="desc_en" cols="80" rows="5"><?php //echo $profile->desc_en ?></textarea>
                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Kratak sadržaj </h4>

                            <div>
                                <textarea value="" name="short_desc" cols="80" rows="5" maxlength="50"></textarea>
                            </div>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Kratak sadržaj <i>en</i></h4>

                            <div>
                                <textarea value="" name="short_desc_en" cols="80" rows="5" maxlength="50"></textarea>
                            </div>

                        </div>

                    </div> <!-- .f-block -->
                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->
