        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar inside-sidebar-move">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main sidebar-nav-news">
<?php foreach (modules::run("yourself/get","id") as $item): ?>
<?php if ($item->id == $this->uri->segment(3) ): ?>
                            <li class="snm-selected">
<?php else: ?>        
                            <li>
<?php endif ?>    
                            <a href="/<?php echo $this->uri->segment(1) ?>/uradisam/<?php echo $item->id; ?>"><?php echo $item->$name; ?></a></li>
<?php endforeach ?>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <div class="sidebar-back">
                        <a href="/<?php echo $this->uri->segment(1) ?>/uradisam" class="btn-default btn-default-back"><i class="icon icon-arrow-left"></i> Uradi sam</a>
                    </div> <!-- .sidebar-back -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle"><?php echo $your->$name ?></h1>

<?php if ($your->center): ?>
                        <a href="/img/yourself/<?php echo $your->image ?>" class="news-big-img news-big-center fancybox">
                            <img src="/img/yourself/<?php echo $your->image ?>">
                        </a>
<?php else: ?>
                        <a href="/img/yourself/<?php echo $your->image ?>" class="news-big-img fancybox">
                            <img src="/img/yourself/<?php echo $your->image ?>">
                        </a>
<?php endif ?>
                        <?php echo $your->$desc; ?>
                    </article>

<?php if ($gallery): ?>
                    <div class="page-gallery">
                        <h3 class="page-subtitle"><span>Galerija</span></h3>
                        <div class="pg-container">
                            <ul class="pg-items group">
<?php foreach ($gallery as $image): ?>
                                <li class="pg-item">
                                    <a href="/img/yourself/<?php echo $image->image; ?>" class="fancybox" rel="gallery">
                                        <img src="/img/yourself/<?php echo $image->image; ?>" alt="">
                                    </a>
                                </li>    
<?php endforeach ?>
                            </ul> <!-- .pg-items -->
                        </div> <!-- .pg-container -->
                    </div> <!-- .page-gallery -->    
<?php endif ?>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->s