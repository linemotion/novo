        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar matchheight">

                    <nav class="sidebar-nav sidebar-nav-mob">

                        <ul class="sidebar-nav-main">
                            <li class="snm-selected">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url; ?>"><?php echo $category->$name; ?></a>
                                <ul class="sidebar-subnav">
<?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $item): ?>
<?php if ($item->id == $subcategory->id): ?>
                                    <li class="ssn-selected">
                                        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url; ?>/<?php echo $subcategory->url; ?>"><?php echo $subcategory->$name; ?></a>
                                        <div class="subnav-content">
                                            <form action="">
                                                <select name="choose-kind" id="choose-kind">
                                                    <option value="all-kinds">Sve vrste</option>
<?php foreach (modules::run("sorts/get_where_custom","subcat_id",$subcategory->id) as $sort): ?>
                                                    <option value="<?php echo $sort->url."-".$sort->id; ?>"><?php echo $sort->name; ?></option>
<?php endforeach ?>                                                
                                                </select>
                                            </form>
                                        </div> <!-- .subnav-content -->
                                    </li>

<?php else: ?>
                                    <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $item->url;?>"><?php echo $item->$name?></a>  </li>
<?php endif ?>
<?php endforeach; ?> 
                                </ul>
                            </li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <div class="sidebar-responsive group">
                        <form action="">
                            <select name="choose-cat" id="choose-cat" class="select-url">
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi">
                                    <?=$this->lang->line('on_sale')?>
                                </option>
                                <?php foreach (modules::run("categories/get","position") as $cat): ?>
                                <?php if ($cat->id == $category->id): ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $cat->url;?>" selected>
                                    <?php echo $cat->$name?>
                                </option>
                                <?php else: ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $cat->url;?>">
                                    <?php echo $cat->$name?>
                                </option>
                                <?php endif ?>
                                <?php endforeach ?> 
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja ">
                                    <?=$this->lang->line('sales')?></a>
                                </option>
                            </select>
                        </form>

                        <form action="" class="resf-secondary">
                            <select name="choose-subcat" id="choose-subcat" class="select-url">
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>">
                                    Sve podgrupe
                                </option>
                                <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $scat): ?>
                                <?php if ($scat->id == $subcategory->id): ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $scat->url;?>" selected>
                                    <?php echo $scat->$name?>
                                </option>
                                <?php else: ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $scat->url;?>">
                                    <?php echo $scat->$name?>
                                </option>
                                <?php endif ?>
                                <?php endforeach ?> 
                            </select>
                        </form>

                        <form action="" class="resf-tertiary">
                        <select name="choose-kind-2" id="choose-kind-2">
                            <option value="all-kinds">Sve vrste</option>
<?php foreach (modules::run("sorts/get_where_custom","subcat_id",$subcategory->id) as $sort): ?>
                            <option value="<?php echo $sort->url."-".$sort->id; ?>"><?php echo $sort->name; ?></option>
<?php endforeach ?>                                                
                        </select>
                    </form>

                    </div> <!-- .sidebar-responsive -->

                    <div class="sidebar-back">
                        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi " class="btn-default btn-default-back"><i class="icon icon-arrow-left"></i>
                        <?php if ($this->uri->segment(1) == "en"): ?>
                            All groups
                        <?php else: ?>
                            Sve grupe
                        <?php endif ?>  
                        </a>
                    </div> <!-- .sidebar-back -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="inside-product-items group">
<?php $products = modules::run("products/get_where_paginateFront",$subcategory->id,12,$this->uri->segment(5)); ?>
<?php if ($products): ?>
<?php for($i=0;$i < count($products) ;$i++): ?>                       

<?php if (!$products[$i]->show_price): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name pi-name-noprice">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 
<?php elseif($products[$i]->discount): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_discount')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->discount_value); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 

<?php elseif($products[$i]->action): ?>

                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_action')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->action_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->
<?php elseif($products[$i]->sales): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_sale')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->sales_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->

<?php elseif($products[$i]->percent): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag-posto">
                                    <span>POSTO</span>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->percent_discount); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->



<?php else: ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $products[$i]->price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->    

<?php endif ?>


<?php endfor ?>
<?php else: ?>
    <p class="no-results">Nema rezultatas.</p>
<?php endif ?>     
 
                    </div> <!-- .group -->

                        <?php echo modules::run('pagination/paginateFront'); ?>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

