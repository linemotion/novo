<?php

class Subcategories extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_subcategories');
		$this->module = "subcategories";
	}
	
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$values = $this->input->post('values');

		for ($i=0; $i <= count($values); $i++) 
		{ 
			$data['position'] = $i + 1;
			$this->_update($values[$i],$data);
		}
	}	
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);
		$data['subcategory'] = modules::run("subcategories/get_by_id",$id);
		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
			array("name" => $data['subcategory']->name,"link" => "")
			); 

		$data['products'] = modules::run("products/get_where_custom","subcat_id",$id);


		$data['categories'] = modules::run("categories/get","position");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function get_num_by_paginateSearch($search) 
	{
		$num = $this->mdl_subcategories->get_num_search($search);
		return $num;
	}
	function get_where_paginateSearch($search,$per_page,$offset) 
	{
		$subcategories = $this->mdl_subcategories->get_where_paginateSearch($search,$per_page,$offset);

		return $subcategories;
	}
	function search()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		$data['search'] = $this->uri->segment(4);
		
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => "Pretraga","link" => ""),
			array("name" => $data['search'],"link" => "")
			); 	
		//$data['products'] = $this->get_by_search($search);
		$this->load->view('search',$data);

	}

	function review()
	{
		$url = $this->uri->segment(4);
		
			$data['category'] =modules::run("categories/get_by_url",$this->uri->segment(3));

			//uzas jebeni
			$this->db->where('url',$url);
			$query = $this->db->get("subcategories");
			//kraj uzasa
		

			$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");

		    $this->lang->load('index',$this->uri->segment(1));

			$data['subcategory'] = $query->row();
			$data["title"] = $data['subcategory']->$data["name"];

			$sorts = modules::run("sorts/get_where_custom","subcat_id",$data["subcategory"]->id);
			if(count($sorts) == 1)
				redirect("/".$this->uri->segment(1)."/proizvodi/".$data["category"]->url."/".$url."/".array_shift(array_values($sorts))->url."-".array_shift(array_values($sorts))->id."/p/0");


			$data['products'] = modules::run("products/get_where_custom","subcat_id",$data['subcategory']->id);
			
			$data['breadcrumbs'] = array(
				array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi "),
				array("name" => $data['category']->$data["name"],"link" => "/".$this->uri->segment(1)."/proizvodi/".$data['category']->url),
				array("name" => $data['subcategory']->$data["name"],"link" => "")
				); 		

		echo modules::run('template/render',$this->module,"single",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$subcategories = modules::run("subcategories/get","position");

		foreach ($subcategories as $subcategory) 
		{
			$data['url'] = url_title(rs_char($subcategory->name));
			$data['image'] = url_title(rs_char($subcategory->name)).".jpg";

			$this->_update($subcategory->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}

	function create()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['category'] = modules::run("categories/get_by_id", $this->uri->segment(3));
		$data['categories'] = modules::run("categories/get", "position");

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => $data['category']->name,"link" => "/categories/admin/".$data['category']->id),
			array("name" => "Dodavanje nove podgrupe","link" => "")
			);

		echo modules::run('template/admin_render',$this->module,"create",$data);

	}	

	function create_proccess()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['cat_id'] = $this->uri->segment(3);

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['search'] = $this->input->post('search');
		$data['url'] = url_title(rs_char($this->input->post('name')));
		$data['position'] = $this->get_max_pos()->position + 1;
		$data['cat_name'] = modules::run('categories/get_by_id',$data['cat_id'])->name;
		
		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/categories/edit/'.$data['cat_id']);

	}
	function change()
	{

		$id = $this->input->post('id');


		$output = '<select name="subcategories" class="uniformselect" id="subgroup"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('cat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}	
	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);
		
		$data['categories'] = modules::run("categories/get", "position");

		$data['subcategory'] = $this->get_by_id($id);
		$bc_category = modules::run("categories/get_by_id", $data['subcategory']->cat_id);
		
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
			array("name" => $data['subcategory']->name,"link" => "")
			);
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['search'] = $this->input->post('search');
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$category->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/subcategories/edit/'.$id);
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$subcategory = $this->get_by_id($this->uri->segment(3));
		$this->_delete($subcategory->id);

		redirect('/categories/edit/'.$subcategory->cat_id);
	}
	function get_max_pos()
	{
		$max_pos = $this->mdl_subcategories->get_max_pos();
		return $max_pos;
	}

	function get($order_by)
	{
		$subcategories = $this->mdl_subcategories->get($order_by);
		return $subcategories;
	}


	function get_with_limit($limit, $offset, $order_by) 
	{
		$subcategories = $this->mdl_subcategories->get_with_limit($limit, $offset, $order_by);
		return $subcategories;
	}

	function get_by_id($id)
	{
		$subcategories = $this->mdl_subcategories->get_where($id);
		return $subcategories;
	}

	function get_where_custom($col, $value) 
	{
		$subcategories = $this->mdl_subcategories->get_where_custom($col, $value);		
		return $subcategories;
	}

	function get_by_url($url) 
	{
		$subcategories = $this->mdl_subcategories->get_by_url($url);		
		return $subcategories;
	}

	function _insert($data)
	{
		$this->mdl_subcategories->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_subcategories->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_subcategories->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_subcategories->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_subcategories->get_max();
		return $max_id;
	}
	function catnames() 
	{
		$subs = $this->get("position");

		foreach ($subs as $sub)
		{
			$data['cat_name'] = modules::run('categories/get_by_id',$sub->cat_id)->name;
			$this->_update($sub->id,$data);
		}
	}
	function _custom_query($mysql_query) 
	{
		$subcategories = $this->mdl_subcategories->_custom_categories($mysql_query);
		return $subcategories;
	}
	function get_by_search($search) 
	{
		$subs = $this->mdl_subcategories->get_by_search($search);		
		return $subs;
	}
}