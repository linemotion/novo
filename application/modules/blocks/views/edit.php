        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                        <li>
                            <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                        </li>
                        <li class="sidebar-nav-selected">
                            <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                        </li>
                        <li>
                            <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        </li>
                        <li>
                            <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                        </li>
                        <li>
                            <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                        </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                        <li>
                            <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Izmeni stavku</h2>
                </div>

                <nav class="breadcrumbs">
                    <ul>
                        <li><a href="#">Kampanje</a></li>
                        <li><a href="#">Muškarac</a></li>
                        <li>Stavka</li>
                    </ul>
                </nav>

                <form data-parsley-validate action="/blocks/edit_proccess/<?php echo $this->uri->segment(3) ?>" method="post" enctype='multipart/form-data'>

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime stavke</h4>

                            <input type="text" name="name" class="txtinput" value="<?php echo $block->name ?>" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Link</h4>

                            <input type="text" name="link" class="txtinput" value="<?php echo $block->link ?>" required>

                            <p class="form-helper">
                                <i class="fa fa-info-circle"></i> Link mora biti u obliku <b>http://www.adresa.com</b>
                            </p>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime stavke <i>en</i></h4>

                            <input type="text" name="name_en" class="txtinput" value="<?php echo $block->name_en ?>" required>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Slika stavke</h4>

                            <a href="/img/blocks/<?php echo $block->image ?>" class="chosen-img">
                                <img src="/img/blocks/<?php echo $block->image ?>">
                            </a>

                            <input type="file" class="txtinput" name="userfile">

                            <p class="form-helper form-helper-important"><i class="fa fa-info-circle"></i> Formati: JPEG, GIF, PNG. Kolorni mod: RGB. Dimenzije: 464x190</p>

                        </div>

                    </div> <!-- .f-block -->

                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->
