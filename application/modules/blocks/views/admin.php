        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="#"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Kampanje</h2>
                    <a href="#" class="btn-add md-trigger" data-modal="modal-add-campaign"><i class="fa fa-plus-circle"></i> Dodaj kampanju</a>
                </div>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">Kampanje</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime kampanje</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
<?php foreach ($campaigns as $camp): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/campaigns/edit/<?php echo $camp->id; ?>"><?php echo $camp->name ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/campaigns/delete/<?php echo $camp->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-campaign" data-id="<?php echo $camp->id; ?>" id="delete_camp_action" data-controller="campaigns"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/campaigns/edit/<?php echo $camp->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                </td>
                            </tr>
    
<?php endforeach ?>
                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->


        <div class="md-modal md-effect-1" id="modal-add-campaign">
            <div class="md-content">
                <h3>Dodaj kampanju <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime kampanje</h4>

                        <form action="/campaigns/create" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-campaign" >
            <div class="md-content md-content-del">
                <h3>Obriši kampanju <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu kampanju?</h4>

                        <form id="delete_cat_form" method="post">
                            <div class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
