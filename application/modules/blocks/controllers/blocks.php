<?php

class Blocks extends MX_Controller
{
	var $module;


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_blocks');

		$this->module = "blocks";
	}

	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => "Kampanje","link" => "")
			); 

		$data['campaigns'] = $this->get("id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}

	function get_campaign() 
	{
		$campaign = $this->mdl_blocks->get_campaign($id);
		return $campaign;
	}


    function get_blocks($cam_id) 
    {    
		$blocks = $this->mdl_blocks->get_blocks($cam_id);		
		return $blocks;
    }

    function get_specs_front($spec_id) 
    {    
    	$id = $this->uri->segment(5);

        $this->db->where("pro_id",$id);
        $this->db->where("spec_id",$spec_id);

        $query = $this->db->get("spec_prod");

        if($query->num_rows() > 0)
            return $query->row();
    }

	function action()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => "Akcija","link" => "/".$this->uri->segment(1)."/proizvodi/akcija")
			); 

		$url = $this->uri->segment(4);


		$data['products'] = $this->get_by_action();

		echo modules::run('template/render',$this->module,"action",$data);
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$block = $this->get_by_id($this->uri->segment(3));
		$this->_delete($block->id);

		redirect('/campaigns/edit/'.$block->cam_id);
	}		
	public function pagination()
	{
		redirect('/<?php echo $this->uri->segment(1);?>/proizvodi/pretraga/'.url_title(rs_char($this->input->post('search'))));
	}

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		// 	$data['breadcrumbs'] = array(
		// array("name" => "Proizvodi","link" => "/admin/products"),
		// array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		// array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		// array("name" => $data["product"]->name,"link" => "")
		// );

		$data["block"] = modules::run("blocks/get_by_id",$id);
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		// 	$data['breadcrumbs'] = array(
		// array("name" => "Proizvodi","link" => "/admin/products"),
		// array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		// array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		// array("name" => $data["product"]->name,"link" => "")
		// );

		//$data["campaign"] = modules::run("campaigns/get_by_id",$id);
		$data["block"] = modules::run("campaign/get_by_id",$id);
		echo modules::run('template/admin_render',$this->module,"create",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		$campaign = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['link'] = $this->input->post('link');
		
		if($_FILES['userfile']['error'] == 0)
		{
			//modules::run('resize/delete',$this->module,$category->image);
			$data['image'] = modules::run('resize/upload',$this->module);
		}

		$this->_update($id,$data);

		redirect('/blocks/edit/'.$id);
	}
	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['link'] = $this->input->post('link');
		$data['cam_id'] = $this->uri->segment(3);
		
		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		$id = $this->_insert($data);
		redirect('/blocks/edit/'.$id);

	}
	function get_by_action() 
	{
		$products = $this->mdl_blocks->get_by_action();
		return $products;
	}
	function get_home() 
	{
		$products = $this->mdl_blocks->get_home();
		return $products;
	}
	function get_num_by_paginateView($id) 
	{
		$products = $this->mdl_blocks->get_num_by_paginateView($id);
		return $products;
	}
	function get_num_by_paginateAction($id) 
	{
		$products = $this->mdl_blocks->get_num_by_paginateAction($id);
		return $products;
	}
	function get_num_by_paginateFront($id) 
	{
		$products = $this->mdl_blocks->get_num_by_paginateFront($id);
		return $products;
	}
	function get_num_by_paginateSearch($search) 
	{
		$products = $this->mdl_blocks->get_num_by_paginateSearch($search);
		return $products;
	}

	function get_where_paginateView($id,$per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateView($id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateAction($per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateAction($per_page,$offset);
		return $products;
	}
	function get_where_paginateFront($id,$per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateFront($id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontCategory($cat_id,$per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateFrontCategory($cat_id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontSort($sort_id,$per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateFrontSort($sort_id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontFilter($sort_id,$per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateFrontFilter($sort_id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontAll($per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateFrontAll($per_page,$offset);
		return $products;
	}
	function get_where_paginateSearch($search,$per_page,$offset) 
	{
		$products = $this->mdl_blocks->get_where_paginateSearch($search,$per_page,$offset);
		return $products;
	}
	function get($order_by)
	{
		$products = $this->mdl_blocks->get($order_by);
		return $products;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$products = $this->mdl_blocks->get_with_limit($limit, $offset, $order_by);
		return $products;
	}

	function get_by_id($id)
	{
		$products = $this->mdl_blocks->get_where($id);
		return $products;
	}

	function get_where_custom($col, $value) 
	{
		$products = $this->mdl_blocks->get_where_custom($col, $value);
		return $products;
	}

	function _insert($data)
	{
		return $this->mdl_blocks->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_blocks->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_blocks->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_blocks->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_blocks->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query) 
	{
		$products = $this->mdl_blocks->_custom_query($mysql_query);
		return $products;
	}

}