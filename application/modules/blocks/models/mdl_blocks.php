<?php
class Mdl_blocks extends CI_Model 
{

    var $table; 

    function __construct() 
    {
        parent::__construct();
        $this->table = "campaign_blocks";
    }
    function get_blocks($id) 
    {
        $this->load->helper('array');
        $this->db->where('cam_id', $id);
        //$this->db->where('active',1);
        $query=$this->db->get($this->table);

        $blocks = $query->result();       

        $random = array();
        $counter = 0;
        while($counter < 4 and $counter < count($blocks))
        {
            $temp = random_element($blocks);
            if(!in_array($temp,$random))
            {
                array_push($random,$temp);
                $counter++;
            }
        }

        return $random;
    }


    function get_campaign() 
    {    
        $query = $this->db->get($this->table);
        
        return random_element($query->result());
    } 

    function get_random()
    {
        $this->db->where('active',1);
        $this->load->helper('array');

        $this->db->where('cat_id', 39);
        $query=$this->db->get($this->table);

        $products = $query->result();
        
        $random = random_element($products);

        return $random;
    }
    function get_by_url($url)
    {
        $this->db->where('active',1);
        $this->db->where('url', $url);
        $query=$this->db->get($this->table);

        return $query->row();
    }

    function get($order_by = "position")
    {
        $this->db->order_by($order_by,"desc");
        $query=$this->db->get($this->table);
        return $query->result();
    }

    function get_with_limit($limit, $offset, $order_by) {
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by);
    $query=$this->db->get($this->table);
    return $query;
    }

    function get_where($id){
    $this->db->where('id', $id);
    $query=$this->db->get($this->table);
    return $query->row();
    }

    function get_where_custom($col, $value) {
        $this->db->order_by('id', "desc");
        //$this->db->where('active',1);
    $this->db->where($col, $value);
    $query=$this->db->get($this->table);
    return $query->result();
    }

    function _insert($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
    }

    function _update($id, $data){
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
    }

    function _delete($id){
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    }

    function count_where($column, $value) {
    $this->db->where($column, $value);
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function count_all() {
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function get_max($field = "id") 
    {
        $this->db->select_max($field);
        $query = $this->db->get($this->table);
        $row=$query->row();
        return $query->row()->id;
    }

    function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
    }

}

