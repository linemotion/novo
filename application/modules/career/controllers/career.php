<?php

class Career extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_career');
	}
	function cv()
	{

			// load codeigniter helpers
			$this->load->helper(array('form','url'));
			// set path to store uploaded files
			$config['upload_path'] = $_SERVER['DOCUMENT_ROOT']."/cv/";
			// set allowed file types
			$config['allowed_types'] = 'pdf|doc|docx|jpg';
			// set upload limit, set 0 for no limit
			$config['max_size']	= 0;
			$config['file_name']	= random_string('alnum', 4)."-".$_FILES["userfile"]['name'];
			$filename = str_replace(" ", "_", $config["file_name"]);
	 
			// load upload library with custom config settings
			$this->load->library('upload', $config);
	 
			 // if upload failed , display errors
			$this->upload->do_upload();

			$config = array(
                   'protocol'  => 'smtp',
                   'smtp_host' => 'cp1.ulimitserver.com',
                   'smtp_port' => 25,
                   'smtp_user' => 'robot@okov.me',
                   'smtp_pass' => 'r0b07123',
                   'smtp_timeout' => 10,
                   'charset'  => 'utf-8',
                   'priority' => '1',
                );

		    $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");

		    $this->email->from($this->input->post('email'),'Okov');
		    //$this->email->reply_to('rankovicmarko@yahoo.com',"bati");
		    $this->email->to('karijera@okov.me');
		    $this->email->subject('Karijera');
		    $this->email->message(
				'Ime i Prezime: '.$this->input->post('name')."\r\n".
				'Telefon: '.$this->input->post('phone')."\r\n".
				'E-mail: '.$this->input->post('email')."\r\n".
				'Pozicija: '.$this->input->post('position')."\r\n".
				'Cv: '.base_url()."cv/".$filename."\r\n"
				);

		    if($this->email->send())
		        $this->cv_send();		        
		    else
				show_error($this->email->print_debugger());
	}

	function cv_send()
	{	
	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		// $data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		// $data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");


    	$data['breadcrumbs'] = array(
			array("name" => "Kompanija","link" => "/profil"),
			array("name" => "Karijera","link" => "/profil")
		);  

		echo modules::run('template/render',"career","cv_send",$data);
	}

	function content()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["career"] = array_shift(modules::run('company/get_where_custom',"name","career")); 
		echo modules::run('template/admin_render',"career","career",$data);
	}
	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["positions"] = $this->get("id"); 
		echo modules::run('template/admin_render',"career","admin",$data);
	}
	function edit()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["employee"] = $this->get_by_id($this->uri->segment(3)); 
		echo modules::run('template/admin_render',"career","edit",$data);
	}
	function create()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		echo modules::run('template/admin_render',"career","create");
	}
	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['position'] = $this->input->post('position');

		$this->_update($id,$data);

		redirect('/career/edit/'.$id);
	}

	function edit_content()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['subtitle'] = $this->input->post('subtitle');
		$data['subtitle_en'] = $this->input->post('subtitle_en');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');

	    $this->db->where('name', "career");
	    $this->db->update("company", $data);

		redirect('/career/content/');
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['position'] = $this->input->post('name');

		$this->_insert($data);

		redirect('/career/admin');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$this->_delete($this->uri->segment(3));
		redirect('/career/admin');
	}

	function get($order_by)
	{
		$tags = $this->mdl_career->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_career->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_career->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_career->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_career->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_career->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_career->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_career->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_career->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_career->_custom_tags($mysql_tags);
		return $tags;
	}

}