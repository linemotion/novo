        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/profil"><?=$this->lang->line('profile')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/menadzment"><?=$this->lang->line('management')?></a></li>
                            <li  class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/karijera"><?=$this->lang->line('career')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/brendovi"><?=$this->lang->line('brands')?></a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>


                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle">Hvala!</h1>

                        <p>Vaša prijava je poslata. Javićemo vam se u najkraćem mogućem roku.</p>

                    </article>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

