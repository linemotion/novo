        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                        <li>
                            <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                        </li>
                        <li>
                            <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                        </li>
                        <li>
                            <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        </li>
                        <li>
                            <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                        </li>
                        <li class="sidebar-nav-selected">
                            <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                        </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                        <li>
                            <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>
                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Akcije</h2>
                </div>

                <header class="tab-header group">
                    <a href="/actions/admin" class="tab-1-2 tab-active">Akcija</a>
                    <a href="/actions/pages" class="tab-1-2">Stranice letka</a>
                </header>

                <form data-parsley-validate method="POST" action="/actions/edit_proccess/1" enctype="multipart/form-data">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime akcije</h4>

                            <input type="text" name="name" class="txtinput"  value="<?php echo ($action->name) ? $action->name : "" ?>" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Akcija aktivna?</h4>

                            <div class="switch-cont">
                                <label class="switch switch-green">
                                <?php if ($action->visible): ?>
                                    <input type="checkbox" name="visible" class="switch-input"  checked>
                                <?php else: ?>
                                    <input type="checkbox" name="visible" class="switch-input">
                                <?php endif ?>
                                    <span class="switch-label" data-on="Da" data-off="Ne"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div> <!-- .switch-cont -->

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime akcije <i>En</i></h4>

                            <input type="text" name="name_en" class="txtinput" value="<?php echo $action->name_en ?>" required>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Datum početka akcije</h4>

                            <input type="text" name="start" class="txtinput datepicker" value="<?php echo $action->start ?>">

                            <span class="form-sign"><i class="fa fa-calendar"></i></span>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Datum kraja akcije</h4>

                            <input type="text" name="end" class="txtinput datepicker" value="<?php echo $action->end ?>">

                            <span class="form-sign"><i class="fa fa-calendar"></i></span>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">PDF letka</h4>

                            <input type="file" name="userfile" class="txtinput">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Format: .PDF</p>

                        </div>

                    </div> <!-- .f-block -->

                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->
