        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
    <?php if ($item->id == $category->id): ?>

                            <li class="sidebar-nav-groups-selected">
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name ?></a>
                                <ul class="sidebar-nav-subgroups">

        <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                                    <li><a href="/subcategories/edit/<?php echo $subcategory->id; ?>"><?php echo $subcategory->name ?></a></li>
        <?php endforeach ?>
                                </ul>
                            </li>
    <?php else: ?>
                            <li>
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
    <?php endif ?>

<?php endforeach ?>
                       </ul>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title"><?php echo $category->name; ?> <a href="#" class="edit-pop md-trigger" data-modal="modal-edit-group"><i class="fa fa-pencil"></i></a></h2>
                    <a href="#" class="btn-add md-trigger" data-modal="modal-add-subgroup"><i class="fa fa-plus-circle"></i> Dodaj podgrupu</a>
                </div>

                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>

                <div class="c-block">

                    <table class="main-table">
<?php if (modules::run("subcategories/get_where_custom","cat_id",$category->id)): ?>
                        <caption class="tab-title">Podgrupe</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime podgrupe</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
<?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/subcategories/edit/<?php echo $subcategory->id; ?>"><?php echo $subcategory->name;; ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/subcategories/delete/<?php echo $subcategory->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-subgroup" data-id="<?php echo $subcategory->id; ?>" id="delete_cat_action" data-controller="subcategories"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/subcategories/edit/<?php echo $subcategory->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                    <a href="#" class="move-btn" data-id="<?php echo $subcategory->id; ?>"><i class="fa fa-arrows"></i></a>
                                </td>
                            </tr>
<?php endforeach ?>
<?php else: ?>
                    <table class="main-table">
                        <caption class="tab-title">Podgrupe</caption>
                    </table>

                    <div class="content-blank">

                        <p>Podgrupe proizvoda su prazne. Molimo Vas da dodate podgrupu.</p>

                    </div> <!-- .content-blank -->
<?php endif; ?>

                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->


        <div class="md-modal md-effect-1" id="modal-add-subgroup">
            <div class="md-content">
                <h3>Dodaj podgrupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime podgrupe</h4>

                        <form action="/subcategories/create_proccess/<?php echo $category->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-subgroup">
            <div class="md-content md-content-del">
                <h3>Obriši podgrupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu podgrupu?</h4>

                        <form method="post" id="delete_cat_form" action="">
                            <div class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-edit-group">
            <div class="md-content md-content-edit">
                <h3>Izmeni grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime grupe</h4>

                        <form action="/categories/edit_proccess/<?php echo $category->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="<?php echo $category->name; ?>" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>