        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar inside-sidebar-action">

                <?php echo modules::run("newsletter/sidebar") ?>

                    <div class="sidebar-window sidebar-window-contact group">
                        <h3 class="sw-title"><?=$this->lang->line('more')?></h3>
                        <?php if ($this->uri->segment(1) == "mn"): ?>
                            <p class="sw-text">Za više informacija o proizvodima kontaktirajte nas na telefon 020 658 501</p>
                        <?php else: ?>
                            <p class="sw-text">For more information u can contact us on 020 658 501</p>
                        <?php endif ?>
                        <a href="/<?php echo $this->uri->segment(1) ?>/kontakt" class="link-default sw-link"><?=$this->lang->line('contact')?> <i class="icon icon-arrow-right"></i></a>
                    </div> <!-- .sidebar-window -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">


<?php if (modules::run("actions/has_action")): ?>
<!-- prvi if -->


                    <h1 class="section-subtitle">

                        <?php echo $action->$name ?>
                        <span><?php echo $action->start ?> - <?php echo $action->end; ?></span>
                    </h1>

                    <div id="pageflip"></div>

                    <a href="/pdf/akcija.pdf" class="btn-default btn-pdf"><i class="icon icon-file-pdf"></i>PDF download <?=$this->lang->line('pamphlet')?></a>

                    <h3 class="page-subtitle"><span><?=$this->lang->line('action_products')?></span></h3>

                    <div class="inside-product-items inside-action-items group">

<?php if ($products): ?>
<?php for($i=0;$i < 3 ;$i++): ?>                       

<?php if (!$products[$i]->show_price): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name pi-name-noprice">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 
<?php elseif($products[$i]->discount): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    Popust
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b><?php echo floor($products[$i]->discount_value); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 

<?php elseif($products[$i]->action): ?>

                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    Akcija
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em><?php echo floor($products[$i]->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->action_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->

<?php else: ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $products[$i]->price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->    

<?php endif ?>


<?php endfor ?>
<?php else: ?>
    <p class="no-results">Nema rezultata.</p>
<?php endif ?>     

                    </div> <!-- .group -->

                    <div class="btn-container">
                        <a href="/<?php echo $this->uri->segment(1) ?>/proizvodi" class="btn-default"><?=$this->lang->line('all_action_products')?> <i class="icon icon-arrow-right"></i></a>
                    </div>

<?php else: ?>
<!-- novi else -->
                    <div class="letak-info">
                        <?php if ($this->uri->segment(1) == "mn"): ?>
                        <b class="letak-info-title">Letak</b>
                        <div class="letak-info-desc">
                            <p>Uskoro stiže novi akcijski letak.</p>
                            <p>Do tada, pogledajte ostale proizvode koji su na akcijama i popustima.</p>
                        </div>
                        <?php else: ?>
                        <b class="letak-info-title">Catalogue</b>
                        <div class="letak-info-desc">
                            <p>New catalogue coming soon.</p>
                            <p>In the meantime, check out other discounted products.</p>
                        </div>
                        <?php endif ?>
                    </div>

                    <div class="inside-product-items group">
<?php if ($products): ?>
<?php for($i=0;$i < count($products) ;$i++): ?>                       

<?php if (!$products[$i]->show_price): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name pi-name-noprice">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 
<?php elseif($products[$i]->discount): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_discount')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->discount_value); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 

<?php elseif($products[$i]->action): ?>

                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_action')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->action_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->
<?php elseif($products[$i]->sales): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_sale')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->sales_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->

<?php elseif($products[$i]->percent): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag-posto">
                                    <span>POSTO</span>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->percent_discount); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->

<?php else: ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $products[$i]->price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->    

<?php endif ?>


<?php endfor ?>
<?php else: ?>
    <?php if ($this->uri->segment(1) == "en"): ?>
        <p class="no-results">Currently, there aren't any products on sale</p>
    <?php else: ?>
        <p class="no-results">Trenutno nemamo proizvoda na akciji i popustu</p>
    <?php endif ?>
<?php endif ?>     

                    </div> <!-- .group -->



<?php echo modules::run('pagination/paginateFrontAllAction'); ?>

<?php endif ?> 
<!-- novi endif -->


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->