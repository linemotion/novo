<?php
class Mdl_actions extends CI_Model 
{

    var $table; 

    function __construct() 
    {
        parent::__construct();
        $this->table = "actions";
    }
    function get_where_paginateSearch($search,$per_page,$offset) 
    {        
        $this->db->order_by("id",'desc');
        $this->db->like('url', $search,'after'); 

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
        
    }
    function get_num_search($search) 
    {        
        $this->db->order_by("id",'desc');
        $this->db->like('url', $search,'after'); 

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->num_rows();
    } 
    function get($order_by = "id")
    {
        $this->db->flush_cache();

        $this->db->order_by($order_by,"desc");
        $query=$this->db->get($this->table);
        return $query->row();

        $this->db->flush_cache();

    }
    function get_by_url($url)
    {
        $this->db->where('active', 1);
        $this->db->where('url', $url);
        $query=$this->db->get($this->table);


        return $query->row();
    }
    function get_max_pos()
    {
        $this->db->where('active', 1);
        $this->db->select_max('position');
        $query=$this->db->get($this->table);
        
        return $query->row();
    }

    function get_with_limit($limit, $offset, $order_by) {
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by);
    $query=$this->db->get($this->table);
    return $query;
    }

    function get_where($id){
    $this->db->where('id', $id);
    $query=$this->db->get($this->table);
    return $query->row();
    }

    function get_where_custom($col, $value) {
    $this->db->where($col, $value);
    $query=$this->db->get($this->table);
    return $query;
    }

    function _insert($data){
    $this->db->insert($this->table, $data);
    }

    function _update($id, $data){
    $this->db->flush_cache();

    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
    $this->db->flush_cache();
    }

    function _delete($id){
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    }

    function count_where($column, $value) {
    $this->db->where($column, $value);
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function count_all() {
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function get_max($field = "id") 
    {
        $this->db->select_max($field);
        $query = $this->db->get($this->table);
        $row=$query->row();
        return $query->row()->id;
    }

    function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
    }

}

