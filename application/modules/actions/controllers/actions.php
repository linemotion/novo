<?php

class Actions extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_actions');

		$this->module = "actions";
	}

	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$data['category'] = modules::run("actions/get_by_id",$id);
		$data['action'] =modules::run("actions/get","id");

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		//$data['gallery'] = modules::run("actions/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function pages()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$data['category'] = modules::run("actions/get_by_id",$id);
		$data['action'] =modules::run("actions/get","id");

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['gallery'] = modules::run("action_gallery/get","id");

		echo modules::run('template/admin_render',$this->module,"pages",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$actions = modules::run("actions/get","position");

		foreach ($actions as $category) 
		{
			$data['url'] = url_title(rs_char($category->name));
			$data['image'] = url_title(rs_char($category->name)).".jpg";

			$this->_update($category->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}

	function has_action()
	{
		$action = modules::run("actions/get","id");
		return $action->visible;
	}
	
	function index()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		$this->load->helper("file");
		$line = "";

		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("action"),"link" => "/")
			); 
				
		$data['action'] = modules::run("actions/get","id");
		$data['gallery'] = modules::run("action_gallery/get");
		$data["title"] = $data['action']->$data["name"];

		for ($i=0; $i < count($data["gallery"]); $i++) 
		{ 
			if ($i == 0 OR $i == count($data["gallery"]) - 1) 
				$line .= '<div class="cover"><a href="/img/actions/'.$data["gallery"][$i]->image.'" class="fancybox"><img src="/img/actions/'.$data["gallery"][$i]->image.'"></a></div>';
			else
				$line .= '<div class="page"><a href="/img/actions/'.$data["gallery"][$i]->image.'" class="fancybox"><img src="/img/actions/'.$data["gallery"][$i]->image.'"></a></div>';

			
		}

		write_file('./akcija-content.html', $line, 'w+');
		
		$data["products"] = modules::run("products/get_where_paginateFrontAllAction",12,$this->uri->segment(3));
//		$data['products'] = modules::run("products/get_by_action");

		echo modules::run('template/render',$this->module,"index",$data);
	}

	// function review()
	// {
	// 	$url = $this->uri->segment(2);
	// 	$data['category'] = modules::run("actions/get_by_url",$url);

	// 	$data['breadcrumbs'] = array(
	// 		array("name" => $this->lang->line("action"),"link" => "/".$this->uri->segment(1)."/akcija "),
	// 		array("name" => $data['category']->$name,"link" => "/".$this->uri->segment(1)."/proizvodi/".$url)
	// 		); 

	// 	$data['subactions'] = modules::run("subactions/get_where_custom","cat_id",$data['category']->id);
	// 	$data['products'] = modules::run("products/get_where_custom","cat_id",$data['category']->id);

	// 	echo modules::run('template/render',$this->module,"single",$data);
	// }

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$data['name'] = $this->input->post('name');
		$data['position'] = $this->get_max_pos()->position + 1;

		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/admin/products');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(3));

		redirect('/admin/products/');
	}
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));
		$this->db->where("id" , $this->uri->segment(3));
		$this->db->delete("action_gallery");

		redirect('/actions/pages/');
	}
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		$data['actions'] = modules::run("actions/get","position");

		$data['category'] = $this->get_by_id($id);

		$data['breadcrumbs'] = array(
					array("name" => "Proizvodi","link" => "/admin/products"),
					array("name" => $data["category"]->name,"link" => "/actions/edit/".$data["category"]->id)
		);
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			$data["image"] = modules::run('resize/upload',"actions");

		$data['action_id'] = $this->uri->segment(3);

		$this->db->insert("action_gallery",$data);

		redirect('/actions/pages');
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			modules::run('resize/do_upload');

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['start'] = $this->input->post('start');
		$data['end'] = $this->input->post('end');
		$data['visible'] = ($this->input->post('visible') == "on") ? 1 : 0;

		$this->_update(1,$data);

		redirect('/actions/admin/');
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_actions->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$actions = $this->mdl_actions->get_by_url($url);		
		return $actions;
	}
	function get($order_by)
	{
		$actions = $this->mdl_actions->get($order_by);
		return $actions;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$actions = $this->mdl_actions->get_with_limit($limit, $offset, $order_by);
		return $actions;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_actions->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$actions = $this->mdl_actions->get_where_custom($col, $value);
		return $actions;
	}

	function _insert($data)
	{
		$this->mdl_actions->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_actions->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_actions->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_actions->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_actions->get_max();
		return $max_id;
	}

	function _custom_actions($mysql_actions) 
	{
		$actions = $this->mdl_actions->_custom_actions($mysql_actions);
		return $actions;
	}

}