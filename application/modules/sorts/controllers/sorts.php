<?php

class Sorts extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_sorts');
		$this->module = "sorts";

	}
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$values = $this->input->post('values');

		for ($i=0; $i <= count($values); $i++) 
		{ 
			$data['position'] = $i + 1;
			$this->_update($values[$i],$data);
		}
	}
	function change_sort()
	{
		$sort = $this->input->post("kind");
		$category = $this->uri->segment(3);
		$subcategory = $this->uri->segment(4);
		
		redirect("/<?php echo $this->uri->segment(1);?>/proizvodi/".$category->url."/".$subcategory->url."/".$sort->url);
	}

	function review()
	{
		$url = $this->uri->segment(5);
		
		$sort_id = array_pop(explode("-", $url));
		$data['sort'] = $this->get_by_id($sort_id);

		$data['category'] = modules::run("categories/get_by_url",$this->uri->segment(3));
		$data['subcategory'] = modules::run("subcategories/get_by_url",$this->uri->segment(4));

		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		
		$data["title"] = $data['sort']->name;

		$data['products'] = modules::run("products/get_where_custom","sort_id",$data['sort']->id);
		$data['filters'] = modules::run("filters/get_where_custom","sort_id",$data['sort']->id);
		
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => $data['category']->$data["name"],"link" => "/".$this->uri->segment(1)."/proizvodi/".$data['category']->url),
			array("name" => $data['subcategory']->$data["name"],"link" => "")
			); 		

		echo modules::run('template/render',$this->module,"single",$data);
	}
	function filters()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));

		$filters = $this->input->post('filters');

        $data["flash_filters"] = array();

		if($filters)
		{        
        	$this->session->sess_destroy();
			foreach ($filters as $key => $value) 
		         {
		             $data["flash_filters"][] = $value;
		         }

        	$this->session->set_userdata("filter_ids",$data["flash_filters"]);
        	$this->session->set_userdata("filters",$filters);
	    } 



		$data['products'] = modules::run("products/get_where_filter");

		$url = $this->uri->segment(5);
		$sort_id = array_pop(explode("-", $url));
		$data['sort'] = $this->get_by_id($sort_id);
		$data["title"] = $data['sort']->name;

		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		
		$data['category'] = modules::run("categories/get_by_url",$this->uri->segment(3));
		$data['subcategory'] = modules::run("subcategories/get_by_url",$this->uri->segment(4));


		$data['filters'] = modules::run("filters/get_where_custom","sort_id",$data['sort']->id);
		
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => $data['category']->$data["name"],"link" => "/".$this->uri->segment(1)."/proizvodi/".$data['category']->url),
			array("name" => $data['subcategory']->$data["name"],"link" => "")
			); 		

		echo modules::run('template/render',$this->module,"single_filter",$data);
	}

	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);

		$data['categories'] = modules::run("categories/get","position");
		$data['subcategory'] = modules::run("subcategories/get_by_id",$id);

		$data["sorts"] = modules::run("sorts/get","id"); 

		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);


			$data['breadcrumbs'] = array(
		array("name" => "Proizvodi","link" => "/admin/products"),
		array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		array("name" => $data['subcategory']->name,"link" => "/subcategories/edit/".$data['subcategory']->id),
		);

		echo modules::run('template/admin_render',"sorts","admin",$data);
	}

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);
		
		$data['categories'] = modules::run("categories/get", "position");

		$data['sort'] = $this->get_by_id($id);
		//$bc_category = modules::run("categories/get_by_id", $data['subcategory']->cat_id);

		$bc_subcategory = modules::run("subcategories/get_by_id",$data['sort']->subcat_id);
		$bc_category = modules::run("categories/get_by_id",$data['sort']->cat_id);

		
			$data['breadcrumbs'] = array(
		array("name" => "Proizvodi","link" => "/admin/products"),
		array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		array("name" => $data["sort"]->name,"link" => "")
		);


		echo modules::run('template/admin_render',"sorts","edit",$data);
	}

	function ajaxSpecs()
	{
		// if(!$this->session->userdata('logged_in') == true)
		// 	redirect('prijava');
		
		$id = $this->input->post('id');
		$output = "";

		// $filters = modules::run("filters/get_where_custom","sort_id",$id);
		$specs = modules::run("specifications/get_where_custom","sort_id",$id);

		foreach ($specs as $spec) 
		{
            $output .= '<div class="spec-item group">';
	        $output .= '<label>'.$spec->name.'</label>';
	        $output .= '<input type="text" name="specs[]" class="txtinput"></div>';			
		}


		// $data['filter_id'] = $this->uri->segment(3);
		
		// $filter = modules::run("filters/get_by_id",$this->uri->segment(3));
		
		// $id = $this->_insert($data);

		echo $output;
	}

	function ajaxFilters()
	{
		// if(!$this->session->userdata('logged_in') == true)
		// 	redirect('prijava');
		
		$id = $this->input->post('id');
		$output = "";

		$filters = modules::run("filters/get_where_custom","sort_id",$id);
		// $specs = modules::run("specifications/get_where_custom","sort_id",$id);

		foreach ($filters as $filter) 
		{
            $output .= '<div class="spec-item group">';
	        $output .= '<label>'.$filter->name.'</label>';
	        $output .= '<select class="chosen-select" name="filters[]"><option></option>';
	        foreach (modules::run("filter_values/get_where_custom","filter_id",$filter->id) as $value) 
	        {
	        	$output .= '<option value="'.$value->id.'">'.$value->value.'</option>';
	        }			
	        $output .= '</select></div>';			
		}

		// $data['filter_id'] = $this->uri->segment(3);
		
		// $filter = modules::run("filters/get_by_id",$this->uri->segment(3));
		
		// $id = $this->_insert($data);

		echo $output;
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['search'] = $this->input->post('search');

		//$sort = $this->get_by_id($id);
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$category->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/sorts/edit/'.$id);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['subcat_id'] = $this->uri->segment(3);
		$subcategory = modules::run('subcategories/get_by_id',$data['subcat_id']);
		

		$data['cat_id'] =  $subcategory->cat_id;

		$data['name'] = $this->input->post('name');
		$data['search'] = $this->input->post('search');
		$data['url'] = url_title(rs_char($this->input->post('name')));
		$data['position'] = $this->get_max_pos()->position + 1;
		$data['subcat_name'] = $subcategory->name;
		$data['cat_name'] = $subcategory->cat_name;
		
		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/sorts/admin/'.$data['subcat_id']);
	}
	function change()
	{

		$id = $this->input->post('id');
		$output = '<select name="tags" class="uniformselect" id="tag"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('subcat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$sort = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(3));
		redirect('/sorts/admin/'.$sort->subcat_id);
	}

	function get($order_by)
	{
		$tags = $this->mdl_sorts->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_sorts->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_sorts->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_sorts->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_sorts->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_sorts->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_sorts->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_sorts->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_sorts->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_sorts->_custom_tags($mysql_tags);
		return $tags;
	}
	function get_max_pos()
	{
		$max_pos = $this->mdl_subcategories->get_max_pos();
		return $max_pos;
	}
	function get_by_search($search) 
	{
		$sorts = $this->mdl_sorts->get_by_search($search);		
		return $sorts;
	}

}