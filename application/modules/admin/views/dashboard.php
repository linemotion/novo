            <aside class="sidebar">

                <header class="sidebar-header">
                    <h1 class="sidebar-logo group">
                        <img src="/admin-assets/img/logo.png" alt="">
                        <span><b>Okov</b> Administracija</span>
                    </h1>
                </header>

                <nav class="sidebar-nav">
                    <ul>
                        <li class="sidebar-nav-selected">
                            <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                        </li>
                        <li>
                            <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                        </li>
                        <li>
                            <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        </li>
                        <li>
                            <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                        </li>
                        <li>
                            <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                        </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>                        
                    <li>
                            <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                        </li>
                        
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>
                    </ul>
                </nav>

                <footer class="copy-footer">
                    CMS Copyright &copy; <a href="www.linemotion.com">Linemotion</a>
                </footer>

            </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>
            <div class="main-content">

                <h2 class="page-title">Sinhronizacija</h2>

                <div class="sync-container">
                    <a href="/products/sync" class="btn-add-large"><i class="fa fa-refresh"></i> Sinhronizuj cene proizvoda</a>
                    <small class="sync-time">Poslednja sinhronizacija: <b><?php echo file_get_contents($_SERVER['DOCUMENT_ROOT']."/xml/timestamp.txt") ?></b></small>
                </div> <!-- .sync-container -->

                <h2 class="page-title">Statistika</h2>

                <!-- <div class="stats-container">
                    <iframe src="http://www.seethestats.com/stats/14083/Pageviews_b59d7bba4_ifr.html" style="width:100%;height:300px;border:none;" scrolling="no" frameborder="0"></iframe>
                    <iframe src="http://www.seethestats.com/stats/14083/Visits_68449e5f1_ifr.html" style="width:100%;height:300px;border:none;" scrolling="no" frameborder="0"></iframe>
                </div> -->
            </div> <!-- .main-content -->

        </section> <!-- .main -->