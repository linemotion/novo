        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $category): ?>
                            <li>
                                <a href="/categories/edit/<?php echo $category->id; ?>"><?php echo $category->name;?></a>
                            </li>
<?php endforeach ?>
                        </ul>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>
                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">
                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Proizvodi</h2>
                    <a href="#" class="btn-add md-trigger" data-modal="modal-add-group"><i class="fa fa-plus-circle"></i> Dodaj grupu</a>
                </div>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">GRUPE PROIZVODA</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime grupe</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-controller="categories">

<?php foreach ($categories as $category): ?>
                            <tr id="<?php echo $category->id ?>">
                                <td class="td-left td-name">
                                    <a href="/categories/edit/<?php echo $category->id; ?>"><?php echo $category->name;; ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/categories/delete/<?php echo $category->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-group" data-id="<?php echo $category->id; ?>" id="delete_cat_action" data-controller="categories"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/categories/edit/<?php echo $category->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                    <a href="#" class="move-btn" data-id="<?php echo $category->id; ?>" data-pos="<?php echo $category->position; ?>"><i class="fa fa-arrows"></i></a>
                                </td>
                            </tr>
<?php endforeach ?>
                       </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-add-group">
            <div class="md-content">
                <h3>Dodaj grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime grupe</h4>

                        <form action="/categories/create" method="Post">

                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>
                            
                            <h4 class="cb-title">Ime grupe <i>en</i></h4>
                            <input type="text" name="name_en" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <br>
                            
                            <h4 class="cb-title">Pojmovi za pretragu</h4>
                            <textarea rows="2" name="search" class="txtarea" data-required="true"></textarea>
                            <p class="form-helper"><i class="fa fa-info-circle"></i> Odvojeni razmakom, npr: električni električan alati alat</p> 

                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-group">
            <div class="md-content md-content-del">
                <h3>Obriši grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu grupu?</h4>

                        <form action="" method="Post" id="delete_cat_form">
                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-del-large " ><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>