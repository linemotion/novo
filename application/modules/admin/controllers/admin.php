<?php

class Admin extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
	}

	function dashboard()
	{

		// $page_xml = file_get_contents('http://www.seethestats.com/stats/14083/1_pageviews_widget.xml?curt='.time());
		// $visits_xml = file_get_contents('http://www.seethestats.com/stats/14083/2_visits_widget.xml?curt='.time());


		$this->load->library('simplexml');

		$xdata = $this->simplexml->xml_parse($page_xml);
		$ydata = $this->simplexml->xml_parse($visits_xml);

		$data["page_views"] = array_pop(explode(": ", $xdata['@attributes']['subCaption']));
		$data["visits"] = array_pop(explode(": ", $ydata['@attributes']['subCaption']));

		$page_sets =  $xdata['set'];
		$visits_sets =  $ydata['set'];

		foreach ($page_sets as $row) 
		{
			$value = $row['@attributes']['value'];
			$label = $row['@attributes']['label'];

			$page_arr[$label] = $value;
		}
		foreach ($visits_sets as $rowz) 
		{
			$value = $rowz['@attributes']['value'];
			$label = $rowz['@attributes']['label'];

			$visits_arr[$label] = $value;
		}
		$data["visits_sets"] = $visits_arr;
		$data["page_sets"] = $page_arr;


		echo modules::run('template/admin_render',"admin","dashboard",$data);
	}
	function products()
	{

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products")
			); 

		$data["categories"] = modules::run("categories/get","position");

		echo modules::run('template/admin_render',"admin","products",$data);
	}

	function format($price)
	{
		$integ = array();
		$integ = explode(".", $price);
		
		echo  '<span class="pi-price">'.$integ[0].'<span class="pi-decimal">'.$integ[1]."</span></span>";
	}
	function format_old_price($price)
	{
		$integ = array();
		$integ = explode(".", $price);
		
		echo  '<del class="pi-price-old-value">'.$integ[0].'<span class="pi-decimal">'.$integ[1]."</span></del>";
	}
	function godesignate()
	{
		
		echo modules::run('template/admin_render',"admin","godesignate");
	}

}