

        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">
                    
                    <nav class="sidebar-nav">
 
                        <ul class="sidebar-nav-main">
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/kontakt">Kontakt</a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/kontakt/newsletter">Newsletter</a></li>
                        </ul>
 
                    
                    </nav> <!-- .sidebar-nav -->
                    <p class="contact-intro">
                        Josipa Broza Tita 26<br>
                        81000 Podgorica, Crna Gora<br>
                        +382 20 658 501
                    </p>

                    <p class="contact-intro">
                        <a href="mailto:okov@okov.me"><i class="icon icon-envelope"></i> okov@okov.me</a>
                        <a href="http://www.facebook.com/okovdoo"><i class="icon icon-facebook"></i> facebook.com/okovdoo</a>
<!--                         <a href="http://www.twitter.com/okovdoo"><i class="icon icon-twitter"></i> twitter.com/okovdoo</a>-->                    
                    </p>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">
                    <h1 class="section-subtitle">
                        <?=$this->lang->line('contact_us')?>
                    </h1>

<?php if (!$this->input->post("submit")): ?>
                    <form class="contact-form" action="/<?php echo $this->uri->segment(1) ?>/kontakt" method="post" id="contact-form">


                        <div class="cf-field">
                            <label><?=$this->lang->line('name')?></label>
                            <input type="text" name="name">
                            <div class="cf-error dn">
                                <?=$this->lang->line('Field_Required')?>
                            </div>  
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label>E-mail:</label>
                            <input type="text" name="email">
                            <div class="cf-error dn">
                                <?=$this->lang->line('Field_Required')?>
                            </div>
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label><?=$this->lang->line('phone')?></label>
                            <input type="text" name="phone">
                            <div class="cf-error dn">
                                <?=$this->lang->line('Field_Required')?>
                            </div>
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label><?=$this->lang->line('msg')?></label>
                            <textarea name="msg"></textarea>
                            <div class="cf-error dn">
                                <?=$this->lang->line('Field_Required')?>
                            </div>
                        </div>

                        <button type="submit" class="btn-default" name="submit" value="1">
                            <?=$this->lang->line('send')?> <i class="icon icon-arrow-right"></i>
                        </button>

                    </form>
<?php else: ?>
                    <div class="thanks">
                        <h3>Hvala!</h3>
                        <p>Vaša poruka je poslata.</p>
                    </div>
<?php endif ?>
                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
