

        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">
                    <nav class="sidebar-nav">
 
                        <ul class="sidebar-nav-main">
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/kontakt">Kontakt</a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/kontakt/newsletter">Newsletter</a></li>
                        </ul>
 
                    
                    </nav> <!-- .sidebar-nav -->
                    <p class="contact-intro">
                        Josipa Broza Tita 26<br>
                        81000 Podgorica, Crna Gora<br>
                        +382 20 658 501
                    </p>

                    <p class="contact-intro">
                        <a href="mailto:okov@okov.me"><i class="icon icon-envelope"></i> okov@okov.me</a>
                        <a href="http://www.facebook.com/okovdoo"><i class="icon icon-facebook"></i> facebook.com/okovdoo</a>
<!--                         <a href="http://www.twitter.com/okovdoo"><i class="icon icon-twitter"></i> twitter.com/okovdoo</a>-->                    
                    </p>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">
                
                    <article class="site-article">

                        <h1 class="section-subtitle">Hvala!</h1>

                        <p>Vaša poruka je poslata. Javićemo vam se u najkraćem mogućem roku.</p>

                    </article>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

