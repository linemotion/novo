        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul id="locations" class="sidebar-nav-main">
<?php foreach ($locations as $location): ?>
                            <li data-geo-lat="<?php echo $location->geolat; ?>" data-geo-long="<?php echo $location->geolong; ?>">
                                <a href="#"><?php echo $location->name ?></a>
                                <div class="longdesc">
                                    <?php echo $location->$desc ?>
                                </div>
                            </li>    
<?php endforeach ?>

                        </ul>

                    </nav> <!-- .sidebar-nav -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="map-container">
                        <div id="map_canvas"></div>
                        <div class="map-info"></div>
                    </div> <!-- .map-container -->

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

