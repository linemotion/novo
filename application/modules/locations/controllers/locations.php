<?php

class Locations extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_locations');
	}
	function index()
	{
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("locations"),"link" => "/")
			); 
		$data["title"] = $this->lang->line("locations");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		
		$data['locations'] = modules::run("Locations/get","id");

		echo modules::run('template/render',"locations","index",$data);
	}

	function contact()
	{

		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));
		$data["title"] = $this->lang->line("contact");

		if (!$this->input->post("submit"))
		{
			$data['breadcrumbs'] = array(
				array("name" => $this->lang->line("contact"),"link" => "/")
				); 

			echo modules::run('template/render',"locations","contact",$data);
		}
		else
		{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Ime i prezime', 'required|xss_clean');
		$this->form_validation->set_rules('msg', 'Poruka', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('phone', 'Telefon', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
		    $this->contact();
		}
		else
		{

			$config = array(
                   'protocol'  => 'smtp',
                   'smtp_host' => 'cp1.ulimitserver.com',
                   'smtp_port' => 25,
                   'smtp_user' => 'robot@okov.me',
                   'smtp_pass' => 'r0b07123',
                   'smtp_timeout' => 10,
                   'charset'  => 'utf-8',
                   'priority' => '1',
                );

		    $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");

		    $this->email->from($this->input->post('email'),'Okov');
		    //$this->email->reply_to('rankovicmarko@yahoo.com',"bati");
		    $this->email->to('okov@okov.me');
		    $this->email->subject('kontakt');
		    $this->email->message(
				'Ime i Prezime: '.$this->input->post('name')."\r\n".
				'Telefon: '.$this->input->post('phone')."\r\n".
				'E-mail: '.$this->input->post('email')."\r\n".
				'Poruka: '.$this->input->post('msg')."\r\n"
				);

		    if($this->email->send())
		        $this->contact_send();		        
		    else
				show_error($this->email->print_debugger());

		}			
		}	

	}

	function contact_send()
	{	
	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		// $data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		// $data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");


    	$data['breadcrumbs'] = array(
			array("name" => "kontakt","link" => "/profil")
		);  

		echo modules::run('template/render',"locations","contact_send",$data);
	}

	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["locations"] = $this->get("id"); 
		echo modules::run('template/admin_render',"locations","admin",$data);
	}
	function edit()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["location"] = $this->get_by_id($this->uri->segment(3)); 
		echo modules::run('template/admin_render',"locations","edit",$data);
	}
	function create()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		echo modules::run('template/admin_render',"locations","create");
	}
	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['geolat'] = $this->input->post('geolat');
		$data['geolong'] = $this->input->post('geolong');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');

		$this->_update($id,$data);

		redirect('/locations/edit/'.$id);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['name'] = $this->input->post('name');
		$data['geolat'] = $this->input->post('geolat');
		$data['geolong'] = $this->input->post('geolong');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');

		$this->_insert($data);

		redirect('/locations/admin/');
	}
	function change()
	{

		$id = $this->input->post('id');
		$output = '<select name="tags" class="uniformselect" id="tag"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('subcat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$this->_delete($this->uri->segment(3));
		redirect('/locations/admin');
	}

	function get($order_by)
	{
		$tags = $this->mdl_locations->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_locations->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_locations->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_locations->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_locations->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_locations->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_locations->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_locations->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_locations->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_locations->_custom_tags($mysql_tags);
		return $tags;
	}

}