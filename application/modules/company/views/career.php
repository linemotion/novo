        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/profil"><?=$this->lang->line('profile')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/menadzment"><?=$this->lang->line('management')?></a></li>
                            <li  class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/karijera"><?=$this->lang->line('career')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/brendovi"><?=$this->lang->line('brands')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1); ?>/letsdoit"><?=$this->lang->line('letsdoit')?></a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>


                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">
                        <h1 class="section-subtitle"><?php echo $career->$subtitle; ?></h1>
                        <?php echo $career->$desc; ?>

                    </article>

                    <form data-parsley-validate class="career-form" action="/<?php echo $this->uri->segment(1) ?>/career/cv" method="post" enctype="multipart/form-data">

                        <div class="cf-field">
                            <label><?=$this->lang->line('name')?></label>
                            <input type="text" name="name" required>
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label>E-mail:</label>
                            <input type="email" name="email" required>
                        </div> <!-- .cf-field -->

                        <div class="cf-field">
                            <label><?=$this->lang->line('phone')?></label>
                            <input type="text" name="phone" required>
                        </div> <!-- .cf-field -->
                        <div class="cf-field">
                            <label value="positions"><?=$this->lang->line('position')?></label>
                            <select name="position" required>
                            <option value="">Izaberite poziciju</option>
<?php foreach (modules::run("career/get","id") as $pos): ?>
                                                    <option value="<?php echo $pos->position; ?>"><?php echo $pos->position; ?></option>
<?php endforeach ?>
                            </select>
                        </div>
                        <div class="cf-field">
                            <label><?=$this->lang->line('CV')?> <small>(pdf, word ili jpg; ispod 200kb)</small></label>
                            <input type="file" name="userfile" required>
                        </div> <!-- .cf-field -->

                        <button type="submit" class="btn-default">
                            <?=$this->lang->line('send')?> <i class="icon icon-arrow-right"></i>
                        </button>

                    </form>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

