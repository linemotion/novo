        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/profil"><?=$this->lang->line('profile')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/menadzment"><?=$this->lang->line('management')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/karijera"><?=$this->lang->line('career')?></a></li>
                            <li  class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/brendovi"><?=$this->lang->line('brands')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1); ?>/letsdoit"><?=$this->lang->line('letsdoit')?></a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>


                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle"><?php echo $brand_content->$subtitle ?></h1>

                        <div class="group group-brands">
<?php foreach ($brands as $brand): ?>
                            <div class="col-1-5">
                                <a href="<?php echo $brand->link ?>" class="brand-item">
                                    <img src="/img/brands/<?php echo $brand->image ?>">
                                </a>
                            </div> <!-- .col-1-5 -->	
<?php endforeach ?>

                        </div> <!-- .group -->

                    </article>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->