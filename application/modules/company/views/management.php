        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/profil"><?=$this->lang->line('profile')?></a></li>
                            <li  class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/menadzment"><?=$this->lang->line('management')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/karijera"><?=$this->lang->line('career')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/brendovi"><?=$this->lang->line('brands')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1); ?>/letsdoit"><?=$this->lang->line('letsdoit')?></a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>


                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">
                        <h1 class="section-subtitle"><?php echo $management->$subtitle; ?></h1>

                        <?php echo $management->$desc; ?>


                    </article>

                        <ul class="team-list">
<?php foreach (modules::run("management/get", "id") as $item): ?>
                            <li>
                                <b><?php echo $item->name ?></b>
                                <small><?php echo $item->$position; ?></small>
                                <a href="mailto:<?php echo $item->email ?>"><?php echo $item->email ?></a>
                            </li>    
<?php endforeach ?>
                        </ul> <!-- .team-list -->

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
