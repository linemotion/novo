<?php

class Company extends MX_Controller
{
	var $module;


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_company');

		$this->module = "company";
	}
	function index()
	{
	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");


		$data["title"] = $this->lang->line("company");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("company"),"link" => "/".$this->uri->segment(1)."/profil"),
			array("name" => $this->lang->line("profile"),"link" => "/")
			); 

		$data['profile'] = array_shift($this->get_where_custom("name","profile"));
		$data['gallery'] = modules::run("company/get_gallery");

		echo modules::run('template/render',$this->module,"index",$data);
	}
	function career()
	{

	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");
		$data["position"] = ($this->uri->segment(1) == "en" ? "position_en" : "position");

		$data["title"] = $this->lang->line("career");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("company"),"link" => "/".$this->uri->segment(1)."/profil"),
			array("name" => $this->lang->line("career"),"link" => "/")
			); 
		if ($this->uri->segment(1) == "en")
		{
array_shift($this->get_where_custom("name","career"));			
		}
		$data['career'] = array_shift($this->get_where_custom("name","career"));



		echo modules::run('template/render',$this->module,"career",$data);
	}
	function brands()
	{

	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");

		$data["title"] = $this->lang->line("brands");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("company"),"link" => "/".$this->uri->segment(1)."/profil"),
			array("name" => $this->lang->line("brands"),"link" => "/profil")
			); 

		$data['brand_content'] = array_shift($this->get_where_custom("name","brands"));
		$data['brands'] = modules::run("brands/get","position");



		echo modules::run('template/render',$this->module,"brands",$data);
	}
	function management()
	{

	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");
		$data["position"] = ($this->uri->segment(1) == "en" ? "position_en" : "position");

		$data["title"] = $this->lang->line("management");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("company"),"link" => "/".$this->uri->segment(1)."/profil"),
			array("name" => $this->lang->line("management"),"link" => "/")
			); 

		$data['management'] = array_shift($this->get_where_custom("name","management"));



		echo modules::run('template/render',$this->module,"management",$data);
	}
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => "Kampanje","link" => "")
		// 	); 

		$data['profile'] = array_shift($this->get_where_custom("name","profile"));



		echo modules::run('template/admin_render',$this->module,"profile",$data);
	}
	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => "Kampanje","link" => "")
		// 	); 

		$data['gallery'] = $this->get_gallery();



		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}
	function profile()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data["title"] = $this->lang->line("profile");
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => "Kampanje","link" => "")
			); 

		$data['campaigns'] = $this->get("id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}

	function get_campaign() 
	{
		$campaign = $this->mdl_company->get_campaign($id);
		return $campaign;
	}


    function get_blocks($cam_id) 
    {    
		$blocks = $this->mdl_company->get_blocks($cam_id);		
		return $blocks;
    }

    function get_specs_front($spec_id) 
    {    
    	$id = $this->uri->segment(5);

        $this->db->where("pro_id",$id);
        $this->db->where("spec_id",$spec_id);

        $query = $this->db->get("spec_prod");

        if($query->num_rows() > 0)
            return $query->row();
    }

	// function action()
	// {
	// 	$data['breadcrumbs'] = array(
	// 		array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
	// 		array("name" => "Akcija","link" => "/".$this->uri->segment(1)."/proizvodi/akcija")
	// 		); 

	// 	$url = $this->uri->segment(4);


	// 	$data['products'] = $this->get_by_action();

	// 	echo modules::run('template/render',$this->module,"action",$data);
	// }
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

	    $this->db->where('id', $this->uri->segment(3));
	    $this->db->delete("profile_gallery");

		redirect('/company/gallery');
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$block = $this->get_by_id($this->uri->segment(3));
		$this->_delete($block->id);

		redirect('/campaigns/edit/'.$block->cam_id);
	}		
	public function pagination()
	{
		redirect('/<?php echo $this->uri->segment(1);?>/proizvodi/pretraga/'.url_title(rs_char($this->input->post('search'))));
	}

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		// 	$data['breadcrumbs'] = array(
		// array("name" => "Proizvodi","link" => "/admin/products"),
		// array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		// array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		// array("name" => $data["product"]->name,"link" => "")
		// );

		$data["block"] = modules::run("blocks/get_by_id",$id);
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		// 	$data['breadcrumbs'] = array(
		// array("name" => "Proizvodi","link" => "/admin/products"),
		// array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		// array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		// array("name" => $data["product"]->name,"link" => "")
		// );

		//$data["campaign"] = modules::run("campaigns/get_by_id",$id);
		$data["block"] = modules::run("campaign/get_by_id",$id);
		echo modules::run('template/admin_render',$this->module,"create",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['subtitle'] = $this->input->post('subtitle');
		$data['subtitle_en'] = $this->input->post('subtitle_en');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');
		
		$this->_update(1,$data);

		redirect('/company/admin/');
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
				
		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',"profile");

    	$this->db->insert("profile_gallery", $data);

		redirect('/company/gallery/');
	}
	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['name'] = $this->input->post('name');
		$data['link'] = $this->input->post('link');
		$data['cam_id'] = $this->uri->segment(3);
		
		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);

		$id = $this->_insert($data);
		redirect('/blocks/edit/'.$id);
	}
	function get($order_by)
	{
		$products = $this->mdl_company->get($order_by);
		return $products;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$products = $this->mdl_company->get_with_limit($limit, $offset, $order_by);
		return $products;
	}

	function get_by_id($id)
	{
		$products = $this->mdl_company->get_where($id);
		return $products;
	}
	function get_gallery()
	{
		$products = $this->mdl_company->get_gallery();
		return $products;
	}

	function get_where_custom($col, $value) 
	{
		$products = $this->mdl_company->get_where_custom($col, $value);
		return $products;
	}

	function _insert($data)
	{
		return $this->mdl_company->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_company->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_company->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_company->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_company->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query) 
	{
		$products = $this->mdl_company->_custom_query($mysql_query);
		return $products;
	}

}