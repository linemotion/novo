<?php

class Management extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_management');
	}
	function content()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["management"] = array_shift(modules::run('company/get_where_custom',"name","management")); 
		echo modules::run('template/admin_render',"management","management",$data);
	}
	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["management"] = $this->get("id"); 
		echo modules::run('template/admin_render',"management","admin",$data);
	}
	function edit()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["employee"] = $this->get_by_id($this->uri->segment(3)); 
		echo modules::run('template/admin_render',"management","edit",$data);
	}
	function create()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		echo modules::run('template/admin_render',"management","create");
	}
	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['position'] = $this->input->post('position');
		$data['position_en'] = $this->input->post('position_en');

		$this->_update($id,$data);

		redirect('/management/edit/'.$id);
	}

	function edit_content()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['subtitle'] = $this->input->post('subtitle');
		$data['subtitle_en'] = $this->input->post('subtitle_en');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');

	    $this->db->where('name', "management");
	    $this->db->update("company", $data);

		redirect('/management/content/');
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['position'] = $this->input->post('position');
		$data['position_en'] = $this->input->post('position_en');

		$this->_insert($data);

		redirect('/management/admin/');
	}
	function change()
	{

		$id = $this->input->post('id');
		$output = '<select name="tags" class="uniformselect" id="tag"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('subcat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$this->_delete($this->uri->segment(3));
		redirect('/management/admin');
	}

	function get($order_by)
	{
		$tags = $this->mdl_management->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_management->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_management->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_management->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_management->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_management->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_management->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_management->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_management->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_management->_custom_tags($mysql_tags);
		return $tags;
	}

}