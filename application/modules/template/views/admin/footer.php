

        <div class="md-overlay"></div> <!-- overlay for modals -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/admin-assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="/admin-assets/js/vendor/parsley.min.js"></script>
        <script src="/admin-assets/js/vendor/parsley-sr.js"></script>
        <script src="/admin-assets/js/vendor/fancybox.min.js"></script>
        <script src="/admin-assets/js/vendor/jquery-ui.min.js"></script>
        <script src="/admin-assets/js/vendor/classie.js"></script>
        <script src="/admin-assets/js/vendor/modals.js"></script>
        <script src="/admin-assets/js/vendor/chosen.jquery.min.js"></script>
        <script src="/admin-assets/js/plugins.js"></script>
        <script src="/admin-assets/js/main.js"></script>
        <script src="/ckeditor/ckeditor.js"></script>

<?php if ($this->uri->segment(1) == "news" or $this->uri->segment(1) == "yourself"): ?>
         <script src="/admin-assets/js/news-single.js"></script>   
<?php endif ?>

<?php if ($this->uri->segment(1) == "percent" or $this->uri->segment(1) == "yourself" or $this->uri->segment(1) == "company" or $this->uri->segment(1) == "locations" or $this->uri->segment(1) == "management" or $this->uri->segment(1) == "career" or $this->uri->segment(1) == "brands" or $this->uri->segment(1) == "letsdoit"): ?>
        <script src="/admin-assets/js/company.js"></script>
<?php endif ?>

<?php if ($this->uri->segment(1) == "products" ): ?>
        <script src="/admin-assets/js/products-product.js"></script>
<?php endif ?>

<?php if ($this->uri->segment(1) == "categories" || $this->uri->segment(1) == "filters"): ?>
        <script src="/admin-assets/js/products-all.js"></script>
<?php endif ?>

<?php if ($this->uri->segment(1) == "actions" ): ?>
        <script src="/admin-assets/js/actions.js"></script>
<?php endif ?>

    </body>
</html>