	<div class="vernav2">
		        <a href="/products/create/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(1); ?>" class="stdbtn newproduct">Dodaj novi proizvod</a>

    	<ul>
    		<?php foreach (modules::run("categories/get","position") as $category): ?>
<?php if (modules::run('categories/get_sidebar_cat_id',$this->uri->segment(3)) == $category->id): ?>
	<li class="current">
<?php else: ?>
	<li>
<?php endif ?>

					<a href="/categories/edit/<?php echo $category->id; ?>"><?php echo $category->name?></a><span class="arrow"></span></li>
    			<?php if (modules::run('categories/get_sidebar_cat_id') == $category->id): ?>
	                <ul class="cat-subnav">
	    				<?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
<?php if (modules::run('subcategories/get_sidebar_subcat_id',$this->uri->segment(3)) == $subcategory->id): ?>
	<li class="current">
<?php else: ?>
	<li>
<?php endif ?>

							<a href="/subcategories/edit/<?php echo $subcategory->id; ?>"><?php echo $subcategory->name?></a></li>
						<?php endforeach ?>
	                </ul>					
				<?php endif ?>
			<?php endforeach ?>	
        </ul>
    </div><!--leftmenu-->
