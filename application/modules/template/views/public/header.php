<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; ?> - OKOV d.o.o. - Podgorica, Crna Gora</title>
        <meta name="description" content="OKOV d.o.o. - okovi, alati, oprema za domaćinstvo i baštu. Josipa Broza Tita 26, Podgorica, Crna Gora.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if ($meta): ?>
            <meta name="test" content="test">
            <meta property="og:image" content="http://www.okov.me/img/products/<?php echo $product->image; ?>" />
        <?php endif ?>

        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/assets/css/main.css?v=11">
        <link rel="stylesheet" href="/assets/css/additional.css?v=2">
    </head>
    <body>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1009948045697339');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1009948045697339&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

        <header class="site-header">
            <div class="content group">
                <h1 class="site-logo">
                    <a href="/"><img src="/assets/img/okov-logo-2.svg" alt="Okov"></a>
                    <img src="/assets/img/slogan.svg" alt="Majstoru od majstora" class="site-slogan">
                </h1>
                <nav class="site-nav group">
                    <ul>
                        <li class="has-subnav">
                            <a href="/<?php echo $this->uri->segment(1); ?>/profil"><?=$this->lang->line('company')?> <i class="icon icon-arrow-down"></i></a>
                            <ul class="site-subnav">
                                <li><a href="/<?php echo $this->uri->segment(1); ?>/profil"><?=$this->lang->line('profile')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1); ?>/menadzment"><?=$this->lang->line('management')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1); ?>/karijera"><?=$this->lang->line('career')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1); ?>/brendovi"><?=$this->lang->line('brands')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1); ?>/letsdoit"><?=$this->lang->line('letsdoit')?></a></li>
                            </ul>
                        </li>
<?php echo ($this->uri->segment(2) == "proizvodi" || $this->uri->segment(2) == "proizvodi-rasprodaja" || $this->uri->segment(2) == "proizvodi-akcija") ? '<li class="site-nav-selected has-subnav has-sub-subnav">' : '<li class="has-subnav has-sub-subnav">' ?>                        
                            <a href="/<?php echo $this->uri->segment(1);?>/proizvodi "><?=$this->lang->line('products')?> <i class="icon icon-arrow-down"></i></a>
                            <ul class="site-subnav">
<?php foreach (modules::run("categories/get","position") as $category): ?>
                                <li>
                                    <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->$name?></a>
                                    <ul class="site-sub-subnav">
    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                                        <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->$name?></a>  </li>
    <?php endforeach; ?>
                                    </ul>
                                </li>
<?php endforeach; ?>   
                                <li class="promo-nav"><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja"><?=$this->lang->line('sales')?></a></li>
                            </ul>
                        </li>
    <?php echo ($this->uri->segment(2) == "akcije") ? '<li class="site-nav-selected ">' : '<li>' ?>                        
                            <a href="/<?php echo $this->uri->segment(1);?>/akcija"><?=$this->lang->line('action')?></a>
                        </li>
    <?php echo ($this->uri->segment(2) == "posto") ? '<li class="site-nav-selected ">' : '<li>' ?>
                            <a href="/<?php echo $this->uri->segment(1)?>/posto"><?=$this->lang->line('percent')?> <i class="icon icon-arrow-down"></i></a>
                            <ul class="site-subnav">
                                <li><a href="/<?php echo $this->uri->segment(1)?>/posto"><?=$this->lang->line('Card')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1)?>/posto/prijava"><?=$this->lang->line('Application')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1)?>/posto/pravila"><?=$this->lang->line('rules')?></a></li>
                            </ul>
                        </li>                              
<?php echo ($this->uri->segment(2) == "novosti") ? '<li class="site-nav-selected ">' : '<li>' ?>                        
                            <a href="/<?php echo $this->uri->segment(1);?>/novosti"><?=$this->lang->line('news')?></a>
                        </li>
 
<?php echo ($this->uri->segment(2) == "uradisam") ? '<li class="site-nav-selected ">' : '<li>' ?>                        
                            <a href="/<?php echo $this->uri->segment(1);?>/uradisam"><?=$this->lang->line('yourself')?></a>
                        </li>

<?php echo ($this->uri->segment(2) == "lokacije") ? '<li class="site-nav-selected ">' : '<li>' ?>                        
                            <a href="/<?php echo $this->uri->segment(1);?>/lokacije"><?=$this->lang->line('locations')?></a>
                        </li>
<?php echo ($this->uri->segment(2) == "kontakt") ? '<li class="site-nav-selected has-subnav">' : '<li class="has-subnav">' ?>                        
                            <a href="/<?php echo $this->uri->segment(1);?>/kontakt"><?=$this->lang->line('contact')?><i class="icon icon-arrow-down"></i></a>
                             <ul class="site-subnav">
                                <li><a href="/<?php echo $this->uri->segment(1)?>/kontakt/"><?=$this->lang->line('contact')?></a></li>
                                <li><a href="/<?php echo $this->uri->segment(1)?>/kontakt/newsletter"><?=$this->lang->line('newsletter')?></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <div class="site-header-meta">
                    <a href="/<?php echo $this->uri->segment(1); ?>/letsdoit" class="ldi"><img src="/assets/img/ldi.png" alt="Let's do it!"></a>
<?php if ($this->uri->segment(1) == "en"): ?>
                    <a href="/mn<?php echo substr($this->uri->uri_string(), strpos($this->uri->uri_string(), "/")); ?>" class="sh-lang">MN</a>    
<?php else: ?>
                    <a href="/en<?php echo substr($this->uri->uri_string(), strpos($this->uri->uri_string(), "/")); ?>" class="sh-lang">EN</a>
<?php endif ?>
                    <a href="http://www.facebook.com/okovdoo"><i class="icon icon-facebook"></i></a>
                    <div class="search-container">
                        <a href="#" class="search-btn"><i class="icon icon-search"></i></a>
                        <form action="/<?php echo $this->uri->segment(1) ?>/products/search" method="post">
                            <input type="text" name="search" class="search-input" placeholder="Unesite riječ">
                            <button type="submit" class="search-btn-submit"><i class="icon icon-search"></i></button>
                        </form>
                    </div>
                </div> <!-- .site-header-meta -->
            </div> <!-- .content -->
        </header> <!-- .site-header -->

        <section class="inside-subheader">
            <div class="content group">

            <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>

            </div> <!-- .content -->
        </section> <!-- .home-subheader -->