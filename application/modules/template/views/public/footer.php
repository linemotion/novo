        <footer class="site-footer">

            <section class="site-footer-top">

                <div class="content group">

                    <nav class="footer-cats group">
<?php $ft_categories = modules::run("categories/get","position"); ?>
<?php (count($ft_categories)%3 == 0) ? $moduo = count($ft_categories)/3 : $moduo = count($ft_categories)/3 + 1 ; ?>
<?php for ($i=0; $i < count($ft_categories); $i++): ?>
<?php if ($i%$moduo == 0 ): ?>
    <?php if ($i != 0): ?>
         </ul><ul>
     <?php else: ?>
        <ul>
    <?php endif ?>                
<?php endif ?>
                                <li>
                                    <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $ft_categories[$i]->url;?>"><?php echo $ft_categories[$i]->$name?></a>
                                </li>  
<?php endfor ?>
                    </nav> <!-- .footer-cats -->

                    <div class="footer-info">
                        <img src="/assets/img/okov-logo.svg" alt="Okov" class="footer-info-img">
                        <p>
                            Josipa Broza Tita 26<br>
                            81000 Podgorica, Crna Gora<br>
                            +382 20 658 501<br>
                            <a href="http://www.facebook.com/okovdoo">facebook.com/okovdoo</a>
                        </p>
                    </div>

                </div> <!-- .content -->

            </section> <!-- .site-footer-top -->

            <section class="site-footer-meta">

                <div class="content group">

                    <nav class="footer-nav">
                        <ul>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/"><i class="icon icon-house"></i></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/profil"><?=$this->lang->line('company')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/proizvodi"><?=$this->lang->line('products')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/akcija"><?=$this->lang->line('action')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/novosti"><?=$this->lang->line('news')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/lokacije"><?=$this->lang->line('locations')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/kontakt"><?=$this->lang->line('contact')?></a></li>
                        </ul>
                    </nav>

                    <div class="footer-disclaimer">
                        Cijene na sajtu uključuju PDV i informativnog su karaktera; mogu se razlikovati od stvarnih cijena.<br>
                        Opisi i fotografije mogu odstupati od originala. Zadržavamo pravo greške.
                    </div> <!-- .footer-disclaimer -->

                </div> <!-- .content -->

            </section> <!-- .site-footer-meta -->

        </footer> <!-- .site-footer -->
        
        <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="/assets/js/vendor/owl.min.js"></script>
        <script src="/assets/js/vendor/fancybox.min.js"></script>
        <script src="/assets/js/vendor/pageflip5-min.js"></script>
        <script src="/assets/js/vendor/key.js"></script>
        <script src="/assets/js/vendor/jquery.fitvids.js"></script>
        <script src="/assets/js/vendor/enquire.min.js"></script>
        <script src="/assets/js/default.js?v=5"></script>
        <script src="/assets/js/inside.js?v=3"></script>
<?php if ($this->uri->segment(2) == "karijera"): ?>
        <script src="/assets/js/vendor/parsley.min.js"></script>
        <script src="/assets/js/vendor/parsley-sr.js"></script>
<?php endif ?>
<?php if ($this->uri->segment(2) == "akcija"): ?>
        <script src="/assets/js/actions.js"></script>  
<?php endif ?>
<?php if ($this->uri->segment(2) == "posto"): ?>
        <script src="/assets/js/posto.js"></script>  
<?php endif ?>
<?php if ($this->uri->segment(2) == "kontakt"): ?>
        <script src="/assets/js/newsletter-page.js?v=2"></script>
<?php endif ?>
<?php if ($this->uri->segment(2) == "lokacije"): ?>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="/assets/js/locations.js"></script><?php endif ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-29378350-1', 'auto');
          ga('send', 'pageview');

        </script>
        <script data-cfasync="false" type="text/javascript" src="//filamentapp.s3.amazonaws.com/e1a82686d4149492df79c58baf2578e2.js" async="async"></script>

<!-- Google Code for Remarketing Tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 953291135;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/953291135/?guid=ON&amp;script=0"/>
</div>
</noscript>
    </body>
</html>