<?php

class Template extends MX_Controller
{
    function __construct()
    {
		parent::__construct();
    }

    public function index()
    {
		$this->load->view('template');
    }
    public function admin_breadcrumbs()
    {
        $module = $this->uri->segment(1);
        $id = $this->uri->segment(3);
       /* if ($this->uri->segment(2) == "create" AND $module == "products") 
            return $this->get_dropdown_cat_id();*/

        switch ($module) {
            case 'categories':
                $data['bd_category'] = modules::run('categories/get_by_id',$id);
                break;
            case 'categoriesBRW':
                $data['bd_category'] = modules::run('categoriesBRW/get_by_id',$id);
                break;
            case 'subcategories':
                $data['bd_subcategory'] = modules::run('subcategories/get_by_id',$id);
                $data['bd_category'] = modules::run('categories/get_by_id',$data['bd_subcategory']->cat_id);
                break;
             case 'subcategoriesBRW':
                $data['bd_subcategory'] = modules::run('subcategoriesBRW/get_by_id',$id);
                $data['bd_category'] = modules::run('categoriesBRW/get_by_id',$data['bd_subcategory']->cat_id);
                break;
           case 'products':
                $data['bd_product'] = modules::run('products/get_by_id',$id);
                $data['bd_subcategory'] = modules::run('subcategories/get_by_id',$data['bd_product']->subcat_id);
                $data['bd_category'] = modules::run('categories/get_by_id',$data['bd_product']->cat_id);
                break;                          
           case 'productsBRW':
                $data['bd_product'] = modules::run('productsBRW/get_by_id',$id);
                $data['bd_subcategory'] = modules::run('subcategoriesBRW/get_by_id',$data['bd_product']->subcat_id);
                $data['bd_category'] = modules::run('categoriesBRW/get_by_id',$data['bd_product']->cat_id);
                break;  
            default:
                # code...
                break;
        }

        $this->load->view('admin/breadcrumbs',$data);
    } 

    public function breadcrumbs($breadcrumbs)
    {
        $data['breadcrumbs'] = $breadcrumbs;

        $this->load->view('public/breadcrumbs',$data);
    }            

    public function admin()
    {
        $this->load->view('admin/template');
    }

    public function admin_header()
    {
        $this->load->view('admin/header');
    }

    public function admin_footer()
    {
        $this->load->view('admin/footer');
    }
   
    public function render($module,$view,$data="")
    {
    	$data['module'] = $module;
    	$data['view'] = $view;

		$this->load->view('public/template',$data);
    }
    public function admin_render($module,$view,$data="")
    {
        $data['module'] = $module;
        $data['view'] = $view;

        $this->load->view('admin/template',$data);
    }
}