<?php /**
* 
*/
class Units_mdl extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function get_specs()
	{
		$q = $this->db->get('spec_prod');
		return $q->result();
	}

	function getspecs()
	{
		$q = $this->db->get('specifications');
		return $q->result();
	}

	function get_sorts_all()
	{
		// $this->db->where('subcat_id',$id);
		$q = $this->db->get('sorts');
		return $q->result();
	}

	function get_integer_specs()
	{
		$this->db->where('type',"N");
		$q = $this->db->get('specifications');
		return $q->result();
	}

}