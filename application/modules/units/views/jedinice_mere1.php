<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css?v=11">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
            <ul>
                <li>
                        <a href="/units"><i class="fa fa-lg fa-wrench"></i>Grupe</a>
                </li>            
                <li class="sidebar-nav-selected">
<!--                         <a href="/units/all_sorts"><i class="fa fa-lg fa-scissors"></i>Sve Vrste</a>
 -->                        <ul class="sidebar-nav-groups">
<?php foreach($specs as $spec):?>
<?php if ($this->uri->segment(3) == $spec->sort_id):?>
                            <li>
                            <a href="/units/jm_2/<?php echo $spec->id; ?>"><?php echo $spec->name; ?></a>
                            </li>      
<?php endif;?>
<?php endforeach;?>                                              
                        </ul>
                </li>
                <li>
                        <a href="/units/integer_specs"><i class="fa fa-lg fa-sort-numeric-asc"></i>Brojčane vrednosti</a>
                </li>
                
            </ul>
            </nav>

        </aside> <!-- .sidebar -->

<section class="main">

            <header class="main-header group">

            </header>

<div class="main-content">

    <div class="c-block group">
        <h2 class="page-title">Specifikacije</h2>
    </div>
                <nav class="breadcrumbs">
                    <ul>
                        <li><a href="/units/all_sorts">Vrste</a></li>
                        <li><?php echo $sort->name; ?></li>
                    </ul>
                </nav>


        <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">
                        Specifikacije za <?php echo $sort->name; ?>
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Specifikacije</th>
                                <th class="th-action">Akcija</th>

                            </tr>
                        </thead>
                        <tbody id="sortable" class="sortable" data-controller="/versions/sort">
<?php foreach($specs as $spec):?>
<?php if ($this->uri->segment(3) == $spec->sort_id):?>
                            <tr id="item-<?php echo $spec->id;?>">

                                <td class="td-left td-name">
                                    <a data-version-id="<?php echo $spec->id; ?>" href="/units/jm_2/<?php echo $spec->id; ?>"><em>
                                    <?php if($spec->type == 'N'): ?>
                                        (INTEGER)
                                    <?php endif;?>  </em><?php echo $spec->name; ?></a>
                                </td>

                                <td class="td-action">
                                   
                                    <a href="/units/jm_2/<?php echo $spec->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                
                                  
                                </td>
                                

                            </tr>
<?php endif;?>
<?php endforeach;?>

                        </tbody>
                    </table>
        </div>
    
</div>
</section>
</body>
</html>