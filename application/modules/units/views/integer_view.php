<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css?v=11">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
            <ul>
                <li>
                        <a href="/units"><i class="fa fa-lg fa-wrench"></i>Grupe</a>                    
                </li>             
          <!--       <li>
                        <a href="/units/all_sorts"><i class="fa fa-lg fa-scissors"></i>Sve Vrste</a>
                </li> -->
                <li class="sidebar-nav-selected">
                    <a href="/units/integer_specs"><i class="fa fa-lg fa-sort-numeric-asc"></i>Brojčane vrednosti</a>
                </li>
               
            </ul>
            </nav>

        </aside> <!-- .sidebar -->

<section class="main">
            <header class="main-header group">


            </header>

<div class="main-content">
                    <div class="c-block group">
                    <h2 class="page-title">Brojčane vrednosti</h2>
                </div>

                <div class="c-block">
                    
                    <table class="main-table">
                           <caption class="tab-title">
                                Lista specifikacija brojčane vrednosti
                            </caption>
                            <thead>
                                <tr>
                                    <th class="th-left">Ime specifikacije</th>
                                    <th class="th-action">Filter</th>
                                </tr>
                            </thead>
                        <tbody class="sortable" data-controller="sorts">
<?php foreach($integer_specs as $specs):?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/units/specs_values/<?php echo $specs->id; ?>"><?php echo $specs->name; ?></a> 
                                </td>
                                <td class="td-action">
<?php if ($specs->attr_sort == 19):?>  
                               DA
<?php endif;?>                                                                
                                </td>
                            </tr>
<?php endforeach;?>                                                   
                        </tbody>
                    </table>


                </div>

</div>
</section>
</body>
</html>