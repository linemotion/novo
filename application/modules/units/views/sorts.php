<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css?v=11">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>       
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li class="sidebar-nav-selected">
                        <a href="/units"><i class="fa fa-lg fa-wrench"></i>Grupe</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
    <?php if ($item->id == $subcategory->cat_id): ?>

                            <li class="sidebar-nav-groups-selected">
                                <a href="/units/subgroups/<?php echo $item->id; ?>"><?php echo $item->name ?></a>
                                <ul class="sidebar-nav-subgroups">

        <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$subcategory->cat_id) as $subcat): ?>
            <?php if ($subcategory->id == $subcat->id): ?>
                                      <li class="sidebar-nav-subgroups-selected"><a href="/units/sorts/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>
               <?php else: ?>
                                     <li><a href="/units/sorts/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>

            <?php endif ?>
        <?php endforeach ?>
                                </ul>
                            </li>
    <?php else: ?>
                            <li>
                                <a href="/units/subgroups/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
    <?php endif ?>

<?php endforeach ?>
                        </ul>
                    </li>                
<!--                     <li>
                        <a href="/units/all_sorts"><i class="fa fa-lg fa-scissors"></i>Sve Vrste</a>
                    </li> -->
                    <li>
                        <a href="/units/integer_specs"><i class="fa fa-lg fa-sort-numeric-asc"></i>Brojčane vrednosti</a>
                    </li>



                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">


            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title"><?php echo $subcategory->name; ?> - VRSTE</h2>
                </div>




                <div class="c-block">
                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>
                
<?php if (modules::run("sorts/get_where_custom","subcat_id",$subcategory->id)): ?>
                    <table class="main-table">
                        <caption class="tab-title">
                            Lista vrsta
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime vrste</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-controller="sorts">
<?php foreach (modules::run("sorts/get_where_custom","subcat_id",$subcategory->id) as $sort): ?>
                            <tr id="<?php echo $sort->id ?>">
<?php if ($sort->done == 'done'):?>                            
                                <td class="td-left td-name status-done">
                                    <a href="/units/specs/<?php echo $sort->id; ?>"><?php echo $sort->name; ?><i style="font-size: 25px; margin-right: 7px; float: right; margin-top: 8px;" class="fa fa-check-circle"></i></a>
                                </td>
<?php else:?>            
                                <td class="td-left td-name">
                                    <a href="/units/specs/<?php echo $sort->id; ?>"><?php echo $sort->name; ?></a>
                                   
                                </td>
<?php endif;?>                                                    
                                <td class="td-action">
                                    <a href="/units/specs/<?php echo $sort->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                </td>
                            </tr>
<?php endforeach ?>
<?php else: ?>

                            <table class="main-table">
                                <caption class="tab-title">
                                    Lista vrsta
                                    <a href="#" class="btn-add md-trigger" data-modal="modal-add-subgroup"><i class="fa fa-plus-circle"></i> Dodaj vrstu</a>
                                </caption>
                            </table>

                            <div class="content-blank">

                                <p>Lista vrsta je prazna. Molimo Vas da dodate vrstu.</p>

                            </div> <!-- .content-blank -->
<?php endif; ?>

                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-add-subgroup">
            <div class="md-content">
                <h3>Dodaj Vrstu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime vrste</h4>

                        <form action="/sorts/create_proccess/<?php echo $subcategory->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>
                            
                            <h4 class="cb-title">Pojmovi za pretragu</h4>
                            <textarea rows="2" name="search" class="txtarea" data-required="true"></textarea>
                            <p class="form-helper"><i class="fa fa-info-circle"></i> Odvojeni razmakom, npr: električni električan alati alat</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-kind">
            <div class="md-content md-content-del">
                <h3>Obriši vrstu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu vrstu?</h4>

                        <form  method="post" id="delete_cat_form" action="">
                            <div class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-edit-subgroup">
            <div class="md-content md-content-edit">
                <h3>Izmeni podgrupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime podgrupe</h4>

                        <form action="/subcategories/edit_proccess/<?php echo $subcategory->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="<?php echo $subcategory->name; ?>" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
</body>
</html>
