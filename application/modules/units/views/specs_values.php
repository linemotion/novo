<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css?v=11">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>       
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li class="sidebar-nav-selected">
                        <a href="/units"><i class="fa fa-lg fa-wrench"></i>Grupe</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
    <?php if ($item->id == $subcategory->cat_id): ?>

                            <li class="sidebar-nav-groups-selected">
                                <a href="/units/subgroups/<?php echo $item->id; ?>"><?php echo $item->name ?></a>
                                <ul class="sidebar-nav-subgroups">

        <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$subcategory->cat_id) as $subcat): ?>
            <?php if ($subcategory->id == $subcat->id): ?>
                                      <li class="sidebar-nav-subgroups-selected"><a href="/units/sorts/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a>


                                      <ul class="sidebar-nav-subgroups">
        <?php foreach($sorts as $one_sort):?>
            <?php if ($one_sort->subcat_id == $subcategory->id):?>
            <?php if ($one_sort->id == $sort->id):?>
                                        <li class="sidebar-nav-subgroups-selected">
                                        <a href="/units/specs/<?php echo $one_sort->id; ?>"><?php echo $one_sort->name; ?></a>
                                        </li>
            <?php else:?>
                                        <li>
                                            <a href="/units/specs/<?php echo $one_sort->id; ?>"><?php echo $one_sort->name; ?></a>
                                        </li>
            <?php endif;?>
            <?php endif;?>
        <?php endforeach;?>                                         
                                      </ul>


                                      </li>
               <?php else: ?>
                                     <li><a href="/units/sorts/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>

            <?php endif ?>
        <?php endforeach ?>
                                </ul>
                            </li>
    <?php else: ?>
                            <li>
                                <a href="/units/subgroups/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
    <?php endif ?>

<?php endforeach ?>
                        </ul>
                    </li>                
<!--                     <li>
                        <a href="/units/all_sorts"><i class="fa fa-lg fa-scissors"></i>Sve Vrste</a>
                    </li> -->
                    <li>
                        <a href="/units/integer_specs"><i class="fa fa-lg fa-sort-numeric-asc"></i>Brojčane vrednosti</a>
                    </li>



                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

<section class="main">

            <header class="main-header group">

            </header>

<div class="main-content">
    
    <div class="c-block group">
        <h2 class="page-title"><?php echo $bc_spec->name; ?></h2>
    </div>

<!--                 <nav class="breadcrumbs">
                    <ul>
                        <li><a href="/units/specs/<?php //echo $bc_spec->sort_id; ?>">Specifikacije za <?php// echo modules::run('units/get_sorts_jm',$bc_spec->sort_id)->name; ?></a></li>
                        <li>Vrednosti specifikacije za <?php// echo $bc_spec->name; ?></li>
                    </ul>
                </nav> -->


        <div class="c-block">

                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>
        

<form action="/units/set_integer/<?php echo $this->uri->segment(3); ?>" method="post">

                    <div class="group">
                        <h3>Opcije specifikacije:</h3>
                        <label class="option-half">
                        <i class="fa fa-sort-numeric-asc"></i>
                            <input
<?php if ($bc_spec->type == 'N'):?>
                                checked
<?php endif;?>                                                            
                             type="checkbox" name="integer" value="N">Brojčane vrednosti
                        </label>
                        <label class="option-half">
                        <i class="fa fa-filter"></i>
                            <input
<?php if ($bc_spec->attr_sort == 20):?>
                                checked
<?php endif;?>                                                            
                             type="checkbox" name="filter" value="20">Filter
                        </label>
                    </div>
                    <br>
                    <table class="main-table">
                        <caption class="tab-title">
                        Vrednosti specifikacije
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Vrednosti specifikacije za <?php echo $bc_spec->name; ?></th>

                            </tr>
                        </thead>
                        <tbody id="sortable" class="sortable" data-controller="/versions/sort">
<?php foreach($spec_vals as $spec_val):?>
<?php if (modules::run('units/get_product',$spec_val->pro_id)):?>

                            <tr id="item-<?php echo $spec_val->id;?>">
                                <td class="td-left td-name">
                                    <input class="txtinput" type="text" name="name[]" data-version-id="<?php echo $spec_val->id; ?>" value="<?php echo htmlspecialchars($spec_val->value); ?>" style="margin-top: 30px;">

                                    <em style="opacity: 1;"><?php echo modules::run('units/get_product',$spec_val->pro_id)->name; ?></em>
                                </td>

                            </tr>
<?php else:?> 
                            <tr hidden id="item-<?php echo $spec_val->id;?>">
                                <td class="td-left td-name">
                                    <input class="txtinput" type="text" name="name[]" data-version-id="<?php echo $spec_val->id; ?>" value="<?php echo $spec_val->value; ?>" style="margin-top: 30px;">

                                </td>

                            </tr>                           
<?php endif;?>                                                            
<?php endforeach;?>

                        </tbody>
                    </table>


                    <button type="submit" class="big-submit">Pošalji</button>

                    <br>
            
        </form>

        </div>

    
</div>

</section>

</body>
</html>        