<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css?v=11">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
            <ul>
                <li>
                        <a href="/units"><i class="fa fa-lg fa-wrench"></i>Grupe</a>
                </li>                  
                <li class="sidebar-nav-selected">
<!--                         <a href="/units/all_sorts"><i class="fa fa-lg fa-scissors"></i>Sve Vrste</a>
 -->                        <ul class="sidebar-nav-groups">
<?php foreach(modules::run('units/specval') as $specs):?>
<?php if ($specs->sort_id == $bc_spec->sort_id):?> 
                            <li
<?php if ($specs->id == $bc_spec->id):?>
class="sidebar-nav-groups-selected"
<?php endif;?>                            
                            ><a href="/units/jm_2/<?php echo $specs->id; ?>"><?php echo $specs->name; ?></a></li>    
<?php endif;?>
<?php endforeach;?>                                                
                        </ul>                        
                </li>
                <li>
                        <a href="/units/integer_specs"><i class="fa fa-lg fa-sort-numeric-asc"></i>Brojčane vrednosti</a>
                </li>
          
            </ul>            
            </nav>

        </aside> <!-- .sidebar -->
<section class="main">

            <header class="main-header group">

            </header>

<div class="main-content">
    
    <div class="c-block group">
        <h2 class="page-title">Vrednosti specifikacije</h2>
    </div>

                <nav class="breadcrumbs">
                    <ul>
                        <li><a href="/units/all_sorts">Vrste</a></li>
                        <li><a href="/units/jm_1/<?php echo $bc_spec->sort_id; ?>"><?php echo modules::run('units/get_sorts_jm',$bc_spec->sort_id)->name; ?></a></li>
                        <li><?php echo $bc_spec->name; ?></li>
                    </ul>
                </nav>


        <div class="c-block">

<form action="/units/set_integer/<?php echo $this->uri->segment(3); ?>" method="post">
                    <table class="main-table">
                        <caption class="tab-title">
                        Vrednosti specifikacije
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Vrednosti specifikacije za <?php echo $bc_spec->name; ?></th>
                                <th class="th-action">Akcija</th>

                            </tr>
                        </thead>
                        <tbody id="sortable" class="sortable" data-controller="/versions/sort">
<?php foreach($spec_vals as $spec_val):?>
                            <tr id="item-<?php echo $spec_val->id;?>">

                                <td class="td-left td-name">
                                <br>
                                    <input class="txtinput" type="text" name="name[]" data-version-id="<?php echo $spec_val->id; ?>" value="<?php echo $spec_val->value; ?>"> <em>&nbsp za proizvod -> <?php echo modules::run('units/get_product',$spec_val->pro_id)->name; ?></em>
                                </td>

                                <td class="td-action">
                                   
                                </td>

                            </tr>
<?php endforeach;?>

                        </tbody>
                    </table>
                <div class="fi-1-2">

                    <div class="group">
                        <label class="option-half">
                        <i class="fa fa-sort-numeric-asc"></i>
                            <input
<?php if ($bc_spec->type == 'N'):?>
                                checked
<?php endif;?>                                                            
                             type="checkbox" name="integer" value="N">Integer
                        </label>
                        <label class="option-half">
                        <i class="fa fa-filter"></i>
                            <input
<?php if ($bc_spec->attr_sort == 19):?>
                                checked
<?php endif;?>                                                            
                             type="checkbox" name="filter" value="19">Filter
                        </label>
                    </div>
                    <button type="submit" class="big-submit">Pošalji</button>
                </div>  
        </form>


        </div>

    
</div>

</section>

</body>
</html>