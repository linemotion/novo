<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OKOV CMS</title>
        <meta name="description" content="OKOV CMS - administracija">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic&subset=latin,latin-ext'>
        <link rel="stylesheet" href="/admin-assets/css/main.css?v=11">
        <link rel="stylesheet" href="/admin-assets/css/additional.css">
        <script src="/admin-assets//js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>

                    <li class="sidebar-nav-selected">
                        <a href="/units"><i class="fa fa-lg fa-wrench"></i>Grupe</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $category): ?>
                            <li>
                                <a href="/units/subgroups/<?php echo $category->id; ?>"><?php echo $category->name;?></a>
                            </li>
<?php endforeach ?>
                        </ul>
                    </li>
<!--                     <li>
 --><!--                         <a href="/units/all_sorts"><i class="fa fa-lg fa-scissors"></i>Sve Vrste</a>
 --><!--                     </li>
 -->                    <li>
                        <a href="/units/integer_specs"><i class="fa fa-lg fa-sort-numeric-asc"></i>Brojčane vrednosti</a>
                    </li>                    

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Grupe</h2>
                </div>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">GRUPE PROIZVODA</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime grupe</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-controller="categories">

<?php foreach ($categories as $category): ?>
                            <tr id="<?php echo $category->id ?>">
                                <td class="td-left td-name">
                                    <a href="/units/subgroups/<?php echo $category->id; ?>"><?php echo $category->name;; ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/units/subgroups/<?php echo $category->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                </td>
                            </tr>
<?php endforeach ?>
                       </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-add-group">
            <div class="md-content">
                <h3>Dodaj grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime grupe</h4>

                        <form action="/categories/create" method="Post">

                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>
                            
                            <h4 class="cb-title">Ime grupe <i>en</i></h4>
                            <input type="text" name="name_en" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <br>
                            
                            <h4 class="cb-title">Pojmovi za pretragu</h4>
                            <textarea rows="2" name="search" class="txtarea" data-required="true"></textarea>
                            <p class="form-helper"><i class="fa fa-info-circle"></i> Odvojeni razmakom, npr: električni električan alati alat</p> 

                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-del-group">
            <div class="md-content md-content-del">
                <h3>Obriši grupu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu grupu?</h4>

                        <form action="" method="Post" id="delete_cat_form">
                            <div class="form-bottom">
                                <button type="submit" href="#" class="btn-del-large " ><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
</body>