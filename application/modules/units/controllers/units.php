<?php /**
* 
*/
class Units extends MX_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('units_mdl');
		
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

	}

	function get_specs()
	{
		$specs = $this->units_mdl->get_specs();
		return $specs;
	}

	function getspecs()
	{
		$specs = $this->units_mdl->getspecs();
		return $specs;
	}


	function specval()
	{
		$specs = $this->get_specs();
		$i = 0;
		// echo "<pre>";
		// echo "ovoliko ih ima " . count($specs);
		$niz = array();
		$spec_niz = array();
		foreach($specs as $spec)
		{
			if(preg_match('/[0-9]/', $spec->value[0]))
			{
				$niz[] = $spec->spec_id;
				$i++;
			}
		}
		// echo "ovoliko ima ovih drugih " . $i; //
		$niz2 = array_unique($niz);
		// echo "<pre>";
		// print_r($niz2);

		foreach($niz2 as $id)
		{
			$this->db->where('id',$id);
			$q = $this->db->get('specifications');
			// imena specifikacija koje pocinju sa brojem.
			$spec_niz[] = $q->result();

		}
		// novi niz kroz koji moze da se provuce petlja. 
		// niz specifikacija cije vrednosti pocinju brojem.
		// za ovaj niz trebalo bi da se odredi koji su stringovi a koji su integeri
		// takodje napraviti interfejs gde bi lako mogli da vide vrednosti specifikacija
		// da bi im bilo laske da obeleze kada vide ime specifikacije
		$final_array = array();

		foreach($spec_niz as $nizzz)
		{
			foreach($nizzz as $n)
			{
				if(!in_array($n, $final_array))
					array_push($final_array, $n);
			}
		}

		// echo "<pre>";
		// print_r($final_array);
		return $final_array;
		// echo "<pre>";
		// echo count($spec_niz);
		
	}

	function get_sorts_all()
	{
		$sorts = $this->units_mdl->get_sorts_all();
		return $sorts;
	}

	function get_breadcrumb_specification($id)
	{
		$this->db->where('id',$id);
		$q = $this->db->get('specifications');
		return $q->row();
	}

	function get_specvals($id)
	{
		$this->db->where('spec_id',$id);
		$q = $this->db->get('spec_prod');
		return $q->result();
	}

	function get_last_num()
	{
		$text = "10 x 300 asehas 10 mm";
		if(preg_match_all('/\d+/', $text, $numbers))
		{
			$lastnum = end($numbers[0]);
			echo "<pre>";
			echo $lastnum;
		}
	}

	function get_units()
	{

		// $array_of_values = array('10 x 10 g/m²','','','','','20 x 20 g/m²', '30 x 14 g/m²','1 x 2 g/m²', '1 x 2 cm (2 kom)','');
		$array_of_values = array("1","1","1");
		$units_array = array();
		foreach($array_of_values as $values)
		{
			if(preg_match_all('/\d+/', $values, $numbers))
			{
				$lastnum = end($numbers[0]);
			}
			$units = end(explode($lastnum, $values));

			if(!$units)
			{
				$units = end(explode(" ", $values));
			}
			if(!is_numeric($units))
			{
				$units_array[] = str_replace(" ", "", $units);
			}
			

		}

		$c = array_count_values(array_filter($units_array));
		$final_units = array_search(max($c), $c);


		echo "<pre>";
		print_r($final_units);
		echo die();

		// $text = "50 x 70 cm (2 komada)";
		// if(preg_match_all('/\d+/', $text, $numbers))
		// {
		// 	$lastnum = end($numbers[0]);
		// }

		// echo $lastnum;


		// $units = substr($text,strpos($text,$lastnum)+1);
		// echo $units;


		// $units = end(explode($lastnum,$text));
		// if(!$units)
		// {
		// 	$units = end(explode(" ", $text));
		// }
		// echo $units;
	}

	function getsubcats($id)
	{
		$this->db->where('id',$id);
		$q = $this->db->get('subcategories');
		return $q->row();
	}

	function get_sorts_jm($id)
	{
		$this->db->where('id',$id);
		$q = $this->db->get('sorts');
		return $q->row();
	}

	function get_integer_specs()
	{
		$integer_specs = $this->units_mdl->get_integer_specs();
		return $integer_specs;
	}

	function all_sorts()
	{
		$data['specs'] = $this->specval();
		$data['sorts'] = $this->get_sorts_all();

		$this->load->view('jedinice_mere',$data);
	}


	function index()
	{


		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/units/products")
			); 


		$data["categories"] = modules::run("categories/get","position");
		$this->load->view('groups',$data);

		// echo modules::run('template/admin_render',"admin","products",$data);
	}

	function subgroups()
	{


		$id = $this->uri->segment(3);

		$data['categories'] = modules::run("categories/get","position");

		$data['category'] = modules::run('categories/get_by_id',$id);

		$data['breadcrumbs'] = array(
				array("name" => "Grupe","link" => "/units"),
				array("name" => $data["category"]->name,"link" => "/units/subgroups/".$data["category"]->id)
		);
		
		$this->load->view('subgroups',$data);
	}

	function sorts()
	{	
		$id = $this->uri->segment(3);

		$data['categories'] = modules::run("categories/get","position");
		$data['subcategory'] = modules::run("subcategories/get_by_id",$id);

		$data["sorts"] = modules::run("sorts/get","id"); 

		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);


		$data['breadcrumbs'] = array(
		array("name" => "Grupe","link" => "/units"),
		array("name" => $bc_category->name,"link" => "/units/subgroups/".$bc_category->id),
		array("name" => $data['subcategory']->name,"link" => "/units/sorts/".$data['subcategory']->id),
		);

		$this->load->view('sorts',$data);
	}


	function specs()
	{
		
		$id = $this->uri->segment(3);
		$data['sort'] = $this->get_sorts_jm($id);		

		$data['categories'] = modules::run("categories/get","position");
		$data['subcategory'] = modules::run("subcategories/get_by_id",$data['sort']->subcat_id);
		$data['specs'] = $this->getspecs();

		// echo "<pre>";
		// print_r($data['specs']);
		// echo die();

		$data["sorts"] = $this->get_sorts_all();



		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);

		$data['breadcrumbs'] = array(
		array("name" => "Grupe","link" => "/units"),
		array("name" => $bc_category->name,"link" => "/units/subgroups/".$bc_category->id),
		array("name" => $data['subcategory']->name,"link" => "/units/sorts/".$data['subcategory']->id),
		array("name" => $data['sort']->name,"link"=> "/units/specs/".$data['sort']->id)
		);		

		$this->load->view('specs',$data);
	}

	function specs_values()
	{
		$id = $this->uri->segment(3);
		$data['spec_vals'] = $this->get_specvals($id);
		// echo "<pre>";
		// print_r($data['spec_vals']);
		// echo die();
		$data['bc_spec'] = $this->get_breadcrumb_specification($id);
		$data['sort'] = $this->get_sorts_jm($data['bc_spec']->sort_id);

		$data['categories'] = modules::run("categories/get","position");
		$data['subcategory'] = modules::run("subcategories/get_by_id",$data['sort']->subcat_id);
		$data['specs'] = $this->getspecs();

		$data["sorts"] = $this->get_sorts_all();


		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);

		$data['breadcrumbs'] = array(
		array("name" => "Grupe","link" => "/units"),
		array("name" => $bc_category->name,"link" => "/units/subgroups/".$bc_category->id),
		array("name" => $data['subcategory']->name,"link" => "/units/sorts/".$data['subcategory']->id),
		array("name" => $data['sort']->name,"link"=> "/units/specs/".$data['sort']->id),
		array("name" => $data['bc_spec']->name,"link" => "/units/specs_values".$data['bc_spec']->id)
		);			

		$this->load->view('specs_values',$data);		

	}

	function count_errors_test()
	{
		$array = array();
		$q = $this->db->get('spec_prod');
		$specs_vals = $q->result();
		foreach ($specs_vals as $vals)
		{
			if (!modules::run('units/get_product',$vals->pro_id) && $this->get_sorts_jm($this->get_breadcrumb_specification($vals->spec_id)->sort_id))
				{
					$array[] = $vals->spec_id;
				}
		}
		$return = array_unique($array);
		// echo "<pre>";
		// print_r(array_unique($array));
		// echo die();		
		return $return;


		// $this->load->view('error_count',$data);		
	}


	function jm_1()
	{
		$id = $this->uri->segment(3);
		$data['specs'] = $this->specval();
		$data['sort'] = $this->get_sorts_jm($id);

		$this->load->view('jedinice_mere1',$data);
	}

	function jm_2()

	{
		$id = $this->uri->segment(3);
		$data['spec_vals'] = $this->get_specvals($id);
		$data['bc_spec'] = $this->get_breadcrumb_specification($id);
		$this->load->view('jedinice_mere2',$data);
	}

	function count()
	{
		$array = array();
		$specvals = $this->db->get('spec_prod')->result();
		foreach($specvals as $val)
		{
			array_push($array, $val->spec_id);
		}
		$c = array_count_values($array);
		echo "<pre>";
		print_r($c);
		echo die();
	}

	function integer_specs()
	{

		$data['integer_specs'] = $this->get_integer_specs();
		$this->load->view('integer_view',$data);
	}

	function error_specs()
	{
		$spec_ids = $this->count_errors_test();
			// echo "<pre>";
			// print_r($spec_ids);
			// echo die();		

		foreach($spec_ids as $ids)
		{
			$this->db->where('id',$ids);
			$q = $this->db->get('specifications');
			$rows[] = $q->row();
		}
		// echo "<pre>";
		// print_r($rows);
		// echo die();
		$data['error_specs'] = $rows;
		$this->load->view('error_view',$data);
	}

	function sort_done()
	{
		$id = $this->uri->segment(3);
		$data['done'] = $this->input->post('done');
		$this->db->where('id',$id);
		$this->db->update('sorts',$data);
		redirect('/units/specs/'.$id);
	}

	function set_integer()
	{
		$id = $this->uri->segment(3);
		if($this->input->post('integer') == 'N')
		{
			$data['type'] = 'N';
		}
		else
		{
			$data['type'] = 'O';
		}
		if($this->input->post('filter') == 20)
		{
			$data['attr_sort'] = 20;
		}
		else
		{
			$data['attr_sort'] = 18;
		}
		$this->db->where('id',$id);
		$this->db->update('specifications',$data);



		$name = $this->input->post('name');


		$spec_vals = $this->get_specvals($id);

		// echo "<pre>";
		// print_r($name);
		// print_r($spec_vals);
		// echo die();		

		for($i = 0; $i < count($spec_vals); $i++)
		{
				if(array_key_exists($i, $name))
				{
					$spec_data['value'] = $name[$i];
					// echo "<pre>";
					// print_r($spec_vals[$i]);

					$this->db->where('id',$spec_vals[$i]->id);

					$this->db->update('spec_prod',$spec_data);
				}


		}




		$sort_id = $this->get_breadcrumb_specification($id)->sort_id;
		redirect('/units/specs_values/' . $id);
	}

	function get_product($id)
	{
		$this->db->where('id',$id);
		$q = $this->db->get('products');
		return $q->row();
	}

	function set_string()
	{
		$id = $this->uri->segment(3);
		$data['type'] = 'O';
		$this->db->where('id',$id);
		$this->db->update('specifications',$data);
		$sort_id = $this->get_breadcrumb_specification($id)->sort_id;
		redirect('/units/jm_1/' . $sort_id);
	}


}