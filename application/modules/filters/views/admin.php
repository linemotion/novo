        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
    <?php if ($item->id == $sort->cat_id): ?>

                            <li class="sidebar-nav-groups-selected">
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name ?></a>
                                <ul class="sidebar-nav-subgroups">

        <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$sort->cat_id) as $subcat): ?>
            <?php if ($sort->subcat_id == $subcat->id): ?>
                                      <li class="sidebar-nav-subgroups-selected"><a href="/subcategories/edit/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>
               <?php else: ?>
                                     <li><a href="/subcategories/edit/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>

            <?php endif ?>
        <?php endforeach ?>
                                </ul>
                            </li>
    <?php else: ?>
                            <li>
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
    <?php endif ?>

<?php endforeach ?>
                         </ul>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title"><?php echo $sort->name; ?> <a href="#" class="edit-pop md-trigger" data-modal="modal-edit-kind"><i class="fa fa-pencil"></i></a></h2>
                </div>

                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>


                <header class="tab-header group">
                    <a href="/filters/admin/<?php echo $sort->id; ?>" class="tab-1-2 tab-active">Filteri</a>
                    <a href="/sorts/edit/<?php echo $sort->id; ?>" class="tab-1-2">Specifikacija</a>
                </header>

                <div class="c-block">
<?php if (modules::run("filters/get_where_custom","sort_id",$sort->id)): ?>


                    <table class="main-table">
                        <caption class="tab-title">
                            Lista filtera
                            <a href="#" class="btn-add md-trigger" data-modal="modal-add-filter"><i class="fa fa-plus-circle"></i> Dodaj filter</a>
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime filtera</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
<?php foreach (modules::run("filters/get_where_custom","sort_id",$sort->id) as $filter): ?>
                                <td class="td-left td-filter">
                                    <h4 class="filter-name"><?php echo $filter->name; ?></h4>
                                    <ul class="filter-values sortable-small">
    <?php foreach (modules::run("filter_values/get_where_custom","filter_id",$filter->id) as $value): ?>
                                        <li id="<?php echo $value->id; ?>"><?php echo $value->value; ?><a href="/filter_values/edit/<?php echo $value->id; ?>" class="filter-value-edit md-trigger" data-modal="modal-edit-filter-value" data-value-edit="true"  data-id="<?php echo $value->id; ?>" data-controller="filter_values"><i class="fa fa-pencil"></i></a><a href="/filter_values/delete/<?php echo $value->id; ?>" class="filter-value-del"><i class="fa fa-times"></i></a></li>
    <?php endforeach ?>
                                    </ul>
                                    <a href="#" class="filter-add-value md-trigger" data-modal="modal-add-filter-value" data-id="<?php echo $filter->id; ?>" data-value="true">Dodaj vrednost <i class="fa fa-plus"></i></a>
                                </td>
                                <td class="td-action">
                                    <a href="/filters/delete/<?php echo $filter->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-filter" data-id="<?php echo $filter->id; ?>" data-controller="filters"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/filters/edit/<?php echo $filter->id; ?>" class="act-btn edit-btn md-trigger" data-modal="modal-edit-filter" data-filter="true" data-id="<?php echo $filter->id; ?>"><i class="fa fa-pencil"></i>Izmeni</a>
                                    <a href="#" class="move-btn" data-id="<?php echo $filter->id; ?>"><i class="fa fa-arrows"></i></a>
                                </td>
                            </tr>
<?php endforeach ?>
<?php else: ?>
                    <table class="main-table">
                        <caption class="tab-title">
                            Lista filtera
                            <a href="#" class="btn-add md-trigger" data-modal="modal-add-filter"><i class="fa fa-plus-circle"></i> Dodaj filter</a>
                        </caption>
                    </table>

                    <div class="content-blank">

                        <p>Lista filtera je prazna. Molimo Vas da dodate filter.</p>

                    </div> <!-- .content-blank -->

<?php endif ?>
                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-del-filter">
            <div class="md-content md-content-del">
                <h3>Obriši filter <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovaj filter?</h4>

                        <form  method="post" id="delete_cat_form" action="">
                            <div class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-edit-kind">
            <div class="md-content md-content-edit">
                <h3>Izmeni vrstu <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime vrste</h4>

                        <form action="/sorts/edit_proccess/<?php echo $sort->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="<?php echo $sort->name; ?>" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-add-filter">
            <div class="md-content">
                <h3>Dodaj filter <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime filtera</h4>

                        <form action="/filters/create_proccess/<?php echo $sort->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-edit-filter">
            <div class="md-content md-content-edit">
                <h3>Izmeni filter <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime filtera</h4>

                        <form action="" method="post" id="edit_form">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="<?php echo "Unesite Ime"; ?>" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-add-filter-value">
            <div class="md-content">
                <h3>Dodaj vrednost za filter<button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime vrednosti:</h4>

                        <form action="" method="post" id="value_form">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-plus-circle"></i> Dodaj</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
        <div class="md-modal md-effect-1" id="modal-edit-filter-value">
            <div class="md-content md-content-edit">
                <h3>Izmeni vrednost za filter<button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime vrednosti:</h4>

                        <form id="edit_filter_value" method="post">
                            <input id="click" type="text" name="name" class="txtinput" value="" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>