<?php
class Mdl_campaigns extends CI_Model 
{

    var $table; 

    function __construct() 
    {
        parent::__construct();
        $this->table = "campaigns";
    }



    function get_campaign() 
    {    
        $query = $this->db->get($this->table);
        
        return random_element($query->result());
    } 

    function get_blocks($cam_id) 
    {    
        $this->db->where("cam_id",$cam_id);

        $query = $this->db->get("campaign_blocks");

        $random = $query->result();

        if(count($random) >= 4)
        {  
            $counter = 0;
            while($counter != 4)
            {
                $temp = random_element($products);
                if(!in_array($temp,$random))
                {
                    array_push($random,$temp);
                    $counter++;
                }
            }
        return $random;
        }
        else
            return false;


    }

    function get_random()
    {
        $this->db->where('active',1);
        $this->load->helper('array');

        $this->db->where('cat_id', 39);
        $query=$this->db->get($this->table);

        $products = $query->result();
        
        $random = random_element($products);

        return $random;
    }
    function get_by_url($url)
    {
        $this->db->where('active',1);
        $this->db->where('url', $url);
        $query=$this->db->get($this->table);

        return $query->row();
    }

    function get($order_by = "position")
    {
        $this->db->order_by($order_by,"asc");
        $query=$this->db->get($this->table);
        return $query->result();
    }

    function get_with_limit($limit, $offset, $order_by) {
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by);
    $query=$this->db->get($this->table);
    return $query;
    }

    function get_where($id){
    $this->db->where('id', $id);
    $query=$this->db->get($this->table);
    return $query->row();
    }

    function get_where_custom($col, $value) {
        $this->db->order_by('id', "desc");
        //$this->db->where('active',1);
    $this->db->where($col, $value);
    $query=$this->db->get($this->table);
    return $query->result();
    }

    function _insert($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
    }

    function _update($id, $data){
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
    }

    function _delete($id){
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    }

    function count_where($column, $value) {
    $this->db->where($column, $value);
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function count_all() {
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function get_max($field = "id") 
    {
        $this->db->select_max($field);
        $query = $this->db->get($this->table);
        $row=$query->row();
        return $query->row()->id;
    }

    function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
    }
    function get_IN_filter($item)
    {
        $final_ids = array();
        $sql="
          SELECT pro_id 
          FROM value_prod
          WHERE val_id in (";
             
        $t = 0;

        foreach ($item as $item_id)
        {   
            if($t != count($item)-1)
                $sql .= $item_id.",";
            else
                $sql .= $item_id;
          $t++;  
        }

        $sql .= ") GROUP  BY pro_id;";

        $query = $this->db->query($sql);
        $pro_ids = $query->result();

        foreach ($pro_ids as $id) 
        {
            $final_ids[] = $id->pro_id;
        }

        return $final_ids;
    }
    function get_ALL_filter($filters_grouped)
    {
        $unrefined_ids = array();

        $i = 0;
        foreach($filters_grouped as $item)
        {
            $unrefined_ids[] = $this->get_IN_filter($item);
            $i++;
        }
        $intersect = call_user_func_array('array_intersect',$unrefined_ids);

        return $intersect;
    }


}

