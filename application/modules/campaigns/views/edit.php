        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                        <li>
                            <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                        </li>
                        <li class="sidebar-nav-selected">
                            <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                        </li>
                        <li>
                            <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        </li>
                        <li>
                            <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                        </li>
                        <li>
                            <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                        </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                        <li>
                            <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                        </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title"><?php echo $campaign->name; ?> <a href="#" class="edit-pop md-trigger" data-modal="modal-edit-campaign"><i class="fa fa-pencil"></i></a></h2>
                    <a href="/blocks/create/<?php echo $campaign->id ?>" class="btn-add"><i class="fa fa-plus-circle"></i> Dodaj stavku</a>
                </div>

                <form action="/campaigns/edit_image/<?php echo $campaign->id ?>" enctype="multipart/form-data" method="post">

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Slika kampanje</h4>

                            <a href="/img/people/<?php echo $campaign->image ?>" class="chosen-img">
                                <img src="/img/people/<?php echo $campaign->image ?>">
                            </a>

                            <input type="file" name="userfile" class="txtinput" required>

                            <p class="form-helper form-helper-important"><i class="fa fa-info-circle"></i> Format: <b>isključivo transparentni PNG</b>. Kolorni mod: RGB. Dimenzije: 240x410 px</p>

                            <button type="submit" class="med-submit">Pošalji sliku</button>

                        </div>

                    </div> <!-- .f-block -->

                </form>

                <div class="c-block">

                    <table class="main-table">
                        <caption class="tab-title">Stavke</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime stavke</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
<?php foreach ($blocks as $block): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/blocks/edit/<?php echo $block->id; ?>"><?php echo $block->name ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/blocks/delete/<?php echo $block->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-block" data-id="<?php echo $block->id; ?>" id="delete_block_action" data-controller="blocks"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/blocks/edit/<?php echo $block->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                </td>
                            </tr>
    
<?php endforeach ?>
                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-del-block">
            <div class="md-content md-content-del">
                <h3>Obriši stavku <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovu stavku?</h4>

                        <form id="delete_cat_form">
                            <div  class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-edit-campaign">
            <div class="md-content md-content-edit">
                <h3>Izmeni kampanju<button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="cb-title">Ime kampanje</h4>

                        <form action="/campaigns/edit_proccess/<?php echo $campaign->id; ?>" method="post">
                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="<?php echo $campaign->name; ?>" data-required="true">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Što kraće to bolje.</p>

                            <h4 class="cb-title">Ime kampanje <i>En</i></h4>
                            <input type="text" name="name_en" class="txtinput" placeholder="Unesite ime" value="<?php echo $campaign->name_en ?>" data-required="true">

                            <div class="form-bottom">
                                <button href="#" class="btn-add-large "><i class="fa fa-pencil"></i> Izmeni</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
