<div class="centercontent">
    

        <?php echo modules::run('template/admin_breadcrumbs'); ?>

        <div class="pageheader notab">
            <h1 class="pagetitle"><?php echo $category->name; ?></h1>
        </div><!--pageheader-->
        
        <div id="contentwrapper" class="contentwrapper">
                
                    <div class="contenttitle2">
                        <h3>Izmeni grupu</h3>
                    </div><!--contenttitle-->
                                        
                    <form class="stdform" action="/categoriesBRW/edit_proccess/<?php echo $category->id;  ?>" method="post" enctype="multipart/form-data">
                        
                        <p>
                            <label>Ime grupe:</label>
                            <span class="field"><input type="text" name="name" class="smallinput" value="<?php echo $category->name; ?>" /></span>
                            <small class="desc">Što kraće to bolje</small>
                        </p>

                        <p>
                            <label>Slika grupe:</label>

                        <?php if($category->image): ?>
                            <span class="field">
                                <a href="/temp/categories/<?php echo $category->image.'?rand(1,3000)'; ?>" class="view thumbimg">
                                    <img src="/temp/categories/<?php echo $category->image.'?rand(1,3000)'; ?>" class="view" alt="" />
                                </a>
                            </span>
                        <?php endif ?>

                            <br>

                            <span class="field">
                                <input type="file" name="userfile" />
                            </span>
                            <small class="desc">RGB boje. Formati: JPEG, GIF, PNG</small>
                        </p>
                                                
                        <p class="stdformbutton">
                            <button class="submit radius2">Pošalji</button>
                        </p>
                        
                        
                    </form>
                                        
                    <div class="contenttitle2">
                        <h3>Nova podgrupa</h3>
                    </div><!--contenttitle-->
                                        
                    <form class="stdform" action="/subcategoriesBRW/create/<?php echo $category->id;  ?>" method="post" enctype="multipart/form-data">
                        
                        <p>
                            <label>Ime nove podgrupe:</label>
                            <span class="field"><input type="text" name="name" class="smallinput" /></span>
                            <small class="desc">Što kraće to bolje</small>
                        </p>

                        <p>
                            <label>Grupa kojoj pripada</label>
                            <span class="field">
                                <?php echo $category->name; ?><br><small>(izabrano)</small>
                            </span>
                        </p>

                        <p>
                            <label>Slika nove podgrupe:</label>
                            <span class="field">
                                <input type="file" name="userfile" />
                            </span>
                            <small class="desc">RGB boje. Formati: JPEG, GIF, PNG</small>
                        </p>
                                                
                        <p class="stdformbutton">
                            <button class="submit radius2">Pošalji</button>
                        </p>
                        
                        
                    </form>
                      
                    <br clear="all" /><br />
                   
        
        </div><!--contentwrapper-->
        
    </div><!-- centercontent -->