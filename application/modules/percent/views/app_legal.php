        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>                    
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                        <ul class="sidebar-nav-groups">
                            <li>
                                <a href="/percent/admin">Posto</a>
                            </li>
                            <li>
                                <a href="/percent/aquestions">Pitanja</a>
                            </li>
                            <li class="sidebar-nav-groups-selected">
                                <a href="/percent/aaplication">Prijava</a>
                            </li>
                            <li>
                                <a href="/percent/arules">Pravila</a>
                            </li>                        
                        </ul>                    
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">POSTO</h2>
                </div>

                <header class="tab-header group">
                    <a href="/percent/aaplication" class="tab-1-2">Fizička lica</a>
                    <a href="/percent/app_legal" class="tab-1-2 tab-active">Pravna lica</a>
                </header>

                <div class="c-block">
<?php if (modules::run("percent/get_app_legal")): ?>


                    <table class="main-table prijava-table prijava-pravna-table">
                        <caption class="tab-title">
                            Lista pravnih lica
                        </caption>
                        <thead>
                            <tr>
                                <th class="th-left">Broj</th>
                                <th class="th-left">Naziv</th>
                                <th class="th-left">Adresa</th>
                                <th class="th-left">Telefon</th>
                                <th class="th-left">E-mail</th>
                                <th class="th-left">PIB</th>
                                <th class="th-left">Djelatnost</th>
                                <th class="th-left">Datum</th>
                            </tr>
                        </thead>
                        <tbody>
<?php foreach (modules::run("percent/get_app_legal") as $legal): ?>
                            <tr>
                                <td class="td-prijava" rowspan="2">
                                    <?php echo $legal->id; ?>
                                </td>
                                <td class="td-prijava">
                                    <b><?php echo $legal->name; ?></b>
                                </td>
                                <td class="td-prijava">
                                    <?php echo $legal->address; ?>
                                </td>
                                <td class="td-prijava">
                                    <?php echo $legal->phone; ?>
                                </td>
                                <td class="td-prijava">
                                    <?php echo $legal->mail; ?>
                                </td>
                                <td class="td-prijava">
                                    <?php echo $legal->pib; ?>
                                </td>
                                <td class="td-prijava">
                                    <?php echo $legal->occupation; ?>
                                </td>
                                <td class="td-prijava">
                                    <?php echo $legal->timestamp; ?>
                                </td>
                            </tr>
                            <tr class="odg-row">
                                <td colspan="7" class="odg-field">
                                    <h3>Odgovorna lica:</h3>
                                    <ol class="odglica">
                                    <?php foreach (modules::run("percent/get_legal_person",$legal->id) as $person): ?>
                                        <li>
                                            <span><?php echo $person->name; ?></span>
                                            <span><?php echo $person->phone; ?></span>
                                            <span><?php echo $person->mail; ?></span>
                                            <span><?php echo $person->LK; ?></span>
                                            <span><?php echo $person->DOB; ?></span>
                                        </li>
                                    <?php endforeach ?>
                                    </ol>
                                </td>
                            </tr>

<?php endforeach ?>
<?php else: ?>
                    <table class="main-table">
                        <caption class="tab-title">
                            Lista lica
                        </caption>
                    </table>

                    <div class="content-blank">

                        <p>Trenutno nema pravnih lica.</p>

                    </div> <!-- .content-blank -->
<?php endif ?>
                        </tbody>
                    </table>

                </div> <!-- .c-block -->

            </div> <!-- .main-content -->

        </section> <!-- .main -->
