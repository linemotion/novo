        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">
                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto"><?=$this->lang->line('Card')?></a></li>
<!--                             <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pitanja"><?=$this->lang->line('Q&A')?></a></li>
 -->                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/posto/prijava"><?=$this->lang->line('Application')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pravila"><?=$this->lang->line('rules')?></a></li>
                        </ul>
                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle">Prijava za Okov loyalty program POSTO</h1>

                        <div class="thanks">
                            <h3>Hvala!</h3>
                            <p>Vaša prijava je poslata, javićemo vam se u najkraćem mogućem roku.</p>
                        </div>

                    </article>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->