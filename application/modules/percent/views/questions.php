        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">
                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto"><?=$this->lang->line('Card')?></a></li>
<!--                             <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/posto/pitanja"><?=$this->lang->line('Q&A')?></a></li>
 -->                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/prijava"><?=$this->lang->line('Application')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pravila"><?=$this->lang->line('rules')?></a></li>
                        </ul>
                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle"><?php echo $questions->$subtitle; ?></h1>

                        <?php echo $questions->$desc; ?>
                    </article>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->