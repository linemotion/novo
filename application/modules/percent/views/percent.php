        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>                    
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                        <ul class="sidebar-nav-groups">
                            <li class="sidebar-nav-groups-selected">
                                <a href="/percent/admin">Posto</a>
                            </li>
                            <li>
                                <a href="/percent/aquestions">Pitanja</a>
                            </li>
                            <li>
                                <a href="/percent/aaplication">Prijava</a>
                            </li>
                            <li>
                                <a href="/percent/arules">Pravila</a>
                            </li>
                        </ul>                    
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">POSTO</h2>
                </div>

                <header class="tab-header group">
                    <a href="/percent/admin" class="tab-1-2 tab-active">Sadržaj</a>
                    <a href="/percent/gallery" class="tab-1-2">Galerija</a>
                </header>

                <form data-parsley-validate action="/percent/edit_proccess/<?php echo $percent->id; ?>" method="post">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Podnaslov</h4>

                            <input type="text" name="subtitle" class="txtinput" value="<?php echo $percent->subtitle; ?>" required>

                        </div>
                       <div class="fi-1-2">

                            <h4 class="cb-title">Podnaslov <i>en</i></h4>

                            <input type="text" name="subtitle_en" class="txtinput" value="<?php echo $percent->subtitle_en; ?>" required>

                        </div>
                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Uvodni tekst </h4>

                            <div class="ckeditor-outer">
                                <textarea value="" id="editor" name="desc" cols="80" rows="5"><?php echo $percent->desc ?></textarea>
                            </div>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Uvodni tekst <i>en</i></h4>

                            <div class="ckeditor-outer">
                                <textarea value="" id="editor2" name="desc_en" cols="80" rows="5"><?php echo $percent->desc_en ?></textarea>
                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->
