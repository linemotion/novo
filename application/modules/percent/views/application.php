        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">
                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto"><?=$this->lang->line('Card')?></a></li>
<!--                             <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pitanja"><?=$this->lang->line('Q&A')?></a></li>
 -->                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/posto/prijava"><?=$this->lang->line('Application')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pravila"><?=$this->lang->line('rules')?></a></li>
                        </ul>
                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle"><?php echo $application->$subtitle; ?></h1>

                        <div class="group">
                            <div class="switch-container">
                                <div class="switch">
                                    <input type="radio" class="switch-input" name="prijava-type" value="fizicka" id="fizicka" checked>
                                    <label for="fizicka" class="switch-label switch-label-on"><?=$this->lang->line('Natural_Persons')?></label>
                                    <input type="radio" class="switch-input" name="prijava-type" value="pravna" id="pravna">
                                    <label for="pravna" class="switch-label switch-label-off"><?=$this->lang->line('Legal_Persons')?></label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tabs">

                            <div class="tab tab-1" id="fizicka-container">

                                <form action="/<?php echo $this->uri->segment(1) ?>/percent/app_physical_proccess/" data-parsley-validate class="contact-form" id="fizicka-form" method="POST">

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Full_Name')?>:</label>
                                        <input name="name" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Full_Address_and_Town')?>:</label>
                                        <input name="address" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Telephone_Number')?>:</label>
                                        <input name="phone" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('E-Mail')?>:</label>
                                        <input name="mail" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Personal_Identity_Number')?>:</label>
                                        <input name="jmbg" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Occupation')?>:</label>
                                        <input name="occupation" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->
                                    
                                    <?php if ($this->uri->segment(1) == "mn"): ?>
                                        <p class="cf-required">Sva polja su obavezna.</p>
                                        <blockquote class="cf-disclaimer">Saglasan/na sam s tim da Okov d.o.o. može čuvati, obrađivati i koristiti moje lične podatke u svrhu izdavanja i korišćenja Posto kartice, kao i za slanje promotivnih obavještenja o pogodnostima.</blockquote>
                                    <?php else: ?>
                                        <p class="cf-required">All fields are required</p>
                                        <blockquote class="cf-disclaimer">I agree that Okov d.o.o. can store, process and use my personal data for the purpose of issuing and using Posto card, as well as for sending promotional materials.</blockquote>
                                    <?php endif ?>

                            
                                    

                                    <button type="submit" class="btn-default btn-big-form">
                                        <?=$this->lang->line('Sendit')?>
                                    </button>

                                </form>

                            </div>

                            <div class="tab tab-2 dn" id="pravna-container">

                                <form action="/<?php echo $this->uri->segment(1) ?>/percent/app_legal_proccess/" data-parsley-validate class="contact-form" id="pravna-form" method="POST">

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Company_Name')?>:</label>
                                        <input name="name_app" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Full_Address_and_Town')?>:</label>
                                        <input name="address_app" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Telephone_Number')?>:</label>
                                        <input name="phone_app" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('E-Mail')?>:</label>
                                        <input name="mail_app" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('VAT_Number')?>:</label>
                                        <input name="pib_app" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="cf-field">
                                        <label><?=$this->lang->line('Main_Business_Activity')?>:</label>
                                        <input name="occupation_app" type="text">
                                        <div class="cf-error dn">
                                            <?=$this->lang->line('Field_Required')?>
                                        </div>
                                    </div> <!-- .cf-field -->

                                    <div class="lice">

                                        <strong><?=$this->lang->line('Authorized_Person')?> <span class="counter">1</span></strong>

                                        <div class="cf-field">
                                            <label><?=$this->lang->line('Full_Name')?>:</label>
                                            <input name="name[]" type="text">
                                            <div class="cf-error dn">
                                                <?=$this->lang->line('Field_Required')?>
                                            </div>
                                        </div> <!-- .cf-field -->

                                        <div class="cf-field">
                                            <label><?=$this->lang->line('Telephone_Number')?>:</label>
                                            <input name="phone[]" type="text">
                                            <div class="cf-error dn">
                                                <?=$this->lang->line('Field_Required')?>
                                            </div>
                                        </div> <!-- .cf-field -->

                                        <div class="cf-field">
                                            <label><?=$this->lang->line('E-Mail')?>:</label>
                                            <input name="mail[]" type="text">
                                            <div class="cf-error dn">
                                                <?=$this->lang->line('Field_Required')?>
                                            </div>
                                        </div> <!-- .cf-field -->

                                        <div class="cf-field">
                                            <label><?=$this->lang->line('Personal_Card_Number')?>:</label>
                                            <input name="LK[]" type="text">
                                            <div class="cf-error dn">
                                                <?=$this->lang->line('Field_Required')?>
                                            </div>
                                        </div> <!-- .cf-field -->

                                        <div class="cf-field">
                                            <label><?=$this->lang->line('DOB')?>:</label>
                                            <input name="DOB[]" type="text">
                                            <div class="cf-error dn">
                                                <?=$this->lang->line('Field_Required')?>
                                            </div>
                                        </div> <!-- .cf-field -->
                                    </div>


                                    <button class="btn-default btn-no-icon" id="dodaj-lice">
                                        + <?=$this->lang->line('Add_Authorized_Person')?>
                                    </button>

                                    <?php if ($this->uri->segment(1) == "mn"): ?>
                                        <p class="cf-required">Sva polja su obavezna.</p>
                                        <blockquote class="cf-disclaimer">Saglasni smo s tim da Okov d.o.o. može čuvati, obrađivati i koristiti podatke o našem pravnom licu i naše lične podatke, kao ovlašćenih lica, u svrhu izdavanja i korišćenja Posto kartice, kao i za slanje promotivnih obavještenja o pogodnostima.</blockquote>
                                    <?php else: ?>
                                        <p class="cf-required">All fields are required</p>
                                        <blockquote class="cf-disclaimer">We agree that Okov d.o.o. can store, process and use our company's data and our own personal data, as authorized persons, for the purpose of issuing and using Posto card, as well as for sending promotional materials</blockquote>
                                    <?php endif ?>

                                    <button type="submit" class="btn-default btn-big-form">
                                        <?=$this->lang->line('Sendit')?>
                                    </button>

                                </form>

                            </div>

                        </div>

                    </article>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->