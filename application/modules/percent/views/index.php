        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <nav class="sidebar-nav">
                        <ul class="sidebar-nav-main">
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1) ?>/posto"><?=$this->lang->line('Card')?></a></li>
<!--                             <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pitanja"><?=$this->lang->line('Q&A')?></a></li>
 -->                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/prijava"><?=$this->lang->line('Application')?></a></li>
                            <li><a href="/<?php echo $this->uri->segment(1) ?>/posto/pravila"><?=$this->lang->line('rules')?></a></li>
                        </ul>
                    </nav> <!-- .sidebar-nav -->

                <?php echo modules::run("newsletter/sidebar") ?>

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <article class="site-article">

                        <h1 class="section-subtitle"><?php echo $percent->$subtitle; ?></h1>

                        <?php echo $percent->$desc; ?>
                    </article>

<?php if ($gallery): ?>
                    <div class="page-gallery">
                        <h3 class="page-subtitle"><span>Galerija</span></h3>
                        <div class="pg-container">
                            <ul class="pg-items group">
<?php foreach ($gallery as $image): ?>
                                <li class="pg-item">
                                    <a href="/img/percent/<?php echo $image->image ?>" class="fancybox" rel="gallery">
                                        <img src="/img/percent/<?php echo $image->image ?>" alt="">
                                    </a>
                                </li>    
<?php endforeach ?>

                            </ul> <!-- .pg-items -->
                        </div> <!-- .pg-container -->
                    </div> <!-- .page-gallery -->    
<?php endif ?>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->