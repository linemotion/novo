<?php
class Mdl_percent extends CI_Model 
{

    var $table; 

    function __construct() 
    {
        parent::__construct();
        $this->table = "percent";
    }


    function get_by_url($url)
    {
        $this->db->where('active',1);
        $this->db->where('url', $url);
        $query=$this->db->get($this->table);

        return $query->row();
    }

    function get($order_by = "position")
    {
        $this->db->order_by($order_by,"desc");
        $query=$this->db->get($this->table);
        return $query->result();
    }
    function get_gallery()
    {
        $query=$this->db->get("percent_gallery");
        return $query->result();
    }
    function get_app_physical()
    {
        $this->db->order_by("id","desc");
        $query=$this->db->get("app_physical");
        return $query->result();
    }
    function get_app_legal()
    {
        $this->db->order_by("id","desc");
        $query=$this->db->get("app_legal");
        return $query->result();
    }
    function get_legal_person($id)
    {
        $this->db->where('app_id', $id);
        $query=$this->db->get("app_person");
        return $query->result();
    }
    function get_with_limit($limit, $offset, $order_by) {
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by);
    $query=$this->db->get($this->table);
    return $query;
    }

    function get_where($id){
    $this->db->where('id', $id);
    $query=$this->db->get($this->table);
    return $query->row();
    }

    function get_where_custom($col, $value) {
        $this->db->order_by('id', "desc");
        //$this->db->where('active',1);
    $this->db->where($col, $value);
    $query=$this->db->get($this->table);
    return $query->result();
    }

    function _insert($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
    }

    function _update($id, $data){
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
    }

    function _delete($id){
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    }

    function count_where($column, $value) {
    $this->db->where($column, $value);
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function count_all() {
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function get_max($field = "id") 
    {
        $this->db->select_max($field);
        $query = $this->db->get($this->table);
        $row=$query->row();
        return $query->row()->id;
    }

    function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
    }


}

