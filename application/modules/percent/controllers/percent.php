<?php

class Percent extends MX_Controller
{
	var $module;


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_percent');

		$this->module = "percent";
	}
	function index()
	{
	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");


		$data["title"] = $this->lang->line("percent");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("percent"),"link" => "/".$this->uri->segment(1)."/posto"),
			array("name" => $this->lang->line("Card"),"link" => "/")
			); 

		$data['percent'] = array_shift($this->get_where_custom("name","percent"));
		$data['gallery'] = modules::run("percent/get_gallery");

		echo modules::run('template/render',$this->module,"index",$data);
	}
	function questions()
	{

	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");
		$data["position"] = ($this->uri->segment(1) == "en" ? "position_en" : "position");

		$data["title"] = $this->lang->line("Q&A");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("percent"),"link" => "/".$this->uri->segment(1)."/posto"),
			array("name" => $this->lang->line("Q&A"),"link" => "/")
			); 

 		$data['questions'] = array_shift($this->get_where_custom("name","questions"));



		echo modules::run('template/render',$this->module,"questions",$data);
	}
	function rules()
	{

	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");
		$data["position"] = ($this->uri->segment(1) == "en" ? "position_en" : "position");

		$data["title"] = $this->lang->line("Rules");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("percent"),"link" => "/".$this->uri->segment(1)."/posto"),
			array("name" => $this->lang->line("rules"),"link" => "/")
			); 

 		$data['rules'] = array_shift($this->get_where_custom("name","rules"));



		echo modules::run('template/render',$this->module,"rules",$data);
	}

	function application()
	{

	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["desc"] = ($this->uri->segment(1) == "en" ? "desc_en" : "desc");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");

		$data["title"] = $this->lang->line("Application");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("percent"),"link" => "/".$this->uri->segment(1)."/posto"),
			array("name" => $this->lang->line("Application"),"link" => "/")
			); 

		$data['application'] = array_shift($this->get_where_custom("name","application"));

		echo modules::run('template/render',$this->module,"application",$data);
	}
	function app_physical_proccess()
	{

		$data['name'] = $this->input->post('name');
		$data['address'] = $this->input->post('address');
		$data['phone'] = $this->input->post('phone');
		$data['mail'] = $this->input->post('mail');
		$data['jmbg'] = $this->input->post('jmbg');
		$data['occupation'] = $this->input->post('occupation');
		$data['timestamp'] = date('Y-m-d',time());

		$this->db->insert("app_physical",$data);

		// sending mail
			$config = array(
                   'protocol'  => 'smtp',
                   'smtp_host' => 'cp1.ulimitserver.com',
                   'smtp_port' => 25,
                   'smtp_user' => 'robot@okov.me',
                   'smtp_pass' => 'r0b07123',
                   'smtp_timeout' => 10,
                   'charset'  => 'utf-8',
                   'priority' => '1',
                );

		    $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");

		    $this->email->from($this->input->post('email'),'Okov');
		    //$this->email->reply_to('rankovicmarko@yahoo.com',"bati");
		    $this->email->to('posto@okov.me');
		    $this->email->subject('kontakt');
		    $this->email->message(
				'Ime i Prezime: '.$data['name']."\r\n".
				'Adresa: '.$data['address']."\r\n".
				'Telefon: '.$data['phone']."\r\n".
				'E-mail: '.$data['mail']."\r\n".
				'JMBG: '.$data['jmbg']."\r\n".
				'Zanimanje: '.$data['occupation']."\r\n".
				'Datum prijave: '.$data['timestamp']."\r\n"
				);

		    if($this->email->send())
		        redirect("/".$this->uri->segment(1)."/posto/hvala");
		    else
				show_error($this->email->print_debugger());

		
	}
	function app_legal_proccess()
	{
		$data['name'] = $this->input->post('name_app');
		$data['address'] = $this->input->post('address_app');
		$data['phone'] = $this->input->post('phone_app');
		$data['mail'] = $this->input->post('mail_app');
		$data['pib'] = $this->input->post('pib_app');
		$data['occupation'] = $this->input->post('occupation_app');
		$data['timestamp'] = date('Y-m-d',time());
		
		$this->db->insert("app_legal",$data);
		$app_id = $this->db->insert_id();
		

		$person['app_id'] = $app_id;

		$prep = array();

		for ($i=0; $i < count($this->input->post("name")); $i++) 
		{ 

			$person['name'] = $_POST["name"][$i];
			$person['phone'] = $_POST["phone"][$i];
			$person['mail'] = $_POST["mail"][$i];
			$person['LK'] = $_POST["LK"][$i];
			$person['DOB'] = $_POST["DOB"][$i];

			$prep[] = $person['name'].",".$person['phone'].",".$person['mail'].",".$person['LK'].",".$person['DOB'];

			$this->db->insert("app_person",$person);	
		}

		// sending mail
			$config = array(
                   'protocol'  => 'smtp',
                   'smtp_host' => 'cp1.ulimitserver.com',
                   'smtp_port' => 25,
                   'smtp_user' => 'robot@okov.me',
                   'smtp_pass' => 'r0b07123',
                   'smtp_timeout' => 10,
                   'charset'  => 'utf-8',
                   'priority' => '1',
                );

		    $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");

		    $this->email->from($this->input->post('email'),'Okov');
		    //$this->email->reply_to('rankovicmarko@yahoo.com',"bati");
		    $this->email->to('posto@okov.me');
		    $this->email->subject('kontakt');
		    $this->email->message(
				'Ime i Prezime: '.$data['name']."\r\n".
				'Adresa: '.$data['address']."\r\n".
				'Telefon: '.$data['phone']."\r\n".
				'E-mail: '.$data['mail']."\r\n".
				'PIB: '.$data['pib']."\r\n".
				'Zanimanje: '.$data['occupation']."\r\n".
				'Datum prijave: '.$data['timestamp']."\r\n".
				'Ovlascena lica: <pre>'.implode("\r\n", $prep)."</pre>\r\n"
				);

		    if($this->email->send())
		        redirect("/".$this->uri->segment(1)."/posto/hvala");
		    else
				show_error($this->email->print_debugger());

	}
	function thanks()
	{
	    $this->lang->load('index',$this->uri->segment(1));
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["subtitle"] = ($this->uri->segment(1) == "en" ? "subtitle_en" : "subtitle");
		$data["position"] = ($this->uri->segment(1) == "en" ? "position_en" : "position");

		$data["title"] = $this->lang->line("Application");
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("percent"),"link" => "/".$this->uri->segment(1)."/posto"),
			array("name" => $this->lang->line("Application"),"link" => "/")
			); 

		echo modules::run('template/render',$this->module,"thanks",$data);
	}
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['percent'] = array_shift($this->get_where_custom("name","percent"));

		echo modules::run('template/admin_render',$this->module,"percent",$data);
	}
	function aquestions()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['questions'] = array_shift($this->get_where_custom("name","questions"));

		echo modules::run('template/admin_render',$this->module,"aquestions",$data);
	}
	function arules()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['rules'] = array_shift($this->get_where_custom("name","rules"));

		echo modules::run('template/admin_render',$this->module,"arules",$data);
	}
	function aaplication()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$data['aplication'] = array_shift($this->get_where_custom("name","aplication"));

		echo modules::run('template/admin_render',$this->module,"aaplication");
	}
	function app_legal()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$data['aplication'] = array_shift($this->get_where_custom("name","aplication"));

		echo modules::run('template/admin_render',$this->module,"app_legal");
	}

	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data['gallery'] = $this->get_gallery();

		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}
	function profile()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$data["title"] = $this->lang->line("profile");
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => "Kampanje","link" => "")
			); 

		$data['campaigns'] = $this->get("id");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}


	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

	    $this->db->where('id', $this->uri->segment(3));
	    $this->db->delete("percent_gallery");

		redirect('/percent/gallery');
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$block = $this->get_by_id($this->uri->segment(3));
		$this->_delete($block->id);

		redirect('/campaigns/edit/'.$block->cam_id);
	}		

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		// 	$data['breadcrumbs'] = array(
		// array("name" => "Proizvodi","link" => "/admin/products"),
		// array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		// array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		// array("name" => $data["product"]->name,"link" => "")
		// );

		$data["block"] = modules::run("blocks/get_by_id",$id);
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		// 	$data['breadcrumbs'] = array(
		// array("name" => "Proizvodi","link" => "/admin/products"),
		// array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		// array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		// array("name" => $data["product"]->name,"link" => "")
		// );

		//$data["campaign"] = modules::run("campaigns/get_by_id",$id);
		$data["block"] = modules::run("campaign/get_by_id",$id);
		echo modules::run('template/admin_render',$this->module,"create",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		$section = $this->get_by_id($id);

		$data['subtitle'] = $this->input->post('subtitle');
		$data['subtitle_en'] = $this->input->post('subtitle_en');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');
		
		$this->_update($id,$data);

		if($id == 1)
			redirect('/percent/admin/');
		else
			redirect('/percent/a'.$section->name);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
				
		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',"percent");

    	$this->db->insert("percent_gallery", $data);

		redirect('/percent/gallery/');
	}

	function get($order_by)
	{
		$products = $this->mdl_percent->get($order_by);
		return $products;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$products = $this->mdl_percent->get_with_limit($limit, $offset, $order_by);
		return $products;
	}

	function get_by_id($id)
	{
		$products = $this->mdl_percent->get_where($id);
		return $products;
	}
	function get_app_physical()
	{
		$products = $this->mdl_percent->get_app_physical();
		return $products;
	}
	function get_app_legal()
	{
		$products = $this->mdl_percent->get_app_legal();
		return $products;
	}
	function get_legal_person($id)
	{
		$products = $this->mdl_percent->get_legal_person($id);
		return $products;
	}
	function get_gallery()
	{
		$products = $this->mdl_percent->get_gallery();
		return $products;
	}

	function get_where_custom($col, $value) 
	{
		$products = $this->mdl_percent->get_where_custom($col, $value);
		return $products;
	}

	function _insert($data)
	{
		return $this->mdl_percent->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_percent->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_percent->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_percent->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_percent->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query) 
	{
		$products = $this->mdl_percent->_custom_query($mysql_query);
		return $products;
	}

}