<?php

class Resize extends MX_Controller
{
	var $upload_path;
	var $image_path;

    function __construct()
    {
		parent::__construct();
		$this->load->library('image_moo');

		$this->upload_path = FCPATH.'temp'.DIRECTORY_SEPARATOR;
		$this->image_path = FCPATH.'images'.DIRECTORY_SEPARATOR;
    }

    public function upload($module, $name = "userfile")
    {   
    	$this->load->helper('string');

		$this->image_moo->load($_FILES[$name]['tmp_name'])
						->save($this->upload_path.$_FILES[$name]['name']);
    	$this->image_moo->clear();
    	
    	$image_size = @getimagesize($_FILES[$name]['tmp_name']);

    	$image_name = $_FILES[$name]['name'];

    	if(file_exists($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name))
    		$image_name = random_string('alnum', 4)."-".$image_name;

    	if ($module == "actions") 
    	{
    		if (!modules::run("action_gallery/get", "id"))
    		{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('105','145')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumb/".$image_name);    			
    		}

			if($image_size[0] > 600)
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('600','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
			else
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize($image_size[0],$image_size[1])
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
    	}
    	else if ($module == "products") 
    	{
			if($image_size[0] > $image_size[1])
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('220','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumb/".$image_name);
			}
			else
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('9999','190')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumb/".$image_name);
			}

			if($image_size[0] > 600)
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('600','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
			else
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize($image_size[0],$image_size[1])
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
    	} 
    	else if ($module == "news" OR $module == "profile") 
    	{
			if($image_size[0] > $image_size[1])
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize_crop('9999','185')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumb/".$image_name);
			}
			else
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize_crop('270','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/thumb/".$image_name);
			}
			
			if($image_size[0] > 600)
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('600','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
			else
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize($image_size[0],$image_size[1])
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
    	}
    	else
    	{
			if($image_size[0] > 600)
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize('600','9999')
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}
			else
			{
				$this->image_moo->load($_FILES[$name]['tmp_name'])
										->resize($image_size[0],$image_size[1])
										->save($_SERVER['DOCUMENT_ROOT']."/img/".$module."/".$image_name);
			}    		
    	}


		return $image_name;
	}
    public function delete($module, $file_name)
    {   	
    	if (file_exists($this->upload_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$file_name))
    		unlink($this->upload_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$file_name);
    	
    	if (file_exists($this->image_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$file_name))
    		unlink($this->image_path.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.$file_name);

	}
 //    public function pdf($module, $name = "userfile")
 //    {   
 //    	$this->load->helper('string');

	// 	$this->image_moo->load($_FILES['userfile']['tmp_name'])
	// 					->save($_SERVER['DOCUMENT_ROOT']."/pdf/akcija.pdf",true);
 //    	$this->image_moo->clear();
    	
 //    	//echo "<pre>";
 //    	// die($_SERVER['DOCUMENT_ROOT']."/pdf/".$_FILES['userfile']['name']);

 //    	$pdf = $_FILES['userfile']['name'];

	// 	return $pdf;
	// }
	function do_upload()
		{
			// load codeigniter helpers
			$this->load->helper(array('form','url'));
			// set path to store uploaded files
			$config['upload_path'] = $_SERVER['DOCUMENT_ROOT']."/pdf/";
			// set allowed file types
			$config['allowed_types'] = 'pdf';
			// set upload limit, set 0 for no limit
			$config['max_size']	= 0;
			$config['overwrite']	= true;
			$config['file_name']	= "akcija.pdf";
	 
			// load upload library with custom config settings
			$this->load->library('upload', $config);
	 
			 // if upload failed , display errors
			if (!$this->upload->do_upload())
			{
				$result = $this->upload->data();
				return $result["file_name"];
			}
			else
			{
				$result = $this->upload->data();
				return $result["file_name"];
			}
		}
		function do_upload_brands($filename)
		{

			// load codeigniter helpers
			$this->load->helper(array('form','url'));

			$this->image_moo->load($_FILES[$filename]['tmp_name'])
							->resize(9999,9999)
							->save($_SERVER['DOCUMENT_ROOT']."/assets/img/".$filename.".png",true);
	    	$this->image_moo->clear();			

	    	// set path to store uploaded files
			// $config['upload_path'] = $_SERVER['DOCUMENT_ROOT']."/assets/img/";
			// // set allowed file types
			// $config['allowed_types'] = 'png';
			// // set upload limit, set 0 for no limit
			// $config['max_size']	= 0;
			// $config['overwrite']	= true;
			// $config['file_name']	= $filename.".png";
	 
			// // load upload library with custom config settings
			// $this->load->library('upload', $config);
	 
			//  // if upload failed , display errors
			// if (!$this->upload->do_upload())
			// {
			// 	$result = $this->upload->data();
			// 	return $result["file_name"];
			// }
			// else
			// {
			// 	$result = $this->upload->data();
			// 	return $result["file_name"];
			// }
		}
}