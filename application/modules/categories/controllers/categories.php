<?php

class Categories extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_categories');

		$this->module = "categories";

	}
	function filter_sales()
	{
		$cat_ids = array();

	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi")
			); 	
		
		$data["title"] = "Proizvodi na rasprodaji";
				
		$data['categories'] = modules::run("categories/get","position");
		//$data['products'] = modules::run("products/get_by_action");
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");

		if ($this->uri->segment(3)) 
			$data["products"] = modules::run("products/get_by_sales_filter",$this->uri->segment(3));
		else
			$data["products"] = modules::run("products/get_by_sales_filter",0);

		

		foreach (modules::run("products/get_by_sales_filter",0) as $pro) 
			$cat_ids[] = $pro->cat_id;
		
		$cat_ids = array_unique($cat_ids);

		foreach ($cat_ids as $catid) 
			$data["fil_groups"][] =  modules::run("categories/get_by_id",$cat_id);;


		echo modules::run('template/render',$this->module,"filter_sales",$data);
	}


	function filter()
	{
		$cat_ids = array();

	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi")
			); 	
		
		$data["title"] = "Proizvodi na akciji i popustu";
				
		$data['categories'] = modules::run("categories/get","position");
		//$data['products'] = modules::run("products/get_by_action");
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");

		if ($this->uri->segment(3)) 
			$data["products"] = modules::run("products/get_by_action_dis",$this->uri->segment(3));
		else
			$data["products"] = modules::run("products/get_by_action_dis",0);

		

		foreach (modules::run("products/get_by_action_dis",0) as $pro) 
			$cat_ids[] = $pro->cat_id;
		
		$cat_ids = array_unique($cat_ids);

		foreach ($cat_ids as $catid) 
			$data["fil_groups"][] = $this->get_by_id($catid);


		echo modules::run('template/render',$this->module,"filter",$data);
	}
	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$values = $this->input->post('values');

		for ($i=0; $i <= count($values); $i++) 
		{ 
			$data['position'] = $i + 1;
			$this->_update($values[$i],$data);
		}
	}
	
	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);
		$data['category'] = modules::run("categories/get_by_id",$id);
		$data['categories'] = modules::run("categories/get","position");

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => $data['category']->name,"link" => "")
			); 

		$data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	// function url()
	// {
	// 	if(!$this->session->userdata('logged_in') == true)
	// 		redirect('prijava');

	// 	$categories = modules::run("categories/get","position");

	// 	foreach ($categories as $category) 
	// 	{
	// 		$data['url'] = url_title(rs_char($category->name));
	// 		$data['image'] = url_title(rs_char($category->name)).".jpg";

	// 		$this->_update($category->id,$data);
	// 	}

	// 	die('done');
	// 	//echo modules::run('template/render',$this->module,"index",$data);
	// }
	
	function index()
	{
		$cat_ids = array();
		// $fil_groups = array();

	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi")
			); 	
		
		$data["title"] = "Proizvodi na akciji i popustu";
				
		$data['categories'] = modules::run("categories/get","position");
		//$data['products'] = modules::run("products/get_by_action");
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		$data["products"] = modules::run("products/get_where_paginateFrontAll",12,$this->uri->segment(3));

		foreach (modules::run("products/get_by_action_dis",0) as $pro) 
			$cat_ids[] = $pro->cat_id;
		
		$cat_ids = array_unique($cat_ids);

		foreach ($cat_ids as $catid) 
			$data["fil_groups"][] = $this->get_by_id($catid);
			
		echo modules::run('template/render',$this->module,"index",$data);
	}

	function review()
	{
		$url = $this->uri->segment(3);
		$data['category'] = modules::run("categories/get_by_url",$url);
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
	    $this->lang->load('index',$this->uri->segment(1));

		$data["title"] = $data['category']->$data["name"];

		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => $data['category']->$data["name"],"link" => "/")
			); 

		$data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);
		$data['products'] = modules::run("products/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['search'] = $this->input->post('search');
		$data['position'] = $this->get_max_pos()->position + 1;

		// if($_FILES['userfile']['error'] == 0)
		// 	$data['image'] = modules::run('resize/upload',$this->module);

		$this->_insert($data);

		redirect('/admin/products');
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(3));

		redirect('/admin/products/');
	}
	// function sort()
	// {
	// 	if(!$this->session->userdata('logged_in') == true)
	// 		redirect('prijava');
		
	// 	$sorted = $this->input->post("data");

	// 	foreach ($sorted as $key => $value)
	// 	{
	// 		$data['position'] = $value;

	// 		$this->db->where("id",$key);
	// 		$this->db->update("categories",$data);	
	// 	}
		
	// 	//$category = $this->get_by_id($id);

	// 	// echo "success"; 
	// }
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		$data['categories'] = modules::run("categories/get","position");

		$data['category'] = $this->get_by_id($id);

		$data['breadcrumbs'] = array(
					array("name" => "Proizvodi","link" => "/admin/products"),
					array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id)
		);
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['name_en'] = $this->input->post('name_en');
		$data['search'] = $this->input->post('search');
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$category->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/categories/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_categories->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_categories->get_by_url($url);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_categories->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_categories->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_categories->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_categories->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_categories->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_categories->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_categories->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_categories->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_categories->get_max();
		return $max_id;
	}

	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_categories->_custom_categories($mysql_categories);
		return $categories;
	}
	function get_by_search($search) 
	{
		$categories = $this->mdl_categories->get_by_search($search);		
		return $categories;
	}

}