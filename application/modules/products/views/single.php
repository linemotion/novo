    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=554978697864319&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar matchheight inside-sidebar-move">

                    <nav class="sidebar-nav sidebar-nav-mob">

                        <ul class="sidebar-nav-main">
                            <li class="snm-selected">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($product->cat_name)); ?>"><?php echo $category->$name; ?></a>
                                <ul class="sidebar-subnav">
    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$product->cat_id) as $item): ?>
                                <?php if ($item->id == $subcategory->id): ?>
                                    <li class="ssn-selected">
                                <?php else: ?>
                                    <li>
                                <?php endif ?>
                                        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($product->cat_name));?>/<?php echo $item->url;?>"><?php echo $item->$name?></a>  </li>
    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <div class="sidebar-responsive group">
                        <form action="">
                            <select name="choose-cat" id="choose-cat" class="select-url">
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi">
                                    <?=$this->lang->line('on_sale')?>
                                </option>
                                <?php foreach (modules::run("categories/get","position") as $cat): ?>
                                <?php if ($cat->id == $category->id): ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $cat->url;?>" selected>
                                    <?php echo $cat->$name?>
                                </option>
                                <?php else: ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $cat->url;?>">
                                    <?php echo $cat->$name?>
                                </option>
                                <?php endif ?>
                                <?php endforeach ?> 
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja ">
                                    <?=$this->lang->line('sales')?></a>
                                </option>
                            </select>
                        </form>

                        <form action="" class="resf-secondary">
                            <select name="choose-subcat" id="choose-subcat" class="select-url">
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>">
                                    Sve podgrupe
                                </option>
                                <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $scat): ?>
                                <?php if ($scat->id == $subcategory->id): ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $scat->url;?>" selected>
                                    <?php echo $scat->$name?>
                                </option>
                                <?php else: ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $scat->url;?>">
                                    <?php echo $scat->$name?>
                                </option>
                                <?php endif ?>
                                <?php endforeach ?> 
                            </select>
                        </form>
                    </div>

                    <div class="sidebar-back">
                        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi " class="btn-default btn-default-back"><i class="icon icon-arrow-left"></i>
                            <?php if ($this->uri->segment(1) == "en"): ?>
                            All groups
                            <?php else: ?>
                                Sve grupe
                            <?php endif ?>  
                        </a>
                    </div> <!-- .sidebar-back -->

                    <div class="sidebar-window sidebar-window-contact group">
                        <h3 class="sw-title">Više</h3>
                        <p class="sw-text">Za više informacija o proizvodima kontaktirajte nas na telefon 020 658 501</p>
                        <a href="#" class="link-default sw-link">Kontakt <i class="icon icon-arrow-right"></i></a>
                    </div> <!-- .sidebar-window -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="product-page group">

                        <div class="group">

                            <div class="product-img">
                                <a href="/img/products/<?php echo $product->image; ?>" class="open-lightbox" rel="gallery"><img src="/img/products/<?php echo $product->image; ?>">
                                    <b><i class="icon icon-zoom-in"></i></b>
                                </a>
<?php if ($gallery): ?>
                                <ul class="product-img-gallery">
<?php foreach ($gallery as $image): ?>
                                    <li>
                                        <a href="/img/products/<?php echo $image->image; ?>" class="open-lightbox" rel="gallery">
                                            <img src="/img/products/<?php echo $image->image; ?>">
                                            <b><i class="icon icon-zoom-in"></i></b>
                                        </a>
                                    </li>
<?php endforeach ?>
                                </ul>  
<?php endif ?>

                            </div> <!-- .product-img -->

                            <div class="product-desc">

                                <div class="fb-like" data-href="<?php echo current_url() ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                                <span class="pd-kind"><?php echo modules::run('sorts/get_by_id',$product->sort_id)->name ; ?></span>

                                <h2 class="pd-name">
                                    <span class="pd-manuf"><?php echo modules::run('manufacturers/get_by_id',$product->manuf_id)->name ; ?></span>
                                    <?php echo $product->name; ?>
                                </h2>
<?php if (!$product->show_price): ?>
<?php elseif($product->discount): ?>
                                <div class="pd-price-container">
                                    <div class="special-tag">
                                        <?=$this->lang->line('pro_discount')?>
                                    </div>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $product->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($product->discount_value); ?>%</b>
                                         </span>
                                    </div>
<?php if ($product->discount_end): ?>
                                    <p class="timeleft">Važi do: <?php echo $product->discount_end; ?></p>
<?php else: ?>   
                                    <p class="timeleft">Važi do isteka zaliha.</p>                            
<?php endif ?>
                                </div>
<?php elseif($product->action): ?>

                                <div class="pd-price-container">
                                    <div class="special-tag">
                                        <?=$this->lang->line('pro_action')?>
                                    </div>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $product->price; ?> €</i> <em>-<?php echo floor($product->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $product->action_price; ?> €</b>
                                         </span>
                                    </div>
<?php if ($product->action_end): ?>
                                    <p class="timeleft">Važi do: <?php echo $product->action_end; ?></p>
<?php else: ?>   
                                    <p class="timeleft">Važi do isteka zaliha.</p>                            
<?php endif ?>

                                </div>
<?php elseif($product->sales): ?>
                                <div class="pd-price-container">
                                    <div class="special-tag">
                                        <?=$this->lang->line('pro_sale')?>
                                    </div>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $product->price; ?> €</i> <em>-<?php echo floor($product->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $product->sales_price; ?> €</b>
                                         </span>
                                    </div>
<?php if ($product->sales_end): ?>
                                    <p class="timeleft">Važi do: <?php echo $product->sales_end; ?></p>
<?php else: ?>   
                                    <p class="timeleft">Važi do isteka zaliha.</p>                            
<?php endif ?>

                                </div>     

<?php elseif($product->percent): ?>
                                <div class="pd-price-container">
                                    <div class="special-tag">
                                        <span>POPUST</span>
                                    </div>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $product->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($product->percent_discount); ?>%</b>
                                         </span>
                                    </div>
<?php if ($product->percent_end): ?>
                                    <p class="timeleft">Važi do: <?php echo $product->percent_end; ?></p>
<?php else: ?>   
                                    <p class="timeleft">Važi do isteka zaliha.</p>                            
<?php endif ?>
                                    <div class="pd-posto-msg">
                                        Popust važi samo za korisnike Posto kartica.
                                    </div>
                                </div>  

<?php else: ?>
                                <div class="pd-price-container">
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $product->price; ?> €</b>
                                         </span>
                                    </div>
                                </div>

<?php endif ?>

                                <div class="pd-content">
                                    <?php echo $product->desc; ?>
                                </div>

                            </div> <!-- .product-desc -->

                        </div> <!-- .group -->

                        <h3 class="page-subtitle"><span>Specifikacija</span></h3>

                        <table class="specification">
<?php foreach ($specs as $spec): ?>
                            <tr>
<?php if (isset(modules::run("products/get_specs_front",$spec->id)->value) and modules::run("products/get_specs_front",$spec->id)->value!=""): ?>
                                <td class="spec-name"><?php echo $spec->name; ?></td>
                                <td><?php echo modules::run("products/get_specs_front",$spec->id)->value ?></td>    
<?php endif ?>
                            </tr>
<?php endforeach ?>
                        </table>

                    </div> <!-- .product-page -->

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
