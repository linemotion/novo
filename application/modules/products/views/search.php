        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar">

                    <div class="sidebar-window sidebar-window-newsletter group">
                        <h3 class="sw-title">Newsletter</h3>
                        <p class="sw-text">Ukoliko želite da dobijate redovne informacije o popustima i akcijama, molimo vas da unesete vašu e-mail adresu.</p>
                        <div class="sidebar-newsletter group">
                            <div class="sn-input">
                                <input type="text" placeholder="Vaš e-mail">
                            </div>
                            <button type="submit" class="btn-arrow"><i class="icon icon-arrow-right"></i></button>
                        </div>
                    </div> <!-- .sidebar-window -->

                    <div class="sidebar-window sidebar-window-contact group">
                        <h3 class="sw-title">Više</h3>
                        <p class="sw-text">Za više informacija o proizvodima kontaktirajte nas na telefon 020 658 501</p>
                        <a href="/<?php echo $this->uri->segment(1) ?>/kontakt" class="link-default sw-link">Kontakt <i class="icon icon-arrow-right"></i></a>
                    </div> <!-- .sidebar-window -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <h3 class="section-subtitle">Grupe proizvoda</h3>
                    <div class="search-groups">
                        <ul>
<?php foreach ($cat_search as $cat): ?>
                             <li><a href="/<?php echo $this->uri->segment(1) ?>/proizvodi/<?php echo $cat->url ?>"><?php echo $cat->name ?></a></li>   
<?php endforeach ?>
<?php foreach ($subcat_search as $subcat): ?>
                             <li><a href="/<?php echo $this->uri->segment(1) ?>/proizvodi/<?php echo url_title(rs_char($subcat->cat_name)) ?>/<?php echo $subcat->url ?>"><?php echo $subcat->name ?></a></li>   
<?php endforeach ?>
<?php foreach ($sorts_search as $sort): ?>
                             <li><a href="/<?php echo $this->uri->segment(1) ?>/proizvodi/<?php echo url_title(rs_char($sort->cat_name)) ?>/<?php echo url_title(rs_char($sort->subcat_name)) ?>/<?php echo $sort->url."-".$sort->id ?>/p/0"><?php echo $sort->name ?></a></li>   
<?php endforeach ?>
                        </ul>
                    </div> <!-- .search-groups -->

                    <h3 class="section-subtitle">Proizvodi</h3>
                    <div class="inside-product-items group">

<?php if ($products): ?>
<?php for($i=0;$i < count($products) ;$i++): ?>                       

<?php if (!$products[$i]->show_price): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name pi-name-noprice">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 
<?php elseif($products[$i]->discount): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_discount')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->discount_value); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 

<?php elseif($products[$i]->action): ?>

                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_action')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->action_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->
<?php elseif($products[$i]->sales): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_sale')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->sales_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->

<?php elseif($products[$i]->percent): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag-posto">
                                    <span>POSTO</span>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->percent_discount); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->



<?php else: ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $products[$i]->price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->    

<?php endif ?>


<?php endfor ?>
<?php else: ?>
    <p class="no-results">Nema rezultata.</p>
<?php endif ?>     
                    </div> <!-- .group -->

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->