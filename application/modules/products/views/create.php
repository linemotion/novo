

        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="#"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
    <?php if ($item->id == $subcategory->cat_id): ?>

                            <li class="sidebar-nav-groups-selected">
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name ?></a>
                                <ul class="sidebar-nav-subgroups">

        <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$subcategory->cat_id) as $subcat): ?>
            <?php if ($subcategory->id == $subcat->id): ?>
                                      <li class="sidebar-nav-subgroups-selected"><a href="/subcategories/edit/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>
               <?php else: ?>
                                     <li><a href="/subcategories/edit/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>

            <?php endif ?>
        <?php endforeach ?>
                                </ul>
                            </li>
    <?php else: ?>
                            <li>
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
    <?php endif ?>

<?php endforeach ?>
                        </ul>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Dodaj novi proizvod</h2>
                </div>

                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>


                <form data-parsley-validate method="Post" action="/products/create_proccess/<?php echo $subcategory->id; ?>" enctype="multipart/form-data">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime proizvoda</h4>

                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Vrsta</h4>

                               <select class="chosen-select" name="sort" id="sort" required>
                                    <option value="">Izaberite vrstu</option>
<?php foreach (modules::run("sorts/get_where_custom","subcat_id",$subcategory->id) as $sort): ?>
                                    <option value="<?php echo $sort->id; ?>"><?php echo $sort->name; ?></option>
<?php endforeach ?>
                                </select>


                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Proizvođač</h4>

                            <select class="chosen-select" name="manufacturers" required>
<?php foreach (modules::run("manufacturers/get","id") as $manuf): ?>
                                    <option value="<?php echo $manuf->id; ?>"><?php echo $manuf->name; ?></option>
<?php endforeach ?>
                            </select>

                        </div>

                    </div> <!-- .f-block -->


                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Šifra</h4>

                            <input type="text" name="code" class="txtinput" placeholder="Unesite šifru" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Cena</h4>
                            <label class="show-check"><input name="show_price" type="checkbox">Prikaži cenu na sajtu</label>

                            <input class="txtinput db-price" name="price" value="" disabled>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title"><label><input name="discount" type="checkbox" class="popust-control"> Popust</label></h4>

                        <div class="popust-window dn group">

                            <div class="fi-1-2">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="new_price" class="txtinput new-price" placeholder="Unesite novu cenu">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-2">

                                <h4 class="cb-title">Iznos popusta</h4>

                                <input type="text" name="discount_value" class="txtinput discount" placeholder="Unesite popust">

                                <span class="form-sign">%</span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title"><label><input name="action" type="checkbox" class="action-control"> Akcija</label></h4>

                        <div class="action-window dn group">

                            <div class="fi-1-2">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="action_price" class="txtinput new-price-action" placeholder="Unesite novu cenu" value="">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-2">

                                <h4 class="cb-title">Iznos akcije</h4>

                                <input type="text" name="action_discount" class="txtinput discount-action" placeholder="Unesite popust" value="">

                                <span class="form-sign">%</span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title"><label><input name="sales" type="checkbox" class="sales-control"> Rasprodaja</label></h4>

                        <div class="sales-window dn group">

                            <div class="fi-1-2">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="sales_price" class="txtinput new-price-sales" placeholder="Unesite novu cenu" value="">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-2">

                                <h4 class="cb-title">Iznos rasprodaje</h4>

                                <input type="text" name="sales_discount" class="txtinput discount-sales" placeholder="Unesite popust" value="">

                                <span class="form-sign">%</span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title"><label><input name="percent" type="checkbox" class="sales-control"> POSTO</label></h4>

                        <div class="sales-window dn group">

                            <div class="fi-1-2">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="percent_price" class="txtinput new-price-sales" placeholder="Unesite novu cenu" value="">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-2">

                                <h4 class="cb-title">Iznos POSTO popusta</h4>

                                <input type="text" name="percent_discount" class="txtinput discount-sales" placeholder="Unesite popust" value="">

                                <span class="form-sign">%</span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Slika proizvoda</h4>

                            <!-- <a href="img/busilica.jpg" class="chosen-img">
                                <img src="img/busilica.jpg">
                            </a> -->

                            <input type="file" class="txtinput" name="userfile" required>

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Formati: JPEG, GIF, PNG. Kolorni mod: RGB.</p>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Opcije</h4>

                            <div class="group">
                                <label class="option-half">
                                    <i class="fa fa-eye"></i>
                                    <input type="checkbox" name="active" checked> Proizvod vidljiv
                                </label>
                                <label class="option-half">
                                    <i class="fa fa-home"></i>
                                    <input type="checkbox" name="featured"> Izdvojen na naslovnoj
                                </label>
                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title">Opis</h4>

                        <div class="ckeditor-outer">
                            <textarea value="" name="desc" id="editor" cols="80" rows="5"></textarea>
                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title">Specifikacija</h4>

                        <div class="spec-items" id="specs">




<!--                             <div class="spec-item group">
                                <label>Jačina:</label>
                                <input type="text" class="txtinput">
                            </div>

                            <div class="spec-item group">
                                <label>Trajanje baterije:</label>
                                <input type="text" class="txtinput">
                            </div>

                            <div class="spec-item group">
                                <label>Promenjive burgije:</label>
                                <input type="text" class="txtinput">
                            </div>
 -->
                        </div> <!-- .spec-items -->

                    </div> <!-- .f-block -->

                    <div class="f-block group" >

                        <h4 class="cb-title">Filteri</h4>

                        <div class="spec-items" id="filters">

<!--                             <div class="spec-item group">
                                <label>Jačina:</label>
                                <div class="selectbox">
                                    <select>

                                        <option></option>

                                        <option value="77"> 150-200 W</option>

                                        <option value="78"> 250-400 W</option>

                                        <option value="79"> 400-700 W</option>

                                   </select>
                                </div>
                            </div>
                            <div class="spec-item group">
                                <label>Burgije:</label>
                                <div class="selectbox">
                                    <select>

                                        <option></option>

                                        <option value="89"> 1-5 </option>

                                        <option value="90"> 5-10 </option>

                                        <option value="91"> 10-15 </option>

                                   </select>
                                </div>
                            </div>
 -->
                        </div> <!-- .spec-items -->

                    </div> <!-- .f-block -->

                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->

