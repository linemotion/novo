        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar matchheight">

                    <nav class="sidebar-nav sidebar-nav-mob">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi "><i class="icon icon-tag"></i> <?=$this->lang->line('on_sale')?></a></li>
    <?php foreach (modules::run("categories/get","position") as $category): ?>
                            <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->$name?></a></li>
    <?php endforeach ?> 
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja "><i class="icon icon-tag"></i> <?=$this->lang->line('sales')?></a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <div class="sidebar-responsive group">
                        <form action="">
                            <select name="choose-cat" id="choose-cat" class="select-url">
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi">
                                    <?=$this->lang->line('on_sale')?>
                                </option>
                                <?php foreach (modules::run("categories/get","position") as $category): ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>">
                                    <?php echo $category->$name?>
                                </option>
                                <?php endforeach ?> 
                                <option selected value="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja ">
                                    <?=$this->lang->line('sales')?></a>
                                </option>
                            </select>
                        </form>
                    </div> <!-- .sidebar-responsive -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="product-items-header">
                        <strong><?=$this->lang->line('all_sales_products')?></strong>
                        <form action="" style="float: right;">
                            <select name="choose-filter-group" id="choose-filter-group">
                                <option value="all-groups"><?=$this->lang->line('all_sales_full')?></option>
                                <?php foreach ($fil_groups as $group): ?>
                                    <option value="<?php echo $group->id ?>"><?php echo $group->$name; ?></option>                    
                                <?php endforeach ?>
                            </select>
                        </form>
                    </div> <!-- .product-items-header -->

                    <div class="inside-product-items group">
<?php $products = modules::run("products/get_where_paginateSales",12,$this->uri->segment(4)); ?>

<?php if ($products): ?>
<?php for($i=0;$i < count($products) ;$i++): ?>  
                  
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_sale')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
<?php if ($products[$i]->show_price): ?>                                       
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->sales_price; ?> €</b>
                                         </span>
                                    </div>
<?php endif ?>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->   
<?php endfor ?>
<?php else: ?>
    <?php if ($this->uri->segment(1) == "en"): ?>
        <p class="no-results">Currently, there aren't any products on sale, please choose a product group.</p>
    <?php else: ?>
        <p class="no-results">Trenutno nemamo proizvoda na rasprodaji, molimo vas da izaberete grupu proizvoda.</p>
    <?php endif ?>

<?php endif ?>     

                    </div> <!-- .group -->



<?php echo modules::run('pagination/paginate_sales'); ?>


                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
