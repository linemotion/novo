        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar matchheight">

                    <nav class="sidebar-nav sidebar-nav-mob">

                        <ul class="sidebar-nav-main">
                            <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi "><i class="icon icon-tag"></i> <?=$this->lang->line('on_sale')?></a></li>
<!--                                 <form action="" class="subnav-content">
                                    <select name="choose-filter-group" id="choose-filter-group">
                                        <option value="all-groups"><?=$this->lang->line('all_groups')?></option>
<?php foreach ($fil_groups as $group): ?>
    <?php if ($group->id == $this->uri->segment(3)): ?>
                                        <option value="<?php echo $group->id ?>" selected><?php echo $group->$name; ?></option>                    
    <?php else: ?>        
                                        <option value="<?php echo $group->id ?>"><?php echo $group->$name; ?></option>                    
    <?php endif ?>
<?php endforeach ?>
                                    </select>
                                </form> -->
    <?php foreach (modules::run("categories/get","position") as $category): ?>
                            <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->$name?></a></li>
    <?php endforeach ?> 
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja "><i class="icon icon-tag"></i> <?=$this->lang->line('sales')?></a></li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <div class="sidebar-responsive group">
                        <form action="">
                            <select name="choose-cat" id="choose-cat" class="select-url">
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi">
                                    <?=$this->lang->line('on_sale')?>
                                </option>
                                <?php foreach (modules::run("categories/get","position") as $category): ?>
                                <option value="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>">
                                    <?php echo $category->$name?>
                                </option>
                                <?php endforeach ?> 
                                <option selected value="/<?php echo $this->uri->segment(1);?>/proizvodi/rasprodaja ">
                                    <?=$this->lang->line('sales')?></a>
                                </option>
                            </select>
                        </form>
                    </div> <!-- .sidebar-responsive -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="product-items-header">
                        <strong>Proizvodi na rasprodaji</strong>
                        <form action="" style="float: right;">
                            <select name="choose-filter-group" id="choose-filter-group">
                                        <option value="all-groups"><?=$this->lang->line('all_sales_full')?></option>
<?php foreach ($fil_groups as $group): ?>
    <?php if ($group->id == $this->uri->segment(3)): ?>
                                        <option value="<?php echo $group->id ?>" selected><?php echo $group->$name; ?></option>                    
    <?php else: ?>        
                                        <option value="<?php echo $group->id ?>"><?php echo $group->$name; ?></option>                    
    <?php endif ?>
<?php endforeach ?>
                                    </select>
                        </form>
                    </div> <!-- .product-items-header -->

                    <div class="inside-product-items group">
<?php if ($products): ?>
<?php for($i=0;$i < count($products) ;$i++): ?>                       

<?php if (!$products[$i]->show_price): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name pi-name-noprice">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 
<?php elseif($products[$i]->discount): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_discount')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->discount_value); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 --> 

<?php elseif($products[$i]->action): ?>

                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_action')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->action_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->action_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->
<?php elseif($products[$i]->sales): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag">
                                    <?=$this->lang->line('pro_sale')?>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?>  €</i> <em>-<?php echo floor($products[$i]->sales_discount); ?>%</em>
                                            </span>
                                            <b><?php echo $products[$i]->sales_price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->

<?php elseif($products[$i]->percent): ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <div class="special-tag-posto">
                                    <span>POSTO</span>
                                </div>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-special">
                                            <span>
                                                <i><?php echo $products[$i]->price; ?> €</i>
                                            </span>
                                            <b>-<?php echo floor($products[$i]->percent_discount); ?>%</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->


<?php else: ?>
                        <div class="col-1-3">
                            <div class="product-item">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                    <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                    <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                                </a>
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-name">
                                    <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span>
                                    <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                                    <div class="product-price">
                                         <span class="product-price-default">
                                            <b><?php echo $products[$i]->price; ?> €</b>
                                         </span>
                                    </div>
                                </a>
                            </div> <!-- .product item -->
                        </div> <!-- .col-1-3 -->    

<?php endif ?>


<?php endfor ?>
<?php else: ?>
    <?php if ($this->uri->segment(1) == "en"): ?>
        <p class="no-results">Currently, there aren't any products on sale, please choose a product group.</p>
    <?php else: ?>
        <p class="no-results">Trenutno nemamo proizvoda na rasprodaji, molimo vas da izaberete grupu proizvoda.</p>
    <?php endif ?>
<?php endif ?>     

                    </div> <!-- .group -->

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
