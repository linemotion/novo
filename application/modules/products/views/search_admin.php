        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
                            <li>
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
<?php endforeach ?>
                       </ul>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>                    
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>                   
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>
            
            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Pretraga (<?php echo $search ?>)<a href="#" ></a></h2>
                </div>

                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>

                <div class="c-block">
                    <div class="search-groups-admin">
                        <ul>
<?php if ($cat_search): ?>
    <?php foreach ($cat_search as $cat_item): ?>
                           <li><a href="/categories/edit/<?php echo $cat_item->id; ?>"><?php echo $cat_item->name;; ?></a></li>        
    <?php endforeach ?>
<?php endif ?>
<?php if ($subcat_search): ?>
    <?php foreach ($subcat_search as $subcat_item): ?>
                           <li> <a href="/subcategories/edit/<?php echo $subcat_item->id; ?>"><?php echo $subcat_item->name;; ?></a> </li>       
    <?php endforeach ?>
<?php endif ?>
<?php if ($sorts_search): ?>
    <?php foreach ($sorts_search as $sort_item): ?>
                           <li> <a href="/sorts/edit/<?php echo $sort_item->id; ?>"><?php echo $sort_item->name;; ?></a>  </li>      
    <?php endforeach ?>
<?php endif ?>
<?php if ($products): ?>
                        </ul>
                    </div>

                    <table class="main-table">
                        <caption class="tab-title">Proizvodi</caption>
                        <thead>
                            <tr>
                                <th class="th-left">Ime proizvoda</th>
                                <th class="th-action">Akcije</th>
                            </tr>
                        </thead>
                        <tbody class="sortable">
<?php foreach ($products as $product): ?>
                            <tr>
                                <td class="td-left td-name">
                                    <a href="/products/edit/<?php echo $product->id; ?>"><?php echo $product->name;; ?></a>
                                </td>
                                <td class="td-action">
                                    <a href="/products/delete/<?php echo $product->id; ?>" class="act-btn del-btn md-trigger" data-modal="modal-del-product" data-id="<?php echo $product->id; ?>" data-controller="products"><i class="fa fa-times"></i>Obriši</a>
                                    <a href="/products/edit/<?php echo $product->id; ?>" class="act-btn edit-btn"><i class="fa fa-pencil"></i>Izmeni</a>
                                    <a href="#" class="move-btn" data-id="<?php echo $product->id; ?>"><i class="fa fa-arrows"></i></a>
                                </td>
                            </tr>
<?php endforeach ?>
<?php else: ?>
                    <table class="main-table">
                        <caption class="tab-title">Proizvodi</caption>
                    </table>

                    <div class="content-blank">

                        <p>Ne postoji proizvod pod tim nazivom.</p>

                    </div> <!-- .content-blank -->  
<?php endif ?>
                        </tbody>
                    </table>

                </div> <!-- .c-block -->


            </div> <!-- .main-content -->

        </section> <!-- .main -->

        <div class="md-modal md-effect-1" id="modal-del-product">
            <div class="md-content md-content-del">
                <h3>Obriši proizvod <button class="md-close"><i class="fa fa-times"></i></button></h3>
                    <div class="c-block">

                        <h4 class="are-sure">Da li ste sigurni da želite da obrišete ovaj proizvod?</h4>

                        <form method="post" id="delete_cat_form" action="">
                            <div class="form-bottom">
                                <button href="#" class="btn-del-large "><i class="fa fa-times"></i> Da, obriši</button>
                            </div>
                        </form>

                    </div> <!-- .c-block -->
            </div>
        </div>
