        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="/admin/dashboard"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="/campaigns/admin"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="/admin/products"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                        <ul class="sidebar-nav-groups">
<?php foreach ($categories as $item): ?>
    <?php if ($item->id == $product->cat_id): ?>

                            <li class="sidebar-nav-groups-selected">
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name ?></a>
                                <ul class="sidebar-nav-subgroups">

        <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$product->cat_id) as $subcat): ?>
            <?php if ($product->subcat_id == $subcat->id): ?>
                                      <li class="sidebar-nav-subgroups-selected"><a href="/subcategories/edit/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>
               <?php else: ?>
                                     <li><a href="/subcategories/edit/<?php echo $subcat->id; ?>"><?php echo $subcat->name ?></a></li>

            <?php endif ?>
        <?php endforeach ?>
                                </ul>
                            </li>
    <?php else: ?>
                            <li>
                                <a href="/categories/edit/<?php echo $item->id; ?>"><?php echo $item->name;?></a>
                            </li>
    <?php endif ?>

<?php endforeach ?>                        </ul>
                    </li>
                    <li>
                        <a href="/manufacturers/admin"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/company/admin"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>                    
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title"><?php echo $product->name; ?></h2>
                </div>

                <?php echo modules::run("template/breadcrumbs",$breadcrumbs); ?>

                <header class="tab-header group">
                    <a href="/products/edit/<?php echo $product->id ?>" class="tab-1-2 tab-active">Sadržaj proizvoda</a>
                    <a href="/products/gallery/<?php echo $product->id ?>" class="tab-1-2 tab">Galerija</a>
                </header>

                <form data-parsley-validate method="Post" action="/products/edit_proccess/<?php echo $product->id; ?>" enctype="multipart/form-data">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Ime proizvoda</h4>

                            <input type="text" name="name" class="txtinput" placeholder="Unesite ime" value="<?php echo $product->name; ?>" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Vrsta</h4>

                                <select class="chosen-select" name="sort" id="sort" required>
<?php foreach (modules::run("sorts/get_where_custom","subcat_id",$product->subcat_id) as $sort): ?>
    <?php if ($sort->id == $product->sort_id): ?>
        <option value="<?php echo $sort->id; ?>" selected><?php echo $sort->name; ?></option>
    <?php else: ?>
        <option value="<?php echo $sort->id; ?>"><?php echo $sort->name; ?></option>
    <?php endif ?>
<?php endforeach ?>
                                </select>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Proizvođač</h4>

                            <select class="chosen-select" name="manufacturers" required>
<?php foreach (modules::run("manufacturers/get","id") as $manuf): ?>
                            <?php if ($manuf->id == $product->manuf_id): ?>
                                <option value="<?php echo $manuf->id; ?>" selected><?php echo $manuf->name; ?></option>
                            <?php else: ?>
                                <option value="<?php echo $manuf->id; ?>"><?php echo $manuf->name; ?></option>
                            <?php endif ?>
<?php endforeach ?>
                            </select>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Šifra</h4>

                            <input type="text" name="code" class="txtinput" placeholder="Unesite šifru" value="<?php echo $product->sifra; ?>" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Cena</h4>
<?php if ($product->show_price): ?>
                        <label class="show-check"><input name="show_price" type="checkbox" checked>Prikaži cenu na sajtu</label>
<?php else: ?>
                        <label class="show-check"><input name="show_price" type="checkbox">Prikaži cenu na sajtu</label>
<?php endif ?>
                            <input class="txtinput db-price" value="<?php echo $product->price; ?>" disabled>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">
<?php if ($product->discount): ?>
                        <h4 class="cb-title"><label><input name="discount" type="checkbox" class="popust-control" checked> Popust</label></h4>
<?php else: ?>
                        <h4 class="cb-title"><label><input name="discount" type="checkbox" class="popust-control"> Popust</label></h4>
<?php endif ?>

                        <div class="popust-window group">

                            <div class="fi-1-3">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="new_price" class="txtinput new-price" placeholder="Unesite novu cenu" value="<?php echo $product->new_price; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Iznos popusta</h4>

                                <input type="text" name="discount_value" class="txtinput discount" placeholder="Unesite popust" value="<?php echo $product->discount_value; ?>">

                                <span class="form-sign">%</span>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Rok važenja</h4>
                                
                                <input type="text" name="discount_end" class="txtinput datepicker" value="<?php echo $product->discount_end; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Datum se unosi u formatu DD.MM.GGGG.</p>
                                
                                <span class="form-sign"><i class="fa fa-calendar"></i></span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

<?php if ($product->action): ?>
                        <h4 class="cb-title"><label><input name="action" type="checkbox" class="action-control" checked> Akcija</label></h4>
<?php else: ?>
                        <h4 class="cb-title"><label><input name="action" type="checkbox" class="action-control"> Akcija</label></h4>
<?php endif ?>
                        <div class="action-window group">

                            <div class="fi-1-3">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="action_price" class="txtinput new-price-action" placeholder="Unesite novu cenu" value="<?php echo $product->action_price; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Iznos akcije</h4>

                                <input type="text" name="action_discount" class="txtinput discount-action" placeholder="Unesite popust" value="<?php echo $product->action_discount; ?>">

                                <span class="form-sign">%</span>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Rok važenja</h4>
                                
                                <input type="text" name="action_end" class="txtinput datepicker" value="<?php echo $product->action_end; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Datum se unosi u formatu DD.MM.GGGG.</p>
                                
                                <span class="form-sign"><i class="fa fa-calendar"></i></span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">

<?php if ($product->sales): ?>
                        <h4 class="cb-title"><label><input name="sales" type="checkbox" class="sales-control" checked> Rasprodaja</label></h4>
<?php else: ?>
                        <h4 class="cb-title"><label><input name="sales" type="checkbox" class="sales-control"> Rasprodaja</label></h4>
<?php endif ?>
                        <div class="sales-window group">

                            <div class="fi-1-3">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="sales_price" class="txtinput new-price-sales" placeholder="Unesite novu cenu" value="<?php echo $product->sales_price; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Iznos rasprodaje</h4>

                                <input type="text" name="sales_discount" class="txtinput discount-sales" placeholder="Unesite popust" value="<?php echo $product->sales_discount; ?>">

                                <span class="form-sign">%</span>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Rok važenja</h4>
                                
                                <input type="text" name="sales_end" class="txtinput datepicker" value="<?php echo $product->sales_end; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Datum se unosi u formatu DD.MM.GGGG.</p>
                                
                                <span class="form-sign"><i class="fa fa-calendar"></i></span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->


                    <div class="f-block group">
<?php if ($product->percent): ?>
                        <h4 class="cb-title"><label><input name="percent" type="checkbox" class="posto-control" checked> Posto</label></h4>
<?php else: ?>
                        <h4 class="cb-title"><label><input name="percent" type="checkbox" class="posto-control"> Posto</label></h4>
<?php endif ?>                        
                        <div class="posto-window group">

                            <div class="fi-1-3">

                                <h4 class="cb-title">Nova cena</h4>

                                <input type="text" name="percent_price" class="txtinput new-price-posto" placeholder="Unesite novu cenu" value="<?php echo $product->percent_price; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Cena se unosi kao broj - bez EUR</p>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Iznos POSTO popusta</h4>

                                <input type="text" name="percent_discount" class="txtinput discount-posto" placeholder="Unesite popust" value="<?php echo $product->percent_discount; ?>">

                                <span class="form-sign">%</span>

                            </div>

                            <div class="fi-1-3">

                                <h4 class="cb-title">Rok važenja</h4>
                                
                                <input type="text" name="percent_end" class="txtinput datepicker" value="<?php echo $product->percent_end; ?>">

                                <p class="form-helper"><i class="fa fa-info-circle"></i> Datum se unosi u formatu DD.MM.GGGG.</p>
                                
                                <span class="form-sign"><i class="fa fa-calendar"></i></span>

                            </div>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Slika proizvoda</h4>

                            <a href="/img/products/<?php echo $product->image; ?>" class="chosen-img">
                                <img src="/img/products/<?php echo $product->image; ?>">
                            </a>

                            <input type="file" class="txtinput" name="userfile">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Formati: JPEG, GIF, PNG. Kolorni mod: RGB.</p>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Opcije</h4>

                            <div class="group">
                                <label class="option-half">
                                    <i class="fa fa-eye"></i>
<?php if ($product->active): ?>
                        <input type="checkbox" name="active" checked> Proizvod vidljiv
<?php else: ?>
                        <input type="checkbox" name="active"> Proizvod vidljiv
<?php endif ?>

                                </label>
                                <label class="option-half">
                                    <i class="fa fa-home"></i>
<?php if ($product->featured): ?>
                         <input type="checkbox" name="featured" checked> Izdvojen na naslovnoj
<?php else: ?>
                         <input type="checkbox" name="featured" > Izdvojen na naslovnoj
<?php endif ?>
                                </label>
                            </div>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title">Opis</h4>

                        <div class="ckeditor-outer">
                            <textarea value="" name="desc" id="editor" cols="80" rows="5"><?php echo $product->desc; ?></textarea>
                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title">Specifikacija</h4>

                        <div class="spec-items">
<?php foreach ($specs as $spec): ?>
                                <div class="spec-item group">
                                    <label><?php echo $spec->name; ?></label>
<?php $value = modules::run("products/get_specs",$spec->id); ?>
<?php if ($value): ?>
                                     <input type="text" name="specs[]" class="txtinput" value="<?php echo htmlspecialchars($value->value); ?>">
<?php else: ?>
                                    <input type="text" name="specs[]" class="txtinput">
<?php endif ?>
                                </div>
<?php endforeach ?>
                        </div> <!-- .spec-items -->

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <h4 class="cb-title">Filteri</h4>

                        <div class="spec-items">
<?php foreach ($filters as $filter): ?>
                                <div class="spec-item group">
                                    <label><?php echo $filter->name; ?>:</label>
                                        <select class="chosen-select" name="values[]">

<?php $filter_values = modules::run("filter_values/get_where_custom","filter_id",$filter->id); ?>
                                            <option value="0"></option>

<?php foreach ($filter_values as $value): ?>
<?php $exists = modules::run("products/prod_value_exists",$value->id, $product->id); ?>
<?php if ($exists): ?>
                                            <option value="<?php echo $value->id; ?>" selected> <?php echo $value->value; ?> </option>
<?php else: ?>
                                            <option value="<?php echo $value->id; ?>"> <?php echo $value->value; ?> </option>
<?php endif ?>
<?php endforeach ?>
                                       </select>
                                </div>
<?php endforeach ?>
                        </div> <!-- .spec-items -->

                    </div> <!-- .f-block -->

                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->