<?php

class Products extends MX_Controller
{
	var $module;


	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_products');

		$this->module = "products";
		$this->load->helper('url');
	}
	function del_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$image = modules::run('pro_gallery/get_by_id', $this->uri->segment(3));

		$this->db->where("id" , $this->uri->segment(3));
		$this->db->delete("pro_gallery");

		redirect('/products/gallery/'.$image->pro_id);
	}
	function add_image()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		if($_FILES['userfile']['error'] == 0)
			$data["image"] = modules::run('resize/upload',"products");

		$data['pro_id'] = $this->uri->segment(3);

		$this->db->insert("pro_gallery",$data);

		redirect('/products/gallery/'.$data['pro_id']);
	}


	function gallery()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		$data['product'] = $this->get_by_id_admin($id);
		$bc_subcategory = modules::run("subcategories/get_by_id",$data['product']->subcat_id);
		$bc_category = modules::run("categories/get_by_id",$data['product']->cat_id);


			$data['breadcrumbs'] = array(
		array("name" => "Proizvodi","link" => "/admin/products"),
		array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		array("name" => $data["product"]->name,"link" => "")
		);
		$data["categories"] = modules::run("categories/get", "position");
		//$data["manufacturers"] = modules::run("manufacturers/get", "id");

		// $data["filters"] = modules::run("filters/get_where_custom","sort_id",$data['product']->sort_id); 
		
		// echo "<pre>";
		// print_r($data['filters']);
		// die();

		// $data["specs"] = modules::run("specifications/get_where_custom","sort_id",$data['product']->sort_id);
		$data['gallery'] = modules::run("pro_gallery/get_where_custom","pro_id",$this->uri->segment(3));

		echo modules::run('template/admin_render',$this->module,"gallery",$data);
	}




	function get_where_filter() 
	{
		$id = $this->uri->segment(7);
		$products = $this->mdl_products->get_where_filter($id);
		return $products;
	}
	function filter_sales()
	{
		$cat_ids = array();

	    $this->lang->load('index',$this->uri->segment(1));
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => "Rasprodaja","link" => "/".$this->uri->segment(1)."/proizvodi/sales")
			); 

		
		$data["title"] = "Proizvodi na rasprodaji";
				
		$data['categories'] = modules::run("categories/get","position");
		//$data['products'] = modules::run("products/get_by_action");
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");

		if ($this->uri->segment(3)) 
			$data["products"] = modules::run("products/get_by_sales_filter",$this->uri->segment(3));
		else
			$data["products"] = modules::run("products/get_by_sales_filter",0);

		

		foreach (modules::run("products/get_by_sales") as $pro) 
			$cat_ids[] = $pro->cat_id;
		
		$cat_ids = array_unique($cat_ids);

		foreach ($cat_ids as $catid) 
			$data["fil_groups"][] =  modules::run("categories/get_by_id",$catid);;

		// 		echo "<pre>";
		// echo print_r($data);
		// die();


		echo modules::run('template/render',$this->module,"filter_sales",$data);
	}


	function sales()
	{
		$this->lang->load('index',$this->uri->segment(1));

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => "Rasprodaja","link" => "/".$this->uri->segment(1)."/proizvodi/sales")
			); 

		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");

		$num = modules::run("products/get_num_sales");

		$data['products'] = $this->get_by_sales();
		$data['title'] = $this->lang->line("sales");

		foreach ($data['products'] as $pro) 
			$cat_ids[] = $pro->cat_id;
		
		$cat_ids = array_unique($cat_ids);

		foreach ($cat_ids as $catid) 
			$data["fil_groups"][] =  modules::run("categories/get_by_id",$catid);




		echo modules::run('template/render',$this->module,"sales",$data);
	}
	//for admin
    function get_specs($spec_id) 
    {    
    	$id = $this->uri->segment(3);

        $this->db->where("pro_id",$id);
        $this->db->where("spec_id",$spec_id);

        $query = $this->db->get("spec_prod");

        if($query->num_rows() > 0)
            return $query->row();
    }

    function get_specs_front($spec_id) 
    {    
    	$id = $this->uri->segment(6);

        $this->db->where("pro_id",$id);
        $this->db->where("spec_id",$spec_id);

        $query = $this->db->get("spec_prod");

        if($query->num_rows() > 0)
            return $query->row();
    }
    function sync() 
    {    
		$xmlfile=$_SERVER['DOCUMENT_ROOT']."/xml/timestamp.txt";

		//$xmlRaw = file_put_contents($xmlfile, date("d-m-Y H:i"));
		$xmlRaw = file_put_contents($xmlfile, gmdate("d-m-Y H:i", time()+(2*60*60)));
		$this->refresh();

    }

//-------------------refresh Prices---------------------------------//

    function refresh() 
    {    
    	$this->load->library('Simplexml');

		$xmlfile=$_SERVER['DOCUMENT_ROOT']."/xml/sajt.XML";

		$xmlRaw = file_get_contents($xmlfile);
		$this->load->library('simplexml');
		$xdata = $this->simplexml->xml_parse($xmlRaw);

		$rows = $xdata['ROWDATA']['ROW'];

		foreach ($rows as $row) 
		{
			$sifra_kase = $row['@attributes']['sifra_kase'];
			$product = $this->sifra_exists($sifra_kase);

			if($product)
			{
				$data['price'] = $row['@attributes']['cena'];
				$this->_update($product->id, $data);
			}
		}
		redirect('/admin/dashboard');
    }


//-------------------^^refresh Prices---------------------------------//

    function prod_value_exists($val_id,$pro_id) 
    {    
        $this->db->where("pro_id",$pro_id);
        $this->db->where("val_id",$val_id);

        $query = $this->db->get("value_prod");

        if($query->num_rows() > 0)
            return true;
        else
        	return false;
    }

    function sifra_exists($sifra_kase) 
    {    
        $this->db->where("sifra",$sifra_kase);

        $query = $this->db->get("products");

        if($query->num_rows() > 0)
            return $query->row();
        else
        	return false;
    }

	function search()
	{
	    $this->lang->load('index',$this->uri->segment(1));
	    $data['name'] = ($this->uri->segment(1) == "en") ? "name_en" : "name" ;
		$data['search'] = $this->input->post("search");
		$data["title"] = $this->lang->line("search");

		$data['breadcrumbs'] = array(
			array("name" => 'Pretraga: "'."<b>".$data['search'].'</b>"',"link" => "")
			); 

		$data['products'] = $this->get_by_search($data['search']);
		$data['cat_search'] = modules::run("categories/get_by_search",$data['search']);
		$data['subcat_search'] = modules::run("subcategories/get_by_search",$data['search']);
		$data['sorts_search'] = modules::run("sorts/get_by_search",$data['search']);

		echo modules::run('template/render',$this->module,"search",$data);

	}

	function search_admin()
	{
		$data["categories"] = modules::run("categories/get","position");
		$data['search'] = $this->input->post("search",true);

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => "Pretraga","link" => "")
			); 

		$data['products'] = $this->get_by_search_admin($data['search']);
		$data['cat_search'] = modules::run("categories/get_by_search",$data['search']);
		$data['subcat_search'] = modules::run("subcategories/get_by_search",$data['search']);
		$data['sorts_search'] = modules::run("sorts/get_by_search",$data['search']);
		
		echo modules::run('template/admin_render',$this->module,"search_admin",$data);

	}
	function action()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => "Akcija","link" => "/".$this->uri->segment(1)."/proizvodi/akcija")
			); 

		$url = $this->uri->segment(5);


		$data['products'] = $this->get_by_action();

		echo modules::run('template/render',$this->module,"action",$data);
	}

	function get_random() 
	{
		$product = $this->mdl_products->get_random();		
		return $product;
	}
	function get_by_search($search) 
	{
		$products = $this->mdl_products->get_by_search($search);		
		return $products;
	}
	function get_by_search_admin($search) 
	{
		$products = $this->mdl_products->get_by_search_admin($search);		
		return $products;
	}
	function get_num_search($search) 
	{
		$num = $this->mdl_products->get_num_search($search);		
		return $num;
	}
	function get_num_sales() 
	{
		$num = $this->mdl_products->get_num_sales();		
		return $num;
	}
	function get_related($subcat_id) 
	{
		$products = $this->mdl_products->get_related($subcat_id);		
		return $products;
	}		
	function review()
	{
	    $this->lang->load('index',$this->uri->segment(1));

		$id = $this->uri->segment(6);


		$data['meta'] = true;

	    $data['name'] = ($this->uri->segment(1) == "en") ? "name_en" : "name" ;

		$data['product'] = $this->get_by_id($id);
		$data['related'] = $this->get_related($data['product']->subcat_id);

		$data['category'] = modules::run("categories/get_by_id",$data['product']->cat_id);
		$data['subcategory'] = modules::run("subcategories/get_by_id",$data['product']->subcat_id);
		$data['specs'] = modules::run("specifications/get_where_custom","sort_id",$data['product']->sort_id);
		$data["name"] = ($this->uri->segment(1) == "en" ? "name_en" : "name");
		
		$data['breadcrumbs'] = array(
			array("name" => $this->lang->line("products"),"link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => $data['category']->$data["name"],"link" => "/".$this->uri->segment(1)."/proizvodi/".$data['category']->url),
			array("name" => $data['subcategory']->$data["name"],"link" => "/".$this->uri->segment(1)."/proizvodi/".$data['category']->url."/".$data['subcategory']->url),
			array("name" => $data['product']->name,"link" => "")			
			); 
		$data["title"] = modules::run('sorts/get_by_id',$data["product"]->sort_id)->name." ".modules::run('manufacturers/get_by_id',$data["product"]->manuf_id)->name." ".$data['product']->name;
		
		$data['gallery'] = modules::run("pro_gallery/get_where_custom","pro_id",$id);

		echo modules::run('template/render',$this->module,"single",$data);
	}


	function create()
	{		
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$subcat_id = $this->uri->segment(3);
		
		$data["subcategory"] = modules::run("subcategories/get_by_id",$subcat_id);
		$data["category"] = modules::run("categories/get_by_id",$data["subcategory"]->cat_id);

		 	$data['breadcrumbs'] = array(
		array("name" => "Proizvodi","link" => "/admin/products"),
		array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id),
		array("name" => $data['subcategory']->name,"link" => "/subcategories/edit/".$data["subcategory"]->id),
		array("name" => "Novi proizvod","link" => "")
		);

		$data["categories"] = modules::run("categories/get","position");
		//$data["manufacturers"] = modules::run("manufacturers/get","id");
		// $data["specifications"] = modules::run("specifications/get_where_custom","sort_id",$subcat_id);
		// $data["filters"] = modules::run("filters/get_where_custom","subcat_id",$subcat_id);

		echo modules::run('template/admin_render',$this->module,"create",$data);
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$product = $this->get_by_id($this->uri->segment(3));
		$this->_delete($product->id);

		redirect('/subcategories/edit/'.$product->subcat_id);
	}		
	public function pagination()
	{
		redirect('/<?php echo $this->uri->segment(1);?>/proizvodi/pretraga/'.url_title(rs_char($this->input->post('search'))));
	}

	function edit()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);

		$data['product'] = $this->get_by_id_admin($id);
		$bc_subcategory = modules::run("subcategories/get_by_id",$data['product']->subcat_id);
		$bc_category = modules::run("categories/get_by_id",$data['product']->cat_id);


			$data['breadcrumbs'] = array(
		array("name" => "Proizvodi","link" => "/admin/products"),
		array("name" => $bc_category->name,"link" => "/categories/edit/".$bc_category->id),
		array("name" => $bc_subcategory->name,"link" => "/subcategories/edit/".$bc_subcategory->id),
		array("name" => $data["product"]->name,"link" => "")
		);
		$data["categories"] = modules::run("categories/get", "position");
		//$data["manufacturers"] = modules::run("manufacturers/get", "id");

		$data["filters"] = modules::run("filters/get_where_custom","sort_id",$data['product']->sort_id); 
		
		// echo "<pre>";
		// print_r($data['filters']);
		// die();

		$data["specs"] = modules::run("specifications/get_where_custom","sort_id",$data['product']->sort_id);
		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}

	function create_proccess()
	{	

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		if($_FILES['userfile']['error'] == 0)
			$data['image'] = modules::run('resize/upload',$this->module);
		
		$subcat_id = $this->uri->segment(3);
		$sort_id = $this->input->post("sort");

		$subcategory = modules::run("subcategories/get_by_id",$subcat_id);

		$specification = modules::run("specifications/get_where_custom","sort_id",$sort_id);
		$specs = $this->input->post('specs');



		$data['name'] = ($this->input->post('name')) ? $this->input->post('name') : "";
		$data['url'] = ($this->input->post('name')) ? url_title(rs_char($this->input->post('name'))) : "";
		$data['sifra'] = ($this->input->post('code')) ? $this->input->post('code') : 0;
		$data['cat_id'] = $subcategory->cat_id;
		$data['cat_name'] = modules::run('categories/get_by_id',$subcategory->cat_id)->name;
		$data['subcat_id'] = $subcat_id;
		$data['subcat_name'] = $subcategory->name;
		$data['sort_id'] = $sort_id;
		$data['manuf_id'] = $this->input->post('manufacturers');
		$data['sort_name'] =  modules::run('sorts/get_by_id',$sort_id)->name;
		$data['new_price'] = ($this->input->post('new_price')) ? $this->input->post('new_price') : 0;
		$data['action_price'] = ($this->input->post('action_price')) ? $this->input->post('action_price') : 0;
		$data['action_discount'] = ($this->input->post('action_discount')) ? $this->input->post('action_discount') : 0;
		$data['action_end'] = ($this->input->post('action_end')) ? $this->input->post('action_end') : "";
		$data['sales_price'] = ($this->input->post('sales_price')) ? $this->input->post('sales_price') : 0;;
		$data['sales_discount'] = ($this->input->post('sales_discount')) ? $this->input->post('sales_discount') : 0;
		$data['sales_end'] = ($this->input->post('sales_end')) ? $this->input->post('sales_end') : "";
		$data['percent_price'] = ($this->input->post('percent_price')) ? $this->input->post('percent_price') : 0;
		$data['percent_discount'] = ($this->input->post('percent_discount')) ? $this->input->post('percent_discount') : 0;
		$data['percent_end'] = ($this->input->post('percent_end')) ? $this->input->post('percent_end') : "";
		$data['discount_value'] = ($this->input->post('discount_value')) ? $this->input->post('discount_value') : 0;
		$data['discount_end'] = ($this->input->post('discount_end')) ? $this->input->post('discount_end') : "";
		$data['show_price'] = ($this->input->post('show_price')) ? 1 : 0;
		$data['desc'] = ($this->input->post('desc')) ? $this->input->post('desc') : "";
		$data['discount'] = ($this->input->post('discount')) ? 1 : 0;
		$data['action'] = ($this->input->post('action')) ? 1 : 0;
		$data['sales'] = ($this->input->post('sales')) ? 1 : 0;
		$data['percent'] = ($this->input->post('percent')) ? 1 : 0;
		$data['featured'] = ($this->input->post('featured')) ? 1 : 0;
		$data['active'] = ($this->input->post('active')) ? 1 : 0;


		$id = $this->_insert($data);
		
		for ($i=0; $i < count($specification); $i++) 
		{ 
			$spec["spec_id"] = $specification[$i]->id;
			$spec["spec_name"] = $specification[$i]->name;
			$spec["pro_id"] = $id;
			$spec["value"] = $specs[$i];

    		$this->db->insert("spec_prod", $spec);

		}

		foreach ($this->input->post("filters") as $value)
		{
			$filter["pro_id"] = $id;
			$filter["val_id"] = $value;

    		$this->db->insert("value_prod", $filter);
		}

		// redirect('/products/edit/'.$id);
		redirect('/subcategories/edit/'.$subcat_id);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		$id = $this->uri->segment(3);
		$product = $this->get_by_id_admin($id);
		$sort_id = $this->input->post('sort');
		
		if($_FILES['userfile']['error'] == 0)
		{
			modules::run('resize/delete',$this->module,$product->image);
			$data['image'] = modules::run('resize/upload',$this->module);
		}

		$specifications = modules::run("specifications/get_where_custom","sort_id",$product->sort_id);
		$filters = modules::run("filters/get_where_custom","sort_id",$product->sort_id);
		$specs = $this->input->post('specs');


		$data['name'] = ($this->input->post('name')) ? $this->input->post('name') : "";
		$data['url'] = ($this->input->post('name')) ? url_title(rs_char($this->input->post('name'))) : "";
		$data['sifra'] = ($this->input->post('code')) ? $this->input->post('code') : 0;
		$data['sort_id'] = $sort_id;
		$data['manuf_id'] = $this->input->post('manufacturers');
		$data['sort_name'] =  modules::run('sorts/get_by_id',$sort_id)->name;
		$data['new_price'] = ($this->input->post('new_price')) ? $this->input->post('new_price') : 0;
		$data['action_price'] = ($this->input->post('action_price')) ? $this->input->post('action_price') : 0;
		$data['action_discount'] = ($this->input->post('action_discount')) ? $this->input->post('action_discount') : 0;
		$data['action_end'] = ($this->input->post('action_end')) ? $this->input->post('action_end') : "";
		$data['sales_price'] = ($this->input->post('sales_price')) ? $this->input->post('sales_price') : 0;;
		$data['sales_discount'] = ($this->input->post('sales_discount')) ? $this->input->post('sales_discount') : 0;
		$data['sales_end'] = ($this->input->post('sales_end')) ? $this->input->post('sales_end') : "";
		$data['percent_price'] = ($this->input->post('percent_price')) ? $this->input->post('percent_price') : 0;
		$data['percent_discount'] = ($this->input->post('percent_discount')) ? $this->input->post('percent_discount') : 0;
		$data['percent_end'] = ($this->input->post('percent_end')) ? $this->input->post('percent_end') : "";
		$data['discount_value'] = ($this->input->post('discount_value')) ? $this->input->post('discount_value') : 0;
		$data['discount_end'] = ($this->input->post('discount_end')) ? $this->input->post('discount_end') : "";
		$data['show_price'] = ($this->input->post('show_price')) ? 1 : 0;
		$data['desc'] = ($this->input->post('desc')) ? $this->input->post('desc') : "";
		$data['discount'] = ($this->input->post('discount')) ? 1 : 0;
		$data['action'] = ($this->input->post('action')) ? 1 : 0;
		$data['sales'] = ($this->input->post('sales')) ? 1 : 0;
		$data['percent'] = ($this->input->post('percent')) ? 1 : 0;
		$data['featured'] = ($this->input->post('featured')) ? 1 : 0;
		$data['active'] = ($this->input->post('active')) ? 1 : 0;



/*
		$data['name'] = $this->input->post('name');
		$data['url'] = url_title(rs_char($this->input->post('name')));
		$data['sifra'] = $this->input->post('code');
		$data['sort_id'] = $this->input->post('sort');
		$data['manuf_id'] = $this->input->post('manufacturers');
		$data['sort_name'] =  modules::run('sorts/get_by_id',$data['sort_id'])->name;
		$data['new_price'] = $this->input->post('new_price');
		$data['show_price'] = ($this->input->post('show_price')) ? 1 : 0;
		$data['desc'] = $this->input->post('desc');
		$data['discount'] = ($this->input->post('discount')) ? 1 : 0;
		$data['action'] = ($this->input->post('action')) ? 1 : 0;
		$data['sales'] = ($this->input->post('sales')) ? 1 : 0;
		$data['percent'] = ($this->input->post('percent')) ? 1 : 0;
		$data['discount_value'] = $this->input->post('discount_value');
		$data['action_discount'] = $this->input->post('action_discount');
		$data['action_price'] = $this->input->post('action_price');
		$data['sales_discount'] = $this->input->post('sales_discount');
		$data['sales_price'] = $this->input->post('sales_price');
		$data['percent_price'] = $this->input->post('percent_price');
		$data['percent_discount'] = $this->input->post('percent_discount');
		$data['percent_end'] = $this->input->post('percent_end');
		$data['featured'] = ($this->input->post('featured')) ? 1 : 0;
		$data['active'] = ($this->input->post('active')) ? 1 : 0;
		$data['action_end'] = $this->input->post('action_end');
		$data['sales_end'] = $this->input->post('sales_end');
		$data['discount_end'] = $this->input->post('discount_end');*/


		//delete all spec_values before insert
		$this->db->where("pro_id",$id);
		$this->db->delete("spec_prod");

		for ($i=0; $i < count($specifications); $i++) 
		{ 
			$spec["spec_id"] = $specifications[$i]->id;
			$spec["pro_id"] = $product->id;
			$spec["value"] = ($specs[$i]) ? $specs[$i] : "";

			$this->db->insert("spec_prod", $spec);
			
		}

		//delete all filters before insert
		$this->db->where("pro_id",$id);
		$this->db->delete("value_prod");

		foreach ($this->input->post("values") as $value)
		{
			$filter["pro_id"] = $id;
			$filter["val_id"] = ($value) ? $value : "";

    		$this->db->insert("value_prod", $filter);
		}

		$this->_update($id,$data);

		redirect('/products/edit/'.$id);
	}

	function get_by_action() 
	{
		$products = $this->mdl_products->get_by_action();
		return $products;
	}
	function get_by_action_dis($cat_id) 
	{
		$products = $this->mdl_products->get_by_action_dis($cat_id);
		return $products;
	}
	function get_by_sales_filter($cat_id) 
	{
		$products = $this->mdl_products->get_by_sales_filter($cat_id);
		return $products;
	}
	function get_by_sales() 
	{
		$products = $this->mdl_products->get_by_sales();
		return $products;
	}
	function get_home() 
	{
		$products = $this->mdl_products->get_home();
		return $products;
	}
	function get_num_by_paginateView($id) 
	{
		$products = $this->mdl_products->get_num_by_paginateView($id);
		return $products;
	}
	function get_num_by_paginateAction($id) 
	{
		$products = $this->mdl_products->get_num_by_paginateAction($id);
		return $products;
	}
	function get_num_by_paginateFront($id) 
	{
		$products = $this->mdl_products->get_num_by_paginateFront($id);
		return $products;
	}

	function get_num_by_paginateSearch($search) 
	{
		$products = $this->mdl_products->get_num_by_paginateSearch($search);
		return $products;
	}

	function get_where_paginateView($id,$per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateView($id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateAction($per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateAction($per_page,$offset);
		return $products;
	}
	function get_where_paginateSales($per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateSales($per_page,$offset);
		return $products;
	}
	function get_where_paginateFront($id,$per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateFront($id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontCategory($cat_id,$per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateFrontCategory($cat_id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontSort($sort_id,$per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateFrontSort($sort_id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontFilter($sort_id,$per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateFrontFilter($sort_id,$per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontAll($per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateFrontAll($per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontAllAction($per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateFrontAllAction($per_page,$offset);
		return $products;
	}
	function get_where_paginateFrontAll_rows() 
	{
		$products = $this->mdl_products->get_where_paginateFrontAll_rows();
		return $products;
	}
	function get_where_paginateFrontAllAction_rows() 
	{
		$products = $this->mdl_products->get_where_paginateFrontAllAction_rows();
		return $products;
	}

	function get_where_paginateSearch($search,$per_page,$offset) 
	{
		$products = $this->mdl_products->get_where_paginateSearch($search,$per_page,$offset);
		return $products;
	}
	function get($order_by)
	{
		$products = $this->mdl_products->get($order_by);
		return $products;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$products = $this->mdl_products->get_with_limit($limit, $offset, $order_by);
		return $products;
	}

	function get_by_id($id)
	{
		$products = $this->mdl_products->get_where($id);
		return $products;
	}
	function get_by_id_admin($id)
	{
		$products = $this->mdl_products->get_where_admin($id);
		return $products;
	}
	function get_where_custom($col, $value) 
	{
		$products = $this->mdl_products->get_where_custom($col, $value);
		return $products;
	}
	function get_where_custom_admin($col, $value) 
	{
		$products = $this->mdl_products->get_where_custom_admin($col, $value);
		return $products;
	}
	function _insert($data)
	{
		return $this->mdl_products->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_products->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_products->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_products->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_products->get_max();
		return $max_id;
	}

	function _custom_query($mysql_query) 
	{
		$products = $this->mdl_products->_custom_query($mysql_query);
		return $products;
	}

}