<?php
class Mdl_products extends CI_Model 
{

    var $table; 

    function __construct() 
    {
        parent::__construct();

        $this->table = "products";
    }
    function get_by_action_dis($cat_id) 
    {    
        if($cat_id)    
            $this->db->where("(action = '1' OR discount = '1' OR percent = '1') AND cat_id = '". $cat_id ."'");
        else
            $this->db->where("(action = '1' OR discount = '1' OR percent = '1')");

        $this->db->order_by("id",'desc');
        $this->db->where('active', 1); 

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->result();
    }

    function get_by_sales_filter($cat_id) 
    {    
        if($cat_id)    
            $this->db->where("sales = '1' AND cat_id = '". $cat_id ."'");
        else
            $this->db->where("sales = '1'");

        $this->db->order_by("id",'desc');
        $this->db->where('active', 1); 

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->result();
    }

    function get_by_sales() 
    {        
        $this->db->order_by("id",'desc');
        $this->db->where('sales', 1); 
        $this->db->where('active', 1); 

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateSearch($search,$per_page,$offset) 
    {        
        $this->db->where('active',1);
        $this->db->order_by("id",'desc');
        $this->db->like('name', $search); 

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_filter() 
    {        
        //$filter_values = $this->input->post('filter_values');
        if (count($this->session->userdata("filters")) > 0) 
            $filters = $this->session->userdata("filters");
        else
            $filters = $this->input->post('filters');

        $i=0;$j = 0;
        $abcd = array("a","b","c","d","e","f","g","h","i","j");
        foreach ($abcd as $char) 
        {
            foreach ($filters as $key => $value) 
            {
                if($char == $key[0])
                    $filters_grouped[$j][] = $value;
                $i++;
            } 
            $j++;           
        }

        if (count($filters_grouped) == 1) 
        {
            $pro_ids = $this->get_IN_filter(array_shift( array_values($filters_grouped)));
        }
        else
        {
            $pro_ids = $this->get_ALL_filter($filters_grouped);
        }

        foreach ($pro_ids as $id)
        {
            $test = modules::run("/products/get_by_id",$id);
            if($test)
                $final[] = modules::run("/products/get_by_id",$id);
        }

        return ($final) ? $final : array();
    }
    function get_by_search($search) 
    {        
        $final = array();
        $prod = array();
        $return = array();

        $this->db->escape($search);
        $this->db->where("active",1);
        $this->db->order_by("id",'desc');
        $this->db->like('name', $search); 
        $this->db->or_like('desc', $search); 
        $this->db->limit(50);

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            $final = $query->result();

        $this->db->like('name', $search); 

        $query = $this->db->get("manufacturers");
        if($query->num_rows() > 0)
            $temp = $query->result();

        foreach ($temp as $manuf)
            $prod[] = modules::run("products/get_where_custom","manuf_id",$manuf->id);


        foreach ($prod[0] as $pro) 
            $return[] = $pro;

        foreach ($final as $fin) 
            $return[] = $fin;

        return $return;

    }
    function get_by_search_admin($search) 
    {        
        $this->db->escape($search);
        $this->db->order_by("id",'desc');
        $this->db->like('name', $search); 
        $this->db->limit(50);

        $query = $this->db->get($this->table);

        if($query->num_rows() > 0)
            return $query->result();

    }
    function get_num_search($search) 
    {     
        $this->db->where('active',1);   
        $this->db->order_by("id",'desc');
        $this->db->like('name', $search); 
        $this->db->or_like('desc', $search); 

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->num_rows();
    }
    function get_num_sales() 
    {     
        $this->db->where('active',1);   
        $this->db->where('sales', 1); 
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->num_rows();
    }    
    function get_where_paginateView($subcat_id,$per_page,$offset) 
    {
           
        $this->db->order_by("id",'desc');
        $this->db->where("subcat_id",$subcat_id);

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateAction($per_page,$offset) 
    {        
        $this->db->where('active',1);
        $this->db->where("action",1);
        $this->db->where("discount",1);
        $this->db->where("percent",1);
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateSales($per_page,$offset) 
    {        
        $this->db->where('active',1);
        $this->db->where("sales",1);
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateFront($subcat_id,$per_page,$offset) 
    {        
        $this->db->where('active',1);
        $this->db->order_by("id",'desc');
        $this->db->where("subcat_id",$subcat_id);

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateFrontCategory($cat_id,$per_page,$offset) 
    {        
        $this->db->where('active',1);
        $this->db->where('cat_id',$cat_id);
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateFrontSort($sort_id,$per_page,$offset) 
    {        
        $this->db->where('active',1);
        $this->db->where('sort_id',$sort_id);
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateFrontFilter($sort_id,$per_page,$offset) 
    {        
        $products = $this->get_where_filter();

        if (!$offset) $offset = 0;

        $final = array();

        if ($products)
        {
            for ($i=$offset; $i < count($products) and $i < $per_page + $offset ; $i++)
            { 
                $final[] = $products[$i];
            }
        }

        return $final ;
    }
    function get_where_paginateFrontAll($per_page,$offset) 
    {        
        //$this->db->where('active',1);
        $this->db->where("action",1);
        $this->db->or_where("discount",1);
        $this->db->or_where("percent",1);
        //$this->db->group_by("discount");
        $this->db->order_by("action",'desc');

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateFrontAll_rows() 
    {        
        //$this->db->where('active',1);
        $this->db->where("action",1);
        $this->db->or_where("discount",1);
        $this->db->or_where("percent",1);
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->num_rows();
    }
    function get_where_paginateFrontAllAction($per_page,$offset) 
    {        
        //$this->db->where('active',1);
        $this->db->where("action",1);
        $this->db->or_where("discount",1);
        $this->db->or_where("percent",1);
        //$this->db->group_by("discount");
        $this->db->order_by("action",'desc');

        $query = $this->db->get($this->table, $per_page, $offset);
        if($query->num_rows() > 0)
            return $query->result();
    }
    function get_where_paginateFrontAllAction_rows() 
    {        
        //$this->db->where('active',1);
        $this->db->where("action",1);
        $this->db->or_where("discount",1);
        $this->db->or_where("percent",1);
        $this->db->order_by("id",'desc');

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0)
            return $query->num_rows();
    }
    function get_related($subcat_id)
    {
        $this->db->where('active',1);
        $this->load->helper('array');
        $this->db->where('subcat_id', $subcat_id);
        $this->db->where('url !=', $this->uri->segment(4));
        $query=$this->db->get($this->table);

        $products = $query->result();
        
        if(count($products) > 4)
        {
            $random = array();
            $counter = 0;
            while($counter != 3)
            {
            $temp = random_element($products);
            if(!in_array($temp,$random) && $temp->url != $this->uri->segment(4))
            {
                array_push($random,$temp);
                $counter++;
            }
            }
        }
        else
            $random = $products;


        return $random;
    }

    function get_random()
    {
        $this->db->where('active',1);
        $this->load->helper('array');

        $this->db->where('cat_id', 39);
        $query=$this->db->get($this->table);

        $products = $query->result();
        
        $random = random_element($products);

        return $random;
    }
    function get_by_url($url)
    {
        $this->db->where('active',1);
        $this->db->where('url', $url);
        $query=$this->db->get($this->table);

        return $query->row();
    }
    function get_home() 
    {
        $this->load->helper('array');
        $this->db->where('featured',1);
        //$this->db->where('active',1);
        $query=$this->db->get($this->table);

        $products = $query->result();
        

        $random = array();
        $counter = 0;
        while($counter != count($products))
        {
            $temp = random_element($products);
            if(!in_array($temp,$random))
            {
                array_push($random,$temp);
                $counter++;
            }
        }

        return $random;
    }
    function get_by_action() 
    {
        $this->db->where('active',1);
        $this->db->where('action',1);
        $this->load->helper("array");
        
        $query=$this->db->get($this->table);

        $products = $query->result();

        $random = array();
        $counter = 0;

        if(count($products) > 3)
        {
            while($counter != 3)
            {
                $temp = random_element($products);
                if(!in_array($temp,$random))
                {
                    array_push($random,$temp);
                    $counter++;
                }
            }
            
            return $random;

        }
        else
            return $products;

    }

    function get($order_by = "position")
    {
        $this->db->where('active',1);
        $this->db->order_by($order_by,"desc");
        $query=$this->db->get($this->table);
        return $query->result();
    }

    function get_with_limit($limit, $offset, $order_by) {
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by);
    $query=$this->db->get($this->table);
    return $query;
    }

    function get_where($id){
        $this->db->where('active',1);
    $this->db->where('id', $id);
    $query=$this->db->get($this->table);
    return $query->row();
    }
    function get_where_admin($id){
    $this->db->where('id', $id);
    $query=$this->db->get($this->table);
    return $query->row();
    }
    function get_where_custom($col, $value) {
        $this->db->order_by('id', "desc");
        $this->db->where('active',1);
    $this->db->where($col, $value);
    $query=$this->db->get($this->table);
    return $query->result();
    }
    function get_where_custom_admin($col, $value) {
        $this->db->order_by('id', "desc");
        //$this->db->where('active',1);
    $this->db->where($col, $value);
    $query=$this->db->get($this->table);
    return $query->result();
    }
    function _insert($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
    }

    function _update($id, $data){
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);
    }

    function _delete($id){
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    }

    function count_where($column, $value) {
    $this->db->where($column, $value);
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function count_all() {
    $query=$this->db->get($this->table);
    $num_rows = $query->num_rows();
    return $num_rows;
    }

    function get_max($field = "id") 
    {
        $this->db->select_max($field);
        $query = $this->db->get($this->table);
        $row=$query->row();
        return $query->row()->id;
    }

    function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
    }
    function get_IN_filter($item)
    {
        $final_ids = array();
        $sql="
          SELECT pro_id 
          FROM value_prod
          WHERE val_id in (";
             
        $t = 0;

        foreach ($item as $item_id)
        {   
            if($t != count($item)-1)
                $sql .= $item_id.",";
            else
                $sql .= $item_id;
          $t++;  
        }

        $sql .= ") GROUP  BY pro_id;";

        $query = $this->db->query($sql);
        $pro_ids = $query->result();

        foreach ($pro_ids as $id) 
        {
            $final_ids[] = $id->pro_id;
        }

        return $final_ids;
    }
    function get_ALL_filter($filters_grouped)
    {
        $unrefined_ids = array();

        $i = 0;
        foreach($filters_grouped as $item)
        {
            $unrefined_ids[] = $this->get_IN_filter($item);
            $i++;
        }
        $intersect = call_user_func_array('array_intersect',$unrefined_ids);

        return $intersect;
    }

}

