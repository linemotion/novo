<?php

class Specifications extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_specifications');
	}

	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);
		$data['subcategory'] = modules::run("subcategories/get_by_id",$id);		
		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);		
		
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
			array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
			);

		$data['categories'] = modules::run("categories/get","position");
		$data["specifications"] = modules::run("specifications/get_where_custom","subcat_id",$id); 
		echo modules::run('template/admin_render',"specifications","admin",$data);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');

		$spec = $this->get_by_id($id);
		
		// if($_FILES['userfile']['error'] == 0)
		// {
		// 	modules::run('resize/delete',$this->module,$category->image);
		// 	$data['image'] = modules::run('resize/upload',$this->module);
		// }

		$this->_update($id,$data);

		redirect('/sorts/edit/'.$spec->sort_id);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['name'] = $this->input->post('name');
		$data['sort_id'] = $this->uri->segment(3);

		$this->_insert($data);

		redirect('/sorts/edit/'.$this->uri->segment(3));
	}
	function change()
	{

		$id = $this->input->post('id');
		$output = '<select name="tags" class="uniformselect" id="tag"><option value="">Izaberite jednu</option>';
        
        foreach ($this->get_where_custom('subcat_id',$id) as $item) 
        {
        	$output .= '<option value="'.$item->id.'">'.$item->name.'</option>';	
        }

        echo $output."</select>";
	}
	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$sort_id = modules::run("specifications/get_by_id",$this->uri->segment(3))->sort_id;

		$this->_delete($this->uri->segment(3));
		redirect('/sorts/edit/'.$sort_id);
	}

	function get($order_by)
	{
		$tags = $this->mdl_specifications->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_specifications->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_specifications->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_specifications->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_specifications->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_specifications->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_specifications->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_specifications->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_specifications->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_specifications->_custom_tags($mysql_tags);
		return $tags;
	}

}