        <div class="group">

            <aside class="sidebar">

                <nav class="site-nav group">
                    <ul>
                        <li><a href="#" class="entypo-home">Naslovna</a></li>
                        <li><a href="#" class="entypo-switch">Porudžbine</a></li>
                        <li class="sn-selected has-subnav">
                            <a href="/admin/products" class="entypo-monitor">Proizvodi</a>
                            <ul class="level-1">
<?php foreach ($categories as $key): ?>
    <?php if ($key->id == $subcategory->cat_id): ?>
                                <li class="sn-sub-selected">
                                    <a href="/categories/admin/<?php echo $subcategory->cat_id; ?>"><?php echo $subcategory->cat_name; ?></a>
                                    <ul class="level-2">
<?php foreach (modules::run("subcategories/get_where_custom","cat_id",$subcategory->cat_id) as $subs): ?>
    <?php if ($subs->id == $subcategory->id): ?>
                                        <li class="sn-sub-sub-selected"><a href="/subcategories/admin/<?php echo $subcategory->id; ?>"><?php echo $subcategory->name; ?></a></li>
    <?php else: ?>
                                        <li><a href="/subcategories/admin/<?php echo $subcategory->id; ?>"><?php echo $subcategory->name; ?></a></li>       
    <?php endif ?>
<?php endforeach ?>
                                    </ul>
                                </li>
    <?php else: ?>
                                        <li><a href="/categories/admin/<?php echo $key->id; ?>"><?php echo $key->name; ?></a></li>       
    <?php endif ?>
<?php endforeach ?>
                            </ul>
                        </li>
                        <li><a href="/manufacturers/admin" class="entypo-user">Proizvođači</a></li>
                        <li><a href="#" class="entypo-newspaper">Stranice</a></li>
                    </ul>
                </nav>

                <p class="copyright">
                    LINEMOTION CMS / <a href="#">KONTAKT</a>
                </p>

            </aside> <!-- .sidebar -->

            <section class="content">

                <div class="content-inner">
                
                    <div class="breadcrumbs group">

                        <ul>
                            <li class="bc-home"><a href="#" class="entypo-home"></a></li>
                            <li><a href="#">Proizvodi</a></li>
                            <li><a href="#">TV i Oprema</a></li>
                            <li>Televizori (filteri)</li>
                        </ul>

                        <div class="page-action">
                            <a href="#" class="md-trigger btn entypo-plus" data-modal="modal-new-spec">Dodaj novi filter</a>
                        </div>

                    </div>

                    <div class="content-block">

                        <table class="main-table">
                            <caption class="tab-title"><em>Filteri za:</em> Televizori</caption>
                            <thead>
                                <tr>
                                    <th class="th-left">Filter</th>
                                    <th class="th-action">Akcija</th>
                                </tr>
                            </thead>
                            <tbody>
<?php foreach ($filters as $filter): ?>
                                 <tr>
                                    <td class="td-left td-name">
                                        <?php echo $filter->name; ?>
                                        <div class="values">
                                            <ul class="values-list">
<?php foreach (modules::run("filters/get_where_custom_values","filter_id",$filter->id) as $val): ?>
                                    <td>
                                        <a href="/filters/delete_value/<?php echo $val->id; ?>" onclick="return confirm('Are you sure want to delete?');" class="table-btn entypo-minus-circled"  data-title="Obriši"></a>
                                    </td>              
<?php endforeach ?>
                                                <li class="add-new-value"><a href="/val/create" class="md-trigger" data-modal="modal-2" data-filter-id="<?php echo $filter->subcat_id; ?>">+</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="/filter/delete/<?php echo $filter->id; ?>" onclick="return confirm('Are you sure want to delete?');" class="table-btn entypo-minus-circled"  data-title="Obriši"></a>
                                    </td>
                                </tr>   
<?php endforeach ?>
                            </tbody>
                        </table>

                    </div>

                </div> <!-- .content-inner -->

            </section> <!-- .content -->


                            </tbody>
                        </table>

                    </div>

                </div> <!-- .content-inner -->

            </section> <!-- .content -->

        </div>

        <div class="md-modal md-effect-1" id="modal-1">
            <div class="md-content">
                <div>
                    <p class="cent">Da li ste sigurni da želite da obrišete ovaj filter?</p>
                    <p class="cent">
                        <button class="entypo-check">Da</button>
                        <a class="button entypo-cancel md-close">Ne</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-new-spec">
            <div class="md-content md-content-default">
                <h3>Novi filter</h3>
                <div>
                    <p class="cent">
                        <h4 class="cb-title">Ime filtera:</h4>
                        <input type="text" class="txtinput" placeholder="Unesite ime" data-required="true">
                    </p>
                    <p class="cent">
                        <button class="entypo-check">Dodaj</button>
                        <a class="button entypo-cancel md-close">Poništi</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="md-modal md-effect-1" id="modal-2">
            <div class="md-content md-content-default">
                <h3>Nova vrednost</h3>
                <div>
                    <form action="" id="filter-value-form" method="POST">
                    <p class="cent">
                        <h4 class="cb-title">Ime vrednosti:</h4>
                        <input type="text" class="txtinput" placeholder="Unesite ime" data-required="true">
                    </p>
                    <p class="cent">
                        <button class="entypo-check">Dodaj</button>
                        <a class="button entypo-cancel md-close">Poništi</a>
                    </p>
                    </form>
                </div>
            </div>
        </div>


        <div class="md-overlay"></div><!-- the overlay element -->
