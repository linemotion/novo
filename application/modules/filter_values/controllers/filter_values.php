<?php

class Filter_values extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('mdl_filter_values');
	}

	function admin()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);
		$data['subcategory'] = modules::run("subcategories/get_by_id",$id);		
		$bc_category = modules::run("categories/get_by_id",$data['subcategory']->cat_id);		
		
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/admin/products"),
			array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
			array("name" => $data['subcategory']->name." (Filteri)","link" => "")
			);

		$data['categories'] = modules::run("categories/get","position");
		//$data["filter_values"] = modules::run("filter_values/get_where_custom","subcat_id",$id); 
		//$data["values"] = modules::run("filter_values/get_where_custom_values","filter_id",$filter->id); 
		echo modules::run('template/admin_render',"filter_values","admin",$data);
	}

	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['value'] = $this->input->post('name');
		$data['filter_id'] = $this->uri->segment(3);
		
		$filter = modules::run("filters/get_by_id",$this->uri->segment(3));
		
		$id = $this->_insert($data);

		//echo base_url()."/filters/admin/".$filter->subcat_id;
		redirect('/filters/admin/'.$filter->sort_id);
	}

	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['value'] = $this->input->post('name');
		$filter_id= modules::run("filter_values/get_by_id",$this->uri->segment(3))->filter_id;
		
		$filter = modules::run("filters/get_by_id",$filter_id);
		
		$id = $this->_update($this->uri->segment(3),$data);

		//echo base_url()."/filters/admin/".$filter->subcat_id;
		redirect('/filters/admin/'.$filter->sort_id);
	}

	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$values = $this->input->post('values');

		for ($i=0; $i <= count($values); $i++) 
		{ 
			$data['position'] = $i + 1;
			$this->_update($values[$i],$data);
		}
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$filter_id = modules::run("filter_values/get_by_id",$this->uri->segment(3))->filter_id;
		$sort_id = modules::run("filters/get_by_id",$filter_id)->sort_id;
		$this->_delete($this->uri->segment(3));
		redirect('/filters/admin/'.$sort_id);
	}

	function get($order_by)
	{
		$tags = $this->mdl_filter_values->get($order_by);
		return $tags;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$tags = $this->mdl_filter_values->get_with_limit($limit, $offset, $order_by);
		return $tags;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_filter_values->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$tags = $this->mdl_filter_values->get_where_custom($col, $value);
		return $tags;
	}

	function _insert($data)
	{
		$this->mdl_filter_values->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_filter_values->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_filter_values->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_filter_values->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_filter_values->get_max();
		return $max_id;
	}

	function _custom_tags($mysql_tags) 
	{
		$tags = $this->mdl_filter_values->_custom_tags($mysql_tags);
		return $tags;
	}

}