<?php

class Brands extends MX_Controller
{
	var $module;

	function __construct()
	{
		parent::__construct();

		$this->load->model('mdl_brands');

		$this->module = "brands";
	}

	function sort()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$values = $this->input->post('values');

		for ($i=0; $i <= count($values); $i++) 
		{ 
			$data['position'] = $i + 1;
			$this->_update($values[$i],$data);
		}
	}

	function content()
	{	
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $bc_category->name,"link" => "/categories/admin/".$bc_category->id),
		// 	array("name" => $data['subcategory']->name." (Specifikacija)","link" => "")
		// 	);

		$data["brand"] = array_shift(modules::run('company/get_where_custom',"name","brands")); 
		echo modules::run('template/admin_render',$this->module,"brands",$data);
	}

	function edit_content()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		

		if($_FILES['brands']['error'] == 0)
		{
			//modules::run('resize/delete',$this->module,$category->image);
			modules::run('resize/do_upload_brands',"brands");		
		}		
		if($_FILES['brands-mobile']['error'] == 0)
		{
			//modules::run('resize/delete',$this->module,$category->image);
			modules::run('resize/do_upload_brands',"brands-mobile");		
		}

		$data['subtitle'] = $this->input->post('subtitle');
		$data['subtitle_en'] = $this->input->post('subtitle_en');
		$data['desc'] = $this->input->post('desc');
		$data['desc_en'] = $this->input->post('desc_en');

	    $this->db->where('name', "brands");
	    $this->db->update("company", $data);

		redirect('/brands/content/');
	}

	function admin()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$id = $this->uri->segment(3);
		// $data['category'] = modules::run("categories/get_by_id",$id);
		// $data['categories'] = modules::run("categories/get","position");

		// $data['breadcrumbs'] = array(
		// 	array("name" => "Proizvodi","link" => "/admin/products"),
		// 	array("name" => $data['category']->name,"link" => "")
		// 	); 

		$data['brands'] = modules::run("brands/get","position");

		echo modules::run('template/admin_render',$this->module,"admin",$data);
	}
	function url()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$categories = modules::run("categories/get","position");

		foreach ($categories as $category) 
		{
			$data['url'] = url_title(rs_char($category->name));
			$data['image'] = url_title(rs_char($category->name)).".jpg";

			$this->_update($category->id,$data);
		}

		die('done');
		//echo modules::run('template/render',$this->module,"index",$data);
	}
	
	function index()
	{
		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi ")
			); 	

		$data['brands'] = modules::run("brands/get","position");
		echo "<pre>";
		echo print_r($data["brands"]);
		die();
		//$data['products'] = modules::run("products/get_by_action");

		echo modules::run('template/render',$this->module,"index",$data);
	}

	function review()
	{
		$url = $this->uri->segment(2);
		$data['category'] = modules::run("categories/get_by_url",$url);

		$data['breadcrumbs'] = array(
			array("name" => "Proizvodi","link" => "/".$this->uri->segment(1)."/proizvodi "),
			array("name" => $data['category']->name,"link" => "/".$this->uri->segment(1)."/proizvodi/".$url)
			); 

		$data['subcategories'] = modules::run("subcategories/get_where_custom","cat_id",$data['category']->id);
		$data['products'] = modules::run("products/get_where_custom","cat_id",$data['category']->id);

		echo modules::run('template/render',$this->module,"single",$data);
	}

	function create()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		// $id = $this->uri->segment(3);
		// // $data['category'] = modules::run("categories/get_by_id",$id);
		// // $data['categories'] = modules::run("categories/get","position");

		// // $data['breadcrumbs'] = array(
		// // 	array("name" => "Proizvodi","link" => "/admin/products"),
		// // 	array("name" => $data['category']->name,"link" => "")
		// // 	); 

		// $data['brands'] = modules::run("brands/get","id");

		echo modules::run('template/admin_render',$this->module,"create");
	}

	function delete()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		//$category = $this->get_by_id($this->uri->segment(3));

		$this->_delete($this->uri->segment(3));

		redirect('/brands/admin/');
	}
	// function sort()
	// {
	// 	if(!$this->session->userdata('logged_in') == true)
	// 		redirect('prijava');
		
	// 	$sorted = $this->input->post("data");

	// 	foreach ($sorted as $key => $value)
	// 	{
	// 		$data['position'] = $value;

	// 		$this->db->where("id",$key);
	// 		$this->db->update("categories",$data);	
	// 	}
		
	// 	//$category = $this->get_by_id($id);

	// 	// echo "success"; 
	// }
	function edit()
	{

		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');

		$id = $this->uri->segment(3);


		$data['brand'] = $this->get_by_id($id);

		// $data['breadcrumbs'] = array(
		// 			array("name" => "Proizvodi","link" => "/admin/products"),
		// 			array("name" => $data["category"]->name,"link" => "/categories/edit/".$data["category"]->id)
		// );
		

		echo modules::run('template/admin_render',$this->module,"edit",$data);
	}
	function create_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$data['name'] = $this->input->post('name');
		$data['link'] = $this->input->post('link');

		if($_FILES['userfile']['error'] == 0)
		{
			//modules::run('resize/delete',$this->module,$category->image);
			$data['image'] = modules::run('resize/upload',"brands");		
		}

		$this->_insert($data);

		redirect('/brands/admin/');
	}
	function edit_proccess()
	{
		if(!$this->session->userdata('logged_in') == true)
			redirect('prijava');
		
		$id = $this->uri->segment(3);
		//$category = $this->get_by_id($id);

		$data['name'] = $this->input->post('name');
		$data['link'] = $this->input->post('link');

		if($_FILES['userfile']['error'] == 0)
		{
			modules::run('resize/delete',$this->module,$category->image);
			$data['image'] = modules::run('resize/upload',"brands");		
		}

		$this->_update($id,$data);

		redirect('/brands/edit/'.$id);
	}

	function get_max_pos()
	{
		$max_pos = $this->mdl_brands->get_max_pos();
		return $max_pos;
	}


	function get_by_url($url) 
	{
		$categories = $this->mdl_brands->get_by_url($url);		
		return $categories;
	}
	function get($order_by)
	{
		$categories = $this->mdl_brands->get($order_by);
		return $categories;
	}

	function get_with_limit($limit, $offset, $order_by) 
	{
		$categories = $this->mdl_brands->get_with_limit($limit, $offset, $order_by);
		return $categories;
	}

	function get_by_id($id)
	{
		$category = $this->mdl_brands->get_where($id);
		return $category;
	}

	function get_where_custom($col, $value) 
	{
		$categories = $this->mdl_brands->get_where_custom($col, $value);
		return $categories;
	}

	function _insert($data)
	{
		$this->mdl_brands->_insert($data);
	}

	function _update($id, $data)
	{
		$this->mdl_brands->_update($id, $data);
	}

	function _delete($id)
	{
		$this->mdl_brands->_delete($id);
	}

	function count_where($column, $value) 
	{
		$count = $this->mdl_brands->count_where($column, $value);
		return $count;
	}

	function get_max() 
	{
		$max_id = $this->mdl_brands->get_max();
		return $max_id;
	}

	function _custom_categories($mysql_categories) 
	{
		$categories = $this->mdl_brands->_custom_categories($mysql_categories);
		return $categories;
	}

}