        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar matchheight">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li class="snm-selected">
                                <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url; ?>"><?php echo $category->name; ?></a>
                                <ul class="sidebar-subnav">
    <?php foreach (modules::run("subcategories/get_where_custom","cat_id",$category->id) as $subcategory): ?>
                                        <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>/<?php echo $subcategory->url;?>"><?php echo $subcategory->name?></a>  </li>
    <?php endforeach; ?>                                   	
                                </ul>
                            </li>
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                    <div class="sidebar-back">
                        <a href="/<?php echo $this->uri->segment(1);?>/proizvodi " class="btn-default btn-default-back"><i class="icon icon-arrow-left"></i> Sve grupe</a>
                    </div> <!-- .sidebar-back -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="inside-product-items group">

<?php $products = modules::run("products/get_where_paginateFrontCategory",$category->id,12,$this->uri->segment(3)); ?>
<?php if ($products): ?>
<?php for($i=0;$i < count($products) ;$i++): ?>
                    <div class="col-1-3">
                        <div class="product-item">
                            <a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo url_title(rs_char($products[$i]->cat_name));?>/<?php echo url_title(rs_char($products[$i]->subcat_name));?>/<?php echo $products[$i]->url; ?>/<?php echo $products[$i]->id; ?>" class="pi-img">
                                <img src="/img/products/<?php echo $products[$i]->image; ?>" alt="">
                                <span class="btn-default pi-hover">Više <i class="icon icon-arrow-right"></i></span>
                            </a>
                            <a href="#" class="pi-name">
                                <span class="pi-kind"><?php echo $products[$i]->sort_name; ?></span> <span class="pi-manuf"><?php echo modules::run('manufacturers/get_by_id',$products[$i]->manuf_id)->name ; ?></span> <?php echo  $products[$i]->name; ?>
                            </a>
<?php if ($products[$i]->show_price): ?>
<?php if ((int)$products[$i]->discount): ?>
                            <p class="pi-price-disc">
                                <span class="pi-price-old group">
                                    <?php echo format_old_price($products[$i]->price); ?>
                                    <span class="pi-disc">-<?php echo floor($products[$i]->discount_value); ?>%</span>
                                </span>
                                <?php echo format($products[$i]->new_price); ?>
                            </p>
<?php else: ?>
                            <p class="pi-price-disc">
                                <?php echo format($products[$i]->price); ?>
                            </p>    
<?php endif ?>
<?php endif ?>
                                                         
                        </div> <!-- .product item -->
                    </div> <!-- .col-1-3 -->
<?php endfor ?> 
<?php else: ?>
    <p class="no-results">Nema rezultata.</p>
<?php endif ?>   
                    </div> <!-- .group -->
                        
                    <?php echo modules::run('pagination/paginateFrontCategory'); ?>

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->

