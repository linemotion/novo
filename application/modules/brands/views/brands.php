
        <aside class="sidebar">

            <header class="sidebar-header">
                <h1 class="sidebar-logo group">
                    <img src="/admin-assets/img/logo.png" alt="">
                    <span><b>Okov</b> Administracija</span>
                </h1>
            </header>

            <nav class="sidebar-nav">
                <ul>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-bar-chart-o"></i>Glavna</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-picture-o"></i>Kampanje</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-wrench"></i>Proizvodi</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-dot-circle-o"></i>Proizvođači</a>
                    </li>
                    <li>
                        <a href="/actions/admin"><i class="fa fa-lg fa-tag"></i>Akcije</a>
                    </li>
                    <li>
                        <a href="/news/admin"><i class="fa fa-lg fa-file-text-o"></i>Novosti</a>
                    </li>
                    <li>
                        <a href="/yourself/admin"><i class="fa fa-lg fa-youtube-play"></i>Uradi sam</a>
                    </li>
                    <li class="sidebar-nav-selected">
                        <a href="#"><i class="fa fa-lg fa-building-o"></i>Kompanija</a>
                        <ul class="sidebar-nav-groups">
                            <li>
                                <a href="#">Profil</a>
                            </li>
                            <li>
                                <a href="#">Menadžment</a>
                            </li>
                            <li>
                                <a href="/career/admin">Karijera</a>
                            </li>
                            <li class="sidebar-nav-groups-selected">
                                <a href="/brands/admin">Brendovi</a>
                            </li>
                            <li>
                                <a href="/letsdoit/admin">Let's do it</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/percent/admin"><i class="fa fa-lg fa-credit-card"></i>Posto</a>
                    </li>
                    <li>
                        <a href="/locations/admin"><i class="fa fa-lg fa-map-marker"></i>Lokacije</a>
                    </li>
                    <li>
                        <a href="/newsletter/admin"><i class="fa fa-lg fa-envelope-o"></i>Newsletter</a>
                    </li>
                        <li>
                            <a href="/newsletter/admin_old"><i class="fa fa-lg fa-envelope-o"></i>Newsletter-OLD</a>
                        </li>

                </ul>
            </nav>

            <footer class="copy-footer">
                CMS Copyright &copy; <a href="#">Linemotion</a>
            </footer>

        </aside> <!-- .sidebar -->

        <section class="main">

            <header class="main-header group">

                <form action="/products/search_admin" method="POST">
                    <div class="mh-search">
                        <input type="text" name="search" placeholder="Pretraga">
                        <button class="mh-search-submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>

                <a href="/user/logout" class="button-logout"><i class="fa fa-sign-out"></i> Izloguj se</a>

            </header>

            <div class="main-content">

                <div class="c-block group">
                    <h2 class="page-title">Brendovi</h2>
                </div>

                <header class="tab-header group">
                    <a href="/brands/admin" class="tab-1-2">Brendovi</a>
                    <a href="/brands/content" class="tab-1-2 tab-active">Sadržaj</a>
                </header>

                <form data-parsley-validate action="/brands/edit_content" method="post" enctype="multipart/form-data">

                    <div class="f-block f-block-top group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Podnaslov</h4>

                            <input type="text" name="subtitle" class="txtinput" value="<?php echo $brand->subtitle; ?>" required>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Podnaslov <i>en</i></h4>

                            <input type="text" name="subtitle_en" class="txtinput" value="<?php echo   $brand->subtitle_en; ?>" required>

                        </div>

                    </div> <!-- .f-block -->

                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Uvodni tekst </h4>

                            <div class="ckeditor-outer">
                                <textarea value="" name="desc" id="editor" cols="80" rows="5"><?php echo $brand->desc ?></textarea>
                            </div>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Uvodni tekst <i>en</i></h4>

                            <div class="ckeditor-outer">
                                <textarea value="" name="desc_en" id="editor2" cols="80" rows="5"><?php echo $brand->desc_en ?></textarea>
                            </div>

                        </div>

                    </div> <!-- .f-block -->
                    <div class="f-block group">

                        <div class="fi-1-2">

                            <h4 class="cb-title">Slika brendova</h4>

                            <img class="brands-admin-img" src="/assets/img/brands.png">

                            <input type="file" name="brands" class="txtinput">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Format: isključivo PNG. Kolorni mod: RGB. DIMENZIJE: 940x30 px</p>

                        </div>

                        <div class="fi-1-2">

                            <h4 class="cb-title">Mala slika brendova (mobilni)</h4>

                            <img class="brands-admin-img-small" src="/assets/img/brands-mobile.png">

                            <input type="file" name="brands-mobile" class="txtinput">

                            <p class="form-helper"><i class="fa fa-info-circle"></i> Format: isključivo PNG. Kolorni mod: RGB. DIMENZIJE: 480x95 px</p>

                        </div>

                    </div> <!-- .f-block -->
                    <button type="submit" class="big-submit">Pošalji</button>

                </form>


            </div> <!-- .main-content -->

        </section> <!-- .main -->

