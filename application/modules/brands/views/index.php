        <section class="inside-content group">

            <div class="content group">

                <aside class="inside-sidebar matchheight">

                    <nav class="sidebar-nav">

                        <ul class="sidebar-nav-main">
                            <li class="snm-selected"><a href="/<?php echo $this->uri->segment(1);?>/proizvodi "><i class="icon icon-tag"></i> Na akciji</a></li>
    <?php foreach (modules::run("categories/get","position") as $category): ?>
                            <li><a href="/<?php echo $this->uri->segment(1);?>/proizvodi/<?php echo $category->url;?>"><?php echo $category->name?></a></li>
    <?php endforeach ?> 
                        </ul>

                    </nav> <!-- .sidebar-nav -->

                </aside> <!-- .inside-sidebar -->

                <div class="inside-main">

                    <div class="inside-product-items group">

                    </div> <!-- .group -->

                </div> <!-- .inside-main -->

            </div> <!-- .content -->

        </section> <!-- .inside-content -->
