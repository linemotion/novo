<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>OKOV d.o.o. - okovi, vijčana roba, ručni i električni alati</title>
	<meta name="description" content="OKOV d.o.o. - okovi, vijčana roba, ručni i električni alati. Josipa Broza Tita 26, Podgorica, Crna Gora">

	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="stylesheet" href="/css/style.css?v=2">
	<?php if($this->uri->segment(1) == "en") echo '<link rel="stylesheet" href="/css/style-en.css?v=2">';?>

	<script src="/js/libs/modernizr-1.7.min.js"></script>
	<script>!window.jQuery && document.write(unescape('%3Cscript src="/js/libs/jquery-1.5.1.min.js"%3E%3C/script%3E'))</script>
	<script src="/js/an.js"></script>
	<script src="/js/meta.js"></script>
	<script src="/js/plugins.js"></script>
	<script src="/js/libs/jquery.cycle.lite.js"></script>
	<script src="/js/script.js"></script>
	<!--[if lt IE 7 ]>
	<script src="/js/libs/dd_belatedpng.js"></script>
	<script> DD_belatedPNG.fix('img, .png_bg');</script>
	<![endif]-->

	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-29378350-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

	<script>(function() {
	  var _fbq = window._fbq || (window._fbq = []);
	  if (!_fbq.loaded) {
	    var fbds = document.createElement('script');
	    fbds.async = true;
	    fbds.src = '//connect.facebook.net/en_US/fbds.js';
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(fbds, s);
	    _fbq.loaded = true;
	  }
	  _fbq.push(['addPixelId', '1009948045697339']);
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', 'PixelInitialized', {}]);
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1009948045697339&amp;ev=PixelInitialized" /></noscript>

</head>
<body id="home">

	<header>
		<div id="header-inside">
			<h1>
				<a href="/<?=$this->uri->segment(1)?>" class="ir">OKOV</a>
			</h1>
			<nav class="clearfix">
				<ul class="dropdown">
					<li class="nav-kompanija"><a href="/<?=$this->uri->segment(1)?>/kompanija/"><?=$this->lang->line('company')?></a>
						<ul class="sub_menu">
					<?php
						foreach($company as $item)
							echo '<li><a href="/'.$this->uri->Segment(1).'/kompanija/'.strtolower(convert($item->name_mn)).'">'.$item->$name.'</a></li>';
					?>
		        		</ul>
					</li>
					<li class="nav-proizvodi"><a href="/<?=$this->uri->segment(1)?>/proizvodi"><?=$this->lang->line('products')?></a>
						<ul class="sub_menu">
					<?php
						foreach($categories as $category)
		        			{
							echo '<li><a href="/'.$this->uri->segment(1).'/proizvodi/show/'.$category->id.'/'.convert(str_replace(" ","-",$category->$name)).'">'.$category->$name.'</a>';
							echo '<ul>';
								foreach(Subcategory::get_all_by_parent_id($category->id) as $subcategory)
									echo '<li><a href="/'.$this->uri->segment(1).'/'.'proizvodi'.'/prikazi/'.$subcategory->id.'/'.str_replace(' ','-',convert($subcategory->$name)).'">'.$subcategory->$name.'</a></li>';
							echo '</ul></li>';
						}
					?>
		        		</ul>
					</li>

					<li class="nav-akcije"><a href="/<?=$this->uri->segment(1)?>/akcije"><?=$this->lang->line('action')?></a></li>
					<li class="nav-novosti"><a href="/<?=$this->uri->segment(1)?>/novosti"><?=$this->lang->line('news')?></a></li>
					<li class="nav-servis"><a href="/<?=$this->uri->segment(1)?>/servis"><?=$this->lang->line('service')?></a></li>
					<li class="nav-kontakt"><a href="/<?=$this->uri->segment(1)?>/kontakt"><?=$this->lang->line('contact')?></a></li>
				</ul>
			</nav>
			<div id="search">
				<form id="searchform" action="/<?=$this->uri->segment(1)?>/index/search" method="post">
					<input
					<?php
					if($this->uri->segment(1) == "mn"):?>
						onkeyup="autosuggest(this.value,'mn')"
					<?php else:?>
						onkeyup="autosuggest(this.value,'en');"
					<?php endif;?>

					type="text" name="s" id="s" class="search_input swap autosuggest_input" autocomplete="off" value="<?=$this->lang->line('search')?>" />
					<input type="submit" value="" name="submit" class="search_btn" />
					<div class="autosuggest" id="autosuggest_list"></div>
				</form>
			</div><!-- end div#search -->


			<?php
				if($this->uri->segment(1) == 'mn')
					echo '<a href="/en/'.substr($this->uri->uri_string(),3).'" id="language"><span class="ir en">English</span></a>';
				else
					echo '<a href="/mn/'.substr($this->uri->uri_string(),3).'" id="language-mn"><span class="ir mn">Crnogorski</span></a>';
			?>
			<!-- end a#language -->

		</div><!-- end div#header-inside -->
	</header>

	<section id="subheader">
		<div id="subheader-inside">
			<h2 class="slogan ir">Majstoru od majstora</h2>
			<div id="featured" class="clearfix">
				<? if(isset($action) and !empty($action)):?>
					<div class="feat" id="feat1">
						<div class="feat-img">
							<div id="slider">
								<img src="/img/izbor1.jpg" alt="" />
								<img src="/img/izbor2.jpg" alt="" />
								<img src="/img/izbor3.jpg" alt="" />
							</div>
							<span></span>
						</div>
						<p class="feat-title">
							<?=$this->lang->line('selection')?> <a href="/<?=$this->uri->segment(1);?>/proizvodi" class="proizvodi-btn ir"><?=$this->lang->line('products')?></a>
						</p>
					</div>
					<div class="feat" id="feat2">
						<div class="feat-img">
							<img src="/img/<?=$action->image?>" alt="" />
							<span></span>
						</div>
						<p class="feat-title">
							<?=$action->$name?> <a href="/<?=$this->uri->segment(1)?>/akcije" class="akcije-btn ir"><?=$this->lang->line('action')?></a>
						</p>
					</div>
				<? else: ?>
				<div class="feat" id="feat3">
					<div class="feat-img">
						<div id="slider">
							<img src="/img/izborbig1.jpg" alt="" />
							<img src="/img/izborbig2.jpg" alt="" />
							<img src="/img/izborbig3.jpg" alt="" />
						</div>
						<span></span>
					</div>
					<p class="feat-title">
						<?=$this->lang->line('selection_full')?> <a href="#" class="proizvodi-btn ir"><?=$this->lang->line('products')?></a>
					</p>
				</div>
				<? endif;?>
			</div>
		</div><!-- end div#subheader-inside -->
	</section>

	<div id="main" role="main">
		<div id="main-inside" class="clearfix">
			<div id="sidebar">
				<div id="novosti-aside">
					<h4 class="novosti ir"><?=$this->lang->line('news')?></h4>
					<div class="novost-img">
						<img src="/img/<?=$news->image?>" alt="" />
						<span></span>
					</div>
					<p><?=$news->$short_content?></p>
					<p class="clearfix">
						<a href="/<?=$this->uri->segment(1)?>/novosti" class="vise-btn ir"><?=$this->lang->line('more')?></a>
					</p>
				</div><!-- end div#novosti-aside -->
				<div id="karijera-aside">
					<h4 class="karijera ir"><?=$this->lang->line('career')?></h4>
					<img src="/img/karijera.jpg" alt="" />
					<p><?=$this->lang->line('career_text')?></p>
					<p class="clearfix">
						<a href="/<?=$this->uri->segment(1)?>/kompanija/karijera" class="prijavi-se-btn ir"><?=$this->lang->line('more')?></a>
					</p>
				</div><!-- end div#novosti-aside -->
			</div><!-- end div#sidebar -->
			<div id="content">
				<div class="clearfix">
					<img src="/img/rast.jpg" alt="" class="flm" />
					<h4><?=$profile->$name?></h4>
					<p><?=$profile->$content?></p>
					<a href="/<?=$this->uri->segment(1)?>/kompanija/profil" class="o-nama-btn ir"><?=$this->lang->line('about_us')?></a>
				</div>
				<div class="clearfix topped">
					<img src="/img/mapa.jpg" alt="" class="frm" />
					<h4><?=$contact->$name?></h4>
					<p><?=$contact->$content?></p>
					<a href="/<?=$this->uri->segment(1)?>/kontakt" class="kontakt-btn ir"><?=$this->lang->line('more')?></a>
				</div>
			</div><!-- end div#content -->
		</div><!-- end div#main-inside -->
		<div id="brands">
			<img src="/img/brandshort.png" alt="" />
			<a href="/<?=$this->uri->segment(1)?>/kompanija/brendovi" class="svi-brendovi ir"><?=$this->lang->line('all_brends')?></a>
		</div>
		<br /><br />
	</div><!-- end div#main -->

	<footer>
		<div id="footer-inside">
			<a href="/" id="footer-logo">
				<img src="/img/okov-logo.png" alt="" />
			</a>
			<p id="adresa">
				Josipa Broza Tita 26<br />
				81000 Podgorica, Crna Gora<br />
				+382 20 658 501<br />
			</p>
			<p id="footer-nav">
				<a href="/<?=$this->uri->segment(1);?>/"><?=$this->lang->line('home')?></a> <a href="/<?=$this->uri->segment(1);?>/kompanija"><?=$this->lang->line('company')?></a> <a href="/<?=$this->uri->segment(1);?>/proizvodi"><?=$this->lang->line('products')?></a> <a href="/<?=$this->uri->segment(1);?>/akcije"><?=$this->lang->line('action')?></a> <a href="/<?=$this->uri->segment(1);?>/novosti"><?=$this->lang->line('news')?></a> <a href="/<?=$this->uri->segment(1);?>/servis"><?=$this->lang->line('service')?>
				</a> <a href="/<?=$this->uri->segment(1);?>/kontakt"><?=$this->lang->line('contact')?></a>
			</p>
			<p id="social">
				<a href="http://www.facebook.com/okovdoo" id="fb"><img src="/img/fb.png" alt="" /></a>
				<a href="http://www.twitter.com/okovdoo" id="tw"><img src="/img/tw.png" alt="" /></a>
				<a href="mailto:okov@okov.me" id="mail"><img src="/img/mail.png" alt="" /></a>
			</p>
			<p id="social-msg">

			</p>
		</div><!-- end div#footer-inside -->
	</footer>


</body>
</html>
