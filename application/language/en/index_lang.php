<?php
$lang['career'] = "CAREER";
$lang['selection'] = "Wide range";
$lang['selection_full'] = "Large selection- we offer over 45.000 products";
$lang['about'] = "ABOUT US";
$lang['more'] = "MORE";
$lang['all_brands'] = "ALL BRANDS";
$lang['all_groups'] = "All Groups";
$lang['all_groups_full'] = "All discounted groups";
$lang['all_sales_full'] = "All groups on sale";
$lang['search'] = "search";
$lang['sign_up'] = "APPLY";
$lang['discount'] = "DISCOUNT";
$lang['career_text'] = 'Become a member of our successful team and achieve your goals.';
$lang['related'] = "Related products:";
$lang['thank'] = "Thank You!";
$lang['thank_msg'] = "Your message has been sent.";
$lang['more'] = "More";
$lang['pamphlet'] = "Pamphlet";
$lang['featured'] = "Featured";
$lang['contact_us'] = "Contact us";
$lang['all_news'] = "All news";
$lang['retail'] = "Retail center";
$lang['action_products'] = "Products on sale";
$lang['on_sale'] = "Discounted";
$lang['sales'] = "On sale";
$lang['all_action_products'] = "All Discounted products";
$lang['all_sales_products'] = "All Products on sale";

$lang['pro_action'] = "Promo";
$lang['pro_sale'] = "Sale";
$lang['pro_discount'] = "Discount";


$lang['Full_Name'] = "Full Name";
$lang['Full_Address_and_Town'] = "Full Address and Town";
$lang['Telephone_Number'] = "Telephone Number";
$lang['E-Mail'] = "E-mail";
$lang['Personal_Identity_Number'] = "Personal Identity Number";
$lang['Personal_Card_Number'] = "Personal Card Number";
$lang['DOB'] = "Date Of Birth";
$lang['Occupation'] = "Occupation";
$lang['Products_Interested']  = "Products I am interested in:";

$lang['Company_Name'] = "Company Name";
$lang['VAT_Number'] = "VAT Number";
$lang['Main_Business_Activity'] = "Main Business Activity";

$lang['Authorized_Person'] = "Authorized Person";

$lang['Add_Authorized_Person'] = "Add authorized person";

$lang['Natural_Persons'] = "Natural Persons";
$lang['Legal_Persons'] = "Legal Persons";

$lang['Field_Required'] = "This field is required.";

$lang['Sendit'] = "Send";


//----------navigacija---------------//
$lang['company'] = "Company";
$lang['news'] = "News";
$lang['yourself'] = "Do it yourself";
$lang['products'] = "Products";
$lang['service'] = "Support";
$lang['contact'] = "Contact";
$lang['action'] = "Sale";
$lang['home'] = "Home";
$lang['locations'] = "Locations";
$lang['management'] = "Management";
$lang['brands'] = "Brands";
$lang['profile'] = "Profile";
$lang['letsdoit'] = "Let's do it";
$lang['percent'] = "Posto";
$lang['Q&A'] = "Questions";
$lang['Application'] = "Application for Posto Card";
$lang['Card'] = "Posto Card";
$lang['rules'] = "Rules and Conditions";
$lang['newsletter'] = "Newsletter";



//----------forma---------------//
$lang['name'] = "Full name:";
$lang['year'] = "Year of birth:";
$lang['mail'] = "E-mail:";
$lang['city'] = "City:";
$lang['country'] = "Country:";
$lang['phone'] = "Contact phone:";
$lang['education'] = "Education:";
$lang['languages'] = "Languages:";
$lang['IT'] = "Computing capabilities:";
$lang['XP'] = "Work experience:";
$lang['position'] = "Position you are applying:";
$lang['message'] = "Your message(optional):";
$lang['msg'] = "Your message:";
$lang['CV'] = "Your CV:";
$lang['send'] = "Send";

//----------search---------------//

$lang['Category'] = "Categories";
$lang['Subcategory'] = "Subcategories";
$lang['Group'] = "Groups";
$lang['Category_empty'] = "there`re no categories by that name";
$lang['Subcategory_empty'] = "there`re no subcategories by that name";
$lang['Group_empty'] = "there`re no groups by that name";
$lang['Product_empty'] = "there`re no products by that name";
$lang['Search'] = "fill out the search form";
?>
