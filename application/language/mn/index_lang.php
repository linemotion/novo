<?php
$lang['career'] = "KARIJERA";
$lang['selection'] = "Najveći izbor";
$lang['selection_full'] = "Najveći izbor - preko 45.000 proizvoda u našoj ponudi";
$lang['about'] = "O NAMA";
$lang['all_brands'] = "SVI BRENDOVI";
$lang['all_groups'] = "Sve Grupe";
$lang['all_groups_full'] = "Sve grupe na akciji i popustu";
$lang['all_sales_full'] = "Sve grupe na rasprodaji";
$lang['search'] = "pretraga";
$lang['sign_up'] = "PRIJAVI SE";
$lang['discount'] = "AKCIJA";
$lang['career_text'] = 'Postanete dio našeg uspješnog tima i ostvarite ciljeve.';
$lang['related'] = "Slični proizvodi:";
$lang['thank'] = "Hvala!";
$lang['thank_msg'] = "Vaša poruka je poslata.";
$lang['more'] = "Više";
$lang['pamphlet'] = "Letak";
$lang['featured'] = "Izdvajamo";
$lang['contact_us'] = "Kontaktirajte nas";
$lang['all_news'] = "Sve novosti";
$lang['action_products'] = "Proizvodi na akciji";
$lang['all_action_products'] = "Svi proizvodi na akciji i popustu";
$lang['all_sales_products'] = "Svi proizvodi na rasprodaji";
$lang['retail'] = "Prodajni centri";
$lang['on_sale'] = "Na akciji i popustu";
$lang['sales'] = "Rasprodaja";

$lang['pro_action'] = "Akcija";
$lang['pro_sale'] = "Rasprodaja";
$lang['pro_discount'] = "Popust";

$lang['Full_Name'] = "Ime i prezime";
$lang['Full_Address_and_Town'] = "Adresa i grad";
$lang['Telephone_Number'] = "Broj telefona";
$lang['E-Mail'] = "E-mail";
$lang['Personal_Identity_Number'] = "JMBG";
$lang['Personal_Card_Number'] = "Broj lične karte";
$lang['DOB'] = "Datum rođenja";
$lang['Occupation'] = "Zanimanje";
$lang['Products_Interested']  = "Proizvodi koji me interesuju:";

$lang['Company_Name'] = "Naziv";
$lang['VAT_Number'] = "PIB";
$lang['Main_Business_Activity'] = "Djelatnost";

$lang['Authorized_Person'] = "Ovlašćeno lice";

$lang['Add_Authorized_Person'] = "Dodaj ovlašćeno lice";

$lang['Natural_Persons'] = "Fizička lica";
$lang['Legal_Persons'] = "Pravna lica";

$lang['Field_Required'] = "Morate ispuniti ovo polje.";

$lang['Sendit'] = "Pošalji";


//----------navigacija---------------//
$lang['company'] = "Kompanija";
$lang['news'] = "Novosti";
$lang['yourself'] = "Uradi sam";
$lang['products'] = "Proizvodi";
$lang['service'] = "Servis";
$lang['contact'] = "Kontakt";
$lang['action'] = "Akcija";
$lang['home'] = "Naslovna";
$lang['locations'] = "Lokacije";
$lang['management'] = "Menadžment";
$lang['brands'] = "Brendovi";
$lang['profile'] = "Profil";
$lang['letsdoit'] = "Let's do it";
$lang['percent'] = "Posto";
$lang['Card'] = "O Posto kartici";
$lang['Q&A'] = "Pitanja";
$lang['Application'] = "Prijava za Posto karticu";
$lang['rules'] = "Pravila i uslovi";
$lang['newsletter'] = "Newsletter";

//----------forma---------------//
$lang['name'] = "Ime i Prezime:";
$lang['mail'] = "E-mail:";
$lang['year'] = "Godina rodjenja:";
$lang['city'] = "Grad:";
$lang['country'] = "Država:";
$lang['phone'] = "Kontakt telefon:";
$lang['education'] = "Obrazovanje:";
$lang['languages'] = "Jezici:";
$lang['IT'] = "Računarske sposobnosti:";
$lang['XP'] = "Radno iskustvo:";
$lang['position'] = "Otvorene pozicije:";
$lang['message'] = "Vaša poruka(opciono):";
$lang['msg'] = "Vaša poruka:";
$lang['CV'] = "Vaš CV:";
$lang['send'] = "Pošalji";
//----------search---------------//

$lang['Category'] = "Kategorije";
$lang['Subcategory'] = "Podkategorije";
$lang['Group'] = "Grupe";
$lang['Category_empty'] = "Nema kategorija pod tim imenom";
$lang['Subcategory_empty'] = "Nema podkategorija pod tim imenom";
$lang['Group_empty'] = "Nema grupa pod tim imenom";
$lang['Product_empty'] = "Nema proizvoda pod tim imenom";
$lang['Search'] = "Unesite termin za pretragu";
?>
