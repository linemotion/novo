<?php
	function format($price)
	{
		$integ = array();
		$integ = explode(".", $price);
		
		echo  '<span class="pi-price">'.$integ[0].'<span class="pi-decimal">.'.$integ[1]."</span></span>";
	}
	function format_old_price($price)
	{
		$integ = array();
		$integ = explode(".", $price);
		
		echo  '<del class="pi-price-old-value">'.$integ[0].'<span class="pi-decimal">.'.$integ[1]."</span></del>";
	}
	
	function round_out ($value, $places=0) 
	{
	  if ($places < 0) { $places = 0; }
	  $mult = pow(10, $places);
	  return ($value >= 0 ? ceil($value * $mult):floor($value * $mult)) / $mult;
	}