<?php  

function rs_char($source)
{
	$url = array('S', 'C', "Dj", 'Z', 'C', 's', 'c', 'dj', 'c', 'z'); 
	$char = array('Š', 'Ć', 'Đ', 'Ž', 'Č', 'š', 'ć', 'đ', 'č', 'ž'); 
	
	return strtolower(str_replace($char, $url, $source));
}

function rs_reverse($source)
{
	$url = array('S', 'C', "Dj", 'Z', 'C', 's', 'c', 'dj', 'c', 'z'); 
	$char = array('Š', 'Ć', 'Đ', 'Ž', 'Č', 'š', 'ć', 'đ', 'č', 'ž'); 
	
	return strtolower(str_replace($char, $url, $source));
}

function wp_char($source)
{
	$url = array('Š', 'C', "Dj", 'Z', 'C', 'Å¡', 'Ä‡', 'dj', 'Ä', 'Å¾'); 
	$char = array('Š', 'Ć', 'Đ', 'Ž', 'Č', 'š', 'ć', 'đ', 'č', 'ž'); 
	
	return str_replace($url, $char, $source);
}
