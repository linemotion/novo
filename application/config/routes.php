<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "home";
$route['404_override'] = '';

 $route['admin'] = 'admin/dashboard';

$route['(mn|en)/akcija'] = 'actions/index';
$route['(mn|en)/lokacije'] = 'locations';
$route['(mn|en)/kontakt'] = 'locations/contact';
$route['(mn|en)/posto'] = 'percent/index';

$route['(mn|en)/novosti'] = 'news';
$route['(mn|en)/novosti/(:num)'] = 'news/review/$1';
$route['(mn|en)/uradisam/(:num)'] = 'yourself/review/$1';

$route['(mn|en)/profil'] = 'company/index';
$route['(mn|en)/menadzment'] = 'company/management';
$route['(mn|en)/karijera'] = 'company/career';
$route['(mn|en)/brendovi'] = 'company/brands';
$route['(mn|en)/letsdoit'] = 'letsdoit/index';
$route['(mn|en)/uradisam'] = 'yourself/index';

$route['(mn|en)/posto/kartica'] = 'percent/index';
$route['(mn|en)/posto/pitanja'] = 'percent/questions';
$route['(mn|en)/posto/prijava'] = 'percent/application';
$route['(mn|en)/posto/hvala'] = 'percent/thanks';
$route['(mn|en)/posto/pravila'] = 'percent/rules';
$route['(mn|en)/kontakt/newsletter'] = 'newsletter/index';

// $route['(mn|en)/proizvodi/(:any)/(:any)/(:any)/f/(:num)/(:num)'] = 'sorts/filters/$3';
$route['(mn|en)/akcija/(:num)'] = 'actions/index/$1';
$route['(mn|en)/proizvodi-akcija/(:num)'] = 'categories/filter/$1';
$route['(mn|en)/proizvodi-akcija'] = 'categories/filter';
$route['(mn|en)/proizvodi-rasprodaja/(:num)'] = 'products/filter_sales/$1';
$route['(mn|en)/proizvodi-rasprodaja'] = 'products/filter_sales';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:any)/f'] = 'sorts/filters/$3';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:any)/f/(:num)/(:num)'] = 'sorts/filters/$3';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:any)/p/(:num)'] = 'sorts/review/$3';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:any)/f/(:num)'] = 'sorts/filters/$3';
$route['(mn|en)/proizvodi/pretraga/(:any)/(:num)'] = 'products/search/$1/$2';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:any)/(:num)'] = 'products/review';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:num)'] = 'subcategories/review/$3';
$route['(mn|en)/proizvodi/(:any)/(:any)/(:any)'] = 'sorts/review';
$route['(mn|en)/proizvodi/rasprodaja/(:any)'] = 'products/sales/$1';
$route['(mn|en)/proizvodi/akcija/(:any)'] = 'products/action/$1';
$route['(mn|en)/proizvodi/pretraga/(:any)'] = 'products/search/$1';
$route['(mn|en)/proizvodi/(:num)'] = 'categories/index/$1';
$route['(mn|en)/proizvodi/(:any)/(:num)'] = 'categories/review/$1';
$route['(mn|en)/proizvodi/(:any)/(:any)'] = 'subcategories/review/$2';
$route['(mn|en)/proizvodi/akcija'] = 'products/action';
$route['(mn|en)/proizvodi/rasprodaja'] = 'products/sales';
$route['(mn|en)/proizvodi/(:any)'] = 'categories/review/$1';
$route['(mn|en)/proizvodi'] = 'categories/index';

 $route['prijava'] = 'user/index';
// $route['servis'] = 'service/help';
// $route['o-nama'] = 'about';
// $route['akcije'] = 'action';
// $route['kontakt'] = 'contact';


 
$route['(\w{2})/(.*)'] = '$2';
$route['(\w{2})'] = $route['default_controller'];

// $route['black-red-white/(:any)/(:any)/(:num)/(:num)'] = 'subcategoriesBRW/review';
// $route['black-red-white/(:any)/(:any)/(:any)/(:num)'] = 'productsBRW/review';
// $route['black-red-white/(:any)/(:any)/(:num)'] = 'subcategoriesBRW/review/$3';
// $route['black-red-white/akcija/(:any)'] = 'productsBRW/action/$1';
// $route['black-red-white/akcija'] = 'productsBRW/action';
// $route['black-red-white/(:any)/(:num)'] = 'categoriesBRW/review';
// $route['black-red-white'] = 'categoriesBRW/index';


// $route['servis/pomoc-pri-kupovini'] = 'service/help';
// $route['servis/reklamacije'] = 'service/complaint';
// $route['servis/uslovi-placanja'] = 'service/terms';


// $route['grupe/pretraga/(:any)'] = 'categories/search/$1';
// $route['podgrupe/pretraga/(:any)'] = 'subcategories/search/$1';


/*$route['brw/(:any)/(:any)/(:any)'] = 'productsBRW/index';
$route['brw/(:any)/(:any)'] = 'subcategoriesBRW/index';
$route['brw/(:any)'] = 'categoriesbrw/edit/1';*/

//$route['admin/([a-zA-Z]+)/(:any)'] = "$1/admin_$2";

/* End of file routes.php */
/* Location: ./application/config/routes.php */